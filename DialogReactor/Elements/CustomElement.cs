﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Media;

namespace DialogReactor.Elements
{
    public enum ElementFieldDataType
    {
        String,
        // SQlite 'text'
        // C# 'string' - single text line (without '\n')
        // XML 'string'
        // default value: string.Empty

        Text,
        // SQLite 'text'
        // C# 'string' - multiline text
        // XML 'text'
        // C# default value: string.Empty

        Bool,
        // SQLite 'integer' - 0 is false, 1 is true
        // C# 'bool'
        // XML 'bool'
        // SQLite default value: 0
        // C# default value: false

        Int,
        // SQLite 'integer'
        // C# 'int' (32 bit)
        // XML 'bool'
        // default value: 0

        Long,
        // SQLite 'integer'
        // C# 'long' (64 bit)
        // XML 'long'
        // default value: 0

        Real
        // SQLite 'real'
        // C# 'float'
        // XML 'real'
        // default value: 0.0f
    }

    public class ElementField
    {
        public string Title { get; set; }
        public ElementFieldDataType DataType { get; set; }

        public bool FieldPreview { get; set; }
        public bool FieldNotEmpty { get; set; } // string, text types only

        public ElementField(string title)
        {
            Title = title;
        }
    }

    public class CustomElement
    {
        public string Title { get; private set; }

        public Dictionary<string, ElementField> Fields;
        private Color Color;

        public CustomElement(string title, ObservableCollection<ElementField> fields, Color elementColor)
        {
            Title = title;
            Color = elementColor;

            Fields = new Dictionary<string, ElementField>();
            foreach (ElementField field in fields)
            {
                Fields.Add(field.Title, field);
            }
        }
    }
}