﻿using DialogReactor.Elements;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using System.Xml;

namespace DialogReactor
{
    public partial class MainWindow : Window
    {
        private const int ClosedTabHistorySize = 20;

        private Action EmptyDelegate = delegate () { };

        public DialogReactorData Data { get; private set; }
        public TreeView ProjectFileTree { get { return treeView; } }

        public object SelectedTreeItem { get { return treeView.SelectedItem; } }

        public bool IsKeyCtrlPressed { get; private set; }

        private List<Dialog> ClosedTabs { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            Data = new DialogReactorData(this);
            ClosedTabs = new List<Dialog>();
            BuildContextMenu();
        }

        private void BuildContextMenu()
        {
            var menu = new ContextMenu();
            var clear = new MenuItem();

            clear.Header = "Clear";
            clear.Click += ClearLogBox;

            menu.Items.Add(clear);
            
            ErrorsLog.Init(this);
            LogBox.ContextMenu = menu;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Log("Dialog Reactor v" + Version.Title() + " loaded");
        }

        public void Refresh()
        {   // using to sync rendering thread
            Dispatcher.Invoke(DispatcherPriority.Render, EmptyDelegate);
        }

        private void MenuItem_NewProject(object sender, RoutedEventArgs e)
        {
            var dialog = new DialogNewProject();

            if (dialog.ShowDialog() == true)
            {
                ProjectCreator.CreateNewProject(dialog.NewProjectTitle, dialog.NewProjectPath, dialog.NewProjectSourceFolder);

                Log("Create new project title=\"" + dialog.NewProjectTitle + "\" path=\"" + dialog.NewProjectPath + "\"");
                Data.ProjectFile = dialog.NewProjectPath + dialog.NewProjectTitle + ".dialog";
                OpenProject(Data.ProjectFile);
            }
        }

        private void MenuItem_OpenProject(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog();
            dialog.Filter = "Dialog Project (*dialog)|*.dialog";
            dialog.FilterIndex = 1;
            dialog.RestoreDirectory = true;

            if (dialog.ShowDialog() == true)
            {
                Data.ProjectFile = dialog.FileName;
                OpenProject(Data.ProjectFile);
            }
        }

        private void MenuItem_CloseProject(object sender, RoutedEventArgs e)
        {
            CloseProject();
        }

        private void ExportProject(object sender, RoutedEventArgs e)
        {
            Data.ExportProject();
        }

        private void MenuItem_About(object sender, RoutedEventArgs e)
        {
            var dialog = new DialogAbout();
            dialog.ShowDialog();
        }

        private void MenuItem_Exit(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        public void OpenProject(string file)
        {
            if (!string.IsNullOrEmpty(file) && File.Exists(file))
            {
                // close opened project
                CloseProject();

                // open
                try
                {
                    var doc = new XmlDocument();
                    doc.Load(file);

                    if (doc != null)
                    {
                        var versionMajor = XmlProjectFileManager.FindXMLNode(doc, "/project/version/major");
                        var versionMinor = XmlProjectFileManager.FindXMLNode(doc, "/project/version/minor");

                        if(!string.IsNullOrWhiteSpace(versionMajor) && !string.IsNullOrWhiteSpace(versionMinor))
                        {
                            int major = 0;
                            int minor = 0;

                            if(int.TryParse(versionMajor, out major) && int.TryParse(versionMinor, out minor))
                            {
                                if(major == Version.MAJOR && minor == Version.MINOR)
                                {
                                    // load project
                                    ProjectHandler project = new ProjectHandler(this, file, doc);
                                    Data.Project = project;
                                    project.LoadProject();

                                    MenuButtonClose.IsEnabled = true;
                                    MenuButtonImport.IsEnabled = true;
                                    MenuButtonBuildProject.IsEnabled = true;

                                    Log("Project loaded from \"" + file + "\"");
                                }
                                else
                                {
                                    Log("Error on project loading - used old format version (" + major + "." + minor + ")");
                                }
                            }
                            else
                            {
                                Log("Error on project loading - invalid version data");
                            }
                        }
                        else
                        {
                            Log("Error on project loading - version descriptor not found");
                        }
                    }
                    else
                    {
                        Log("Error on project loading \"" + file + "\"");
                    }
                }
                catch (IOException ex)
                {
                    Log("Error on project loading \"" + file + "\" - IOExeption");
                    Log(ex.StackTrace);
                }
                catch(Exception ex)
                {
                    Log("Error on project loading \"" + file + "\"");
                    Log(ex.StackTrace);
                }
            }
            else
            {
                Log("Error on project loading \"" + file + "\" - file not exist");
            }
        }

        private void CloseProject()
        {
            if(Data.Project != null)
            {
                Log("Project closed \"" + Data.Project.Title + "\"");

                treeView.Items.Clear();
                Data.Project = null;

                MenuButtonClose.IsEnabled = false;
                MenuButtonImport.IsEnabled = false;
                MenuButtonBuildProject.IsEnabled = false;
                DisableUndoRedo();

                tabControl.Items.Clear();

                LogMessagesClear();
            }
        }

        internal ClosableTab AddDialogTab(Dialog dialog)
        {
            var item = new ClosableTab(this, dialog);

            item.Title = dialog.Title;
            tabControl.Items.Add(item);
            item.Focus();

            if (ClosedTabs.Contains(dialog))
            {
                ClosedTabs.Remove(dialog);
                ClosedTabsHistoryUpdate();
            }

            Log("Dialog open \"" + dialog.Title + "\"");
            return item;
        }

        internal void CloseDialogTab(Dialog dialog)
        {
            if (dialog != null && dialog.TabItem != null)
            {
                if (tabControl.Items.Contains(dialog.TabItem))
                {
                    tabControl.Items.Remove(dialog.TabItem);
                    dialog.TabItem = null;

                    ClosedTabsHistoryAdd(dialog);
                    ClosedTabsHistoryUpdate();
                }
                else
                {
                    Log("Error on close tab - tab with title \"" + dialog.Title + "\" not exist");
                }
            }
        }

        internal void CloseAllTabs()
        {
            if(tabControl.Items.Count > 0)
            {
                foreach (TabItem item in tabControl.Items)
                {
                    var tab = item as ClosableTab;
                    tab.Dialog.TabItem = null;
                    ClosedTabsHistoryAdd(tab.Dialog);
                }

                tabControl.Items.Clear();
                ClosedTabsHistoryUpdate();
            }
        }

        private void tabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateUndoRedoControl();
        }

        public void UpdateUndoRedoControl()
        {
            DisableUndoRedo();

            if (tabControl.Items.Count > 0)
            {
                if (tabControl.SelectedItem is ClosableTab)
                {
                    var tab = tabControl.SelectedItem as ClosableTab;

                    if (!tab.JournalUndoIsEmpty())
                    {
                        UndoEnable();
                    }

                    if (!tab.JournalRedoIsEmpty())
                    {
                        RedoEnable();
                    }
                }
            }
        }

        private void DisableUndoRedo()
        {
            Undo.IsEnabled = false;
            Redo.IsEnabled = false;
        }

        private void UndoEnable()
        {
            Undo.IsEnabled = true;
        }

        private void RedoEnable()
        {
            Redo.IsEnabled = true;
        }

        internal bool ConfirmFolderDelete(string title)
        {
            var dialog = new DialogRemoveConfirm();
            dialog.SetFileTitle(title);

            if(dialog.ShowDialog() == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        static TreeViewItem VisualUpwardSearch(DependencyObject source)
        {
            while (source != null && !(source is TreeViewItem))
            {
                source = VisualTreeHelper.GetParent(source);
            }

            return source as TreeViewItem;
        }

        private void TreeView_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Data.Project != null)
            {
                Data.Project.DialogOpen();
            }
        }

        private void TreeView_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            var item = VisualUpwardSearch(e.OriginalSource as DependencyObject);

            if (item != null)
            {
                item.Focus();
                e.Handled = true;
            }
        }

        private void treeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if(treeView.SelectedItem != null)
            {
                var item = treeView.SelectedItem;

                if (item is Dialog)
                {
                    treeView.ContextMenu = treeView.Resources["DialogContext"] as ContextMenu;
                }
                else if (item is Format)
                {
                    treeView.ContextMenu = treeView.Resources["FormatContext"] as ContextMenu;
                }
                else if (item is ProjectFolder)
                {
                    var folder = item as ProjectFolder;

                    if(folder.ContentType == FileContentType.Dialog)
                    {
                        if (folder.IsRoot)
                        {
                            treeView.ContextMenu = treeView.Resources["ProjectSourceFolderContext"] as ContextMenu;
                        }
                        else
                        {
                            treeView.ContextMenu = treeView.Resources["ProjectFolderContext"] as ContextMenu;
                        }
                    }
                    else if(folder.ContentType == FileContentType.Format)
                    {
                        if (folder.IsRoot)
                        {
                            treeView.ContextMenu = treeView.Resources["ProjectFormatsFolderContext"] as ContextMenu;
                        }
                        else
                        {
                            treeView.ContextMenu = treeView.Resources["FormatsFolderContext"] as ContextMenu;
                        }
                    }
                }
                else if(item is ProjectTree)
                {
                    treeView.ContextMenu = treeView.Resources["ProjectTree"] as ContextMenu;
                }
            }
        }

        private void OpenDialog(object sender, RoutedEventArgs e)
        {
            if(Data.Project != null)
            {
                Data.Project.DialogOpen();
            }
        }

        public void OpenDialog(Dialog dialog)
        {
            if(Data.Project != null)
            {
                Data.Project.DialogOpen(dialog);
            }
        }

        public void SelectTab(ClosableTab tabItem)
        {
            if(tabItem != null)
            {
                tabControl.SelectedItem = tabItem;
            }
        }

        public void ScrollToElement(int elementId)
        {
            if(Data.Project != null)
            {
                if(tabControl.SelectedItem != null && tabControl.SelectedItem is ClosableTab)
                {
                    var tab = tabControl.SelectedItem as ClosableTab;
                    tab.ScrollToElementById(elementId);
                }
            }
        }

        internal void SelectElement(int elementId)
        {
            if (Data.Project != null)
            {
                if (tabControl.SelectedItem != null && tabControl.SelectedItem is ClosableTab)
                {
                    var tab = tabControl.SelectedItem as ClosableTab;
                    tab.SelectElementById(elementId);
                }
            }
        }

        private void RemoveProjectItem(object sender, RoutedEventArgs e)
        {
            if (Data.Project != null)
            {
                Data.Project.RemoveProjectItem();
                ClosedTabsHistoryUpdate();
            }
        }

        private void EditDialog(object sender, RoutedEventArgs e)
        {
            if (TreeViewSelectedItemValidation() && treeView.SelectedItem is Dialog)
            {
                var dialog = treeView.SelectedItem as Dialog;
                var edit = new DialogEditDialog(dialog);

                if (edit.ShowDialog() == true)
                {
                    dialog.DialogId = edit.DialogId;
                    dialog.AvatarKey = edit.AvatarKey;
                    dialog.Save();
                }
            }
        }

        private void AddFolder(object sender, RoutedEventArgs e)
        {
            if (TreeViewSelectedItemValidation() && treeView.SelectedItem is ProjectFolder)
            {
                var tree = Data.Project.ProjectTree;
                var folder = treeView.SelectedItem as ProjectFolder;

                var dialog = new DialogNewFolder(tree, folder);

                if (dialog.ShowDialog() == true)
                {
                    Data.Project.FolderAdd(dialog.FolderTitle, folder);
                }
            }
        }

        private void AddContentFile(object sender, RoutedEventArgs e)
        {
            if (!TreeViewSelectedItemValidation())
            {
                return;
            }
            
            if (treeView.SelectedItem is ProjectFolder)
            {
                var targetFolder = treeView.SelectedItem as ProjectFolder;

                if(targetFolder.ContentType == FileContentType.Dialog)
                {
                    var dialog = new DialogNewDialog(Data.Project.ProjectTree, targetFolder);

                    if (dialog.ShowDialog() == true)
                    {
                        Data.Project.DialogAdd(dialog.DialogTitle, dialog.DialogId, dialog.DialogAvatarKey, targetFolder);
                    }
                }
                else if(targetFolder.ContentType == FileContentType.Format)
                {
                    var dialog = new DialogNewFormat(Data.Project, targetFolder);

                    if(dialog.ShowDialog() == true)
                    {
                        Data.Project.FormatAdd(targetFolder, dialog.FormatName);
                    }
                }
            }
            else if (treeView.SelectedItem is ContentFile)
            {
                var targetFile = treeView.SelectedItem as ContentFile;

                if(targetFile.ContentType == FileContentType.Format)
                {
                    var format = targetFile as Format;
                    var dialog = new DialogNewElement(format);

                    if (dialog.ShowDialog() == true)
                    {
                        Data.Project.ElementAdd(format, new CustomElement(dialog.ElementTtitle, dialog.Fields, dialog.ElementColor));
                    }
                }
            }
        }

        private void ImportSource(object sender, RoutedEventArgs e)
        {
            if(Data.Project != null)
            {
                var dialog = new DialogImportData();
                if (dialog.ShowDialog() == true)
                {
                    Data.Project.ImportDialogs(dialog.ImportPath);
                }
            }
        }

        private void EditProjectSettings(object sender, RoutedEventArgs e)
        {
            if(Data.Project != null)
            {
                var dialog = new DialogEditProject(Data.Project);
                if (dialog.ShowDialog() == true)
                {
                    Data.Project.EditSettings(dialog.TextTitle.Text, dialog.TextSourcePath.Text, dialog.IsSourcePathRelative);
                }
            }
        }

        private void ImportDialog(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog();
            dialog.Filter = "Dialog reactor source|*.xml|AuroraRL XLSX|*.xlsx|AuroraRL CSV|*.csv";
            dialog.FilterIndex = 1;
            dialog.RestoreDirectory = true;

            if (dialog.ShowDialog() == true)
            {
                if(Data.Project != null)
                {
                    var format = ImportFormat.DialogReactorXML;
                    switch (dialog.FilterIndex)
                    {
                        case 1: format = ImportFormat.DialogReactorXML; break;
                        case 2: format = ImportFormat.AuroraXLSX; break;
                        case 3: format = ImportFormat.AuroraCSV; break;
                    }

                    if(format == ImportFormat.DialogReactorXML)
                    {
                        Data.Project.ImportNativeXml(dialog.FileName);
                    }
                    else
                    {
                        Data.Project.ImportDialogTable(dialog.FileName, format);
                    }
                }
            }
        }

        private void OpenWithExplorer(object sender, RoutedEventArgs routedEvent)
        {
            if(Data.Project != null && treeView.SelectedItem != null)
            {
                var item = treeView.SelectedItem;
                string filePath = null;

                if (item is Dialog)
                {
                    filePath = (item as Dialog).XmlFilePath;
                }
                else if(item is ProjectFolder)
                {
                    filePath = Data.Project.GetFullFilePath(item as ProjectFile);
                }
                else if(item is ProjectTree)
                {
                    filePath = Data.Project.ProjectXmlPath;
                }

                if (!string.IsNullOrWhiteSpace(filePath))
                {
                    string openFolder = null;

                    if (File.Exists(filePath))
                    {
                        var info = new FileInfo(filePath);
                        openFolder = info.DirectoryName;
                    }
                    else if (Directory.Exists(filePath))
                    {
                        openFolder = filePath;
                    }

                    if (!string.IsNullOrWhiteSpace(openFolder) && Directory.Exists(openFolder))
                    {
                        try
                        {
                            Process.Start("explorer.exe", openFolder);
                        }
                        catch(Exception e)
                        {
                            Log("Unexpected error on opening with explorer.exe - " + e.StackTrace);
                        }
                    }
                }
            }
        }

        internal void OnDialogRemoved(Dialog dialog)
        {
            CloseDialogTab(dialog);

            if (ClosedTabs.Contains(dialog))
            {
                ClosedTabs.Remove(dialog);
            }
        }

        private bool TreeViewSelectedItemValidation()
        {
            if (Data.Project != null)
            {
                if (treeView.SelectedItem != null && treeView.SelectedItem is ProjectFile)
                {
                    return true;
                }
            }

            return false;
        }

        internal void ProgressBarStart(int maxValue)
        {
            StatusProgressBar.Maximum = maxValue;
            StatusProgressBar.Value = 0;
            StatusProgressBar.Visibility = Visibility.Visible;
        }

        internal void ProgressBarUpdate(int value)
        {
            StatusProgressBar.Value = value;
            Refresh();
        }

        internal void ProgressBarUpdateInc()
        {
            ProgressBarUpdate((int)Math.Min(StatusProgressBar.Maximum, StatusProgressBar.Value + 1));
        }

        internal void ProgressBarUpdateEnd()
        {
            StatusProgressBar.Visibility = Visibility.Hidden;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Delete:
                    HotKeyDelete();
                    break;

                case Key.LeftCtrl:
                case Key.RightCtrl:
                    IsKeyCtrlPressed = true;
                    break;

                case Key.Z:
                    HotKeyUndo();
                    break;

                case Key.Y:
                    HotKeyRedo();
                    break;

                case Key.A:
                    HotKeySelectAll();
                    break;

                case Key.C:
                    HotKeyCopy();
                    break;

                case Key.V:
                    HotKeyPaste();
                    break;

                case Key.X:
                    HotKeyCut();
                    break;

                case Key.T:
                    HotKeyRestoreLastClosedTab();
                    break;

                case Key.W:
                    HotKeyCloseCurrentTab();
                    break;
            }
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.LeftCtrl:
                case Key.RightCtrl:
                    IsKeyCtrlPressed = false;
                    break;
            }
        }


        private void EditUndo(object sender, RoutedEventArgs e)
        {
            if (tabControl.SelectedItem != null)
            {
                (tabControl.SelectedItem as ClosableTab).HotKeyUndo();
            }
        }

        private void EditRedo(object sender, RoutedEventArgs e)
        {
            if (tabControl.SelectedItem != null)
            {
                (tabControl.SelectedItem as ClosableTab).HotKeyRedo();
            }
        }

        private void HotKeyDelete()
        {
            if (tabControl.SelectedItem != null)
            { 
                (tabControl.SelectedItem as ClosableTab).HotKeyDelete();
            }
        }

        private void HotKeyUndo()
        {
            if (IsKeyCtrlPressed)
            {
                if (tabControl.SelectedItem != null)
                {
                    (tabControl.SelectedItem as ClosableTab).HotKeyUndo();
                }
            }
        }

        private void HotKeyRedo()
        {
            if (IsKeyCtrlPressed)
            {
                if (tabControl.SelectedItem != null)
                {
                    (tabControl.SelectedItem as ClosableTab).HotKeyRedo();
                }
            }
        }

        private void HotKeySelectAll()
        {
            if (IsKeyCtrlPressed)
            {
                var element = FocusManager.GetFocusedElement(this);
                if(element != null && element is TabItem && element == TabErrors)
                {
                    ErrorsLog.SelectAll();
                }
                else
                {
                    if (tabControl.SelectedItem != null)
                    {
                        (tabControl.SelectedItem as ClosableTab).SelectAll();
                    }
                }
            }
        }

        private void HotKeyCopy()
        {
            if (IsKeyCtrlPressed)
            {
                var element = FocusManager.GetFocusedElement(this);
                if (element != null && element is ListViewItem)
                {
                    ErrorsLog.CopySelected();
                }
                else
                {
                    if (tabControl.SelectedItem != null)
                    {
                        (tabControl.SelectedItem as ClosableTab).HotKeyCopy();
                    }
                }
            }
        }

        private void HotKeyPaste()
        {
            if (IsKeyCtrlPressed)
            {
                if (tabControl.SelectedItem != null)
                {
                    (tabControl.SelectedItem as ClosableTab).HotKeyPaste();
                }
            }
        }

        private void HotKeyCut()
        {
            if (IsKeyCtrlPressed)
            {
                if (tabControl.SelectedItem != null)
                {
                    (tabControl.SelectedItem as ClosableTab).HotKeyCut();
                }
            }
        }
        private void HotKeyRestoreLastClosedTab()
        {
            if (IsKeyCtrlPressed)
            {
                if(Data.Project != null)
                {
                    if(ClosedTabs.Count > 0)
                    {
                        var lastClosed = ClosedTabs[ClosedTabs.Count - 1];

                        if(lastClosed != null)
                        {
                            OpenDialog(lastClosed);
                        }
                    }
                }
            }
        }

        private void HotKeyCloseCurrentTab()
        {
            if (IsKeyCtrlPressed)
            {
                if(Data.Project != null)
                {
                    if(tabControl.SelectedItem != null && tabControl.SelectedItem is ClosableTab)
                    {
                        CloseDialogTab((tabControl.SelectedItem as ClosableTab).Dialog);
                    }
                }
            }
        }

        internal void Log(string line)
        {
            LogBox.Text = LogBox.Text + line + "\r\n";
            LogBox.ScrollToEnd();
        }

        private void ClearLogBox(object sender, RoutedEventArgs e)
        {
            LogBox.Text = string.Empty;
        }

        private void LogBox_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            LogBox.ContextMenu.IsOpen = true;
        }

        private void ClosedTabsHistoryAdd(Dialog dialog)
        {
            if(ClosedTabs.Count > ClosedTabHistorySize)
            {
                ClosedTabs.RemoveAt(0);
            }

            ClosedTabs.Add(dialog);
        }

        private void LogMessage(LogMessage message)
        {
            message.Id = ErrorsLog.Messages.Count + 1;
            ErrorsLog.Messages.Add(message);
            ErrorsLog.UpdateContextMenu();
            LogMessagesCounterUpdate();
        }

        public void LogLine(LogLevel level, string type, ProjectTree tree, Dialog dialog, string description, Element element)
        {
            var message = new LogMessage(level, type, description);
            element.ValidationError = true;
            message.File = dialog.GetRelativePath(tree);
            message.Dialog = dialog;
            message.Element = element;
            message.ElementId = element.Id;
            LogMessage(message);
        }

        public void LogLine(LogLevel level, string type, ProjectTree tree, Dialog dialog, string description)
        {
            var message = new LogMessage(level, type, description);
            message.File = dialog.GetRelativePath(tree);
            message.Dialog = dialog;
            LogMessage(message);
        }
        
        internal void LogMessage(LogLevel level, string type, string description)
        {
            var message = new LogMessage(level, type, description);
            LogMessage(message);
        }

        public void LogMessage(LogLevel level, string type, string description, string filePath)
        {
            var message = new LogMessage(level, type, description);
            message.File = filePath;
            LogMessage(message);
        }

        public void LogMessagesClear()
        {
            ErrorsLog.Messages.Clear();
            ErrorsLog.UpdateContextMenu();
            TabErrors.Header = "Errors";
        }

        public void LogMessagesCounterUpdate()
        {
            TabErrors.Header = "Errors (" + ErrorsLog.Messages.Count + ")";
        }

        private void ClosedTabsHistoryUpdate()
        {
            if(ClosedTabs.Count == 0)
            {
                ClosedTabsMenu.IsEnabled = false;
            }
            else
            {
                ClosedTabsMenu.IsEnabled = true;

                if(ClosedTabsMenu.ContextMenu == null)
                {
                    ClosedTabsMenu.ContextMenu = new ContextMenu();
                }

                var menu = ClosedTabsMenu.ContextMenu;
                menu.Items.Clear();

                for(int i = ClosedTabs.Count - 1; i >= 0; --i)
                {
                    var dialog = ClosedTabs[i];
                    var item = new MenuItem();

                    item.Header = dialog.Title;
                    item.Tag = dialog;
                    item.Click += RestoreTab;

                    menu.Items.Add(item);
                }

                menu.Items.Add(new Separator());

                var clear = new MenuItem();
                clear.Header = "Clear history";
                clear.Click += CliearClosedTabsHistory;
                menu.Items.Add(clear);
            }
        }

        private void CliearClosedTabsHistory(object sender, RoutedEventArgs e)
        {
            if(ClosedTabs.Count > 0)
            {
                ClosedTabs.Clear();
                ClosedTabsHistoryUpdate();
            }
        }

        private void RestoreTab(object sender, RoutedEventArgs e)
        {
            ClosedTabsMenu.ContextMenu.IsOpen = false;

            if (sender is MenuItem)
            {
                var item = sender as MenuItem;

                if(item.Tag != null && item.Tag is Dialog)
                {
                    var dialog = item.Tag as Dialog;

                    if (ClosedTabs.Contains(dialog))
                    {
                        ClosedTabs.Remove(dialog);
                        ClosedTabsHistoryUpdate();

                        OpenDialog(dialog);
                    }
                }
            }
        }

        private void ClosedTabsHistoryShow(object sender, RoutedEventArgs e)
        {
            if(ClosedTabsMenu.ContextMenu != null)
            {
                ClosedTabsMenu.ContextMenu.IsOpen = true;
            }
        }

        public void ValidateDialog(Dialog dialog)
        {
            if (Data.Project != null)
            {
                ErrorsLog.ValidateDialog(Data.Project.ProjectTree, dialog);
            }
        }
    }
}