﻿namespace DialogReactor
{
    internal static class Version
    {
        private const int YEAR = 2016;
        private const string MONTH = "December";

        public const int MAJOR = 0;
        public const int MINOR = 2;

        private const int BUILD = 70;

        internal static string Title()
        {
            return MAJOR + "." + MINOR + " build: " + BUILD + " (" + MONTH + ", " + YEAR + ")";
        }

        internal static string Year
        {
            get { return "" + YEAR; }
        }

        internal static string Major
        {
            get { return "" + MAJOR; }
        }

        internal static string Minor
        {
            get { return "" + MINOR; }
        }
    }
}
