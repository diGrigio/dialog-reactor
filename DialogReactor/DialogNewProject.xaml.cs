﻿using System.IO;
using System.Windows;
using System.Windows.Forms;

namespace DialogReactor
{
    public partial class DialogNewProject : Window
    {
        public string NewProjectTitle { get { return TextTitle.Text; } }
        public string NewProjectPath { get { return TextPath.Text; } }
        public string NewProjectSourceFolder { get { return "\\" + TextSourceFolder.Text + "\\"; } }

        public DialogNewProject()
        {
            InitializeComponent();
        }

        private void ButtonOk_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void ButtonSelectPath_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                TextPath.Text = dialog.SelectedPath + "\\";
                ValidateInput();
            }
        }

        private void TextTitle_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            ValidateInput();
        }

        private void TextPath_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            ValidateInput();
        }

        private void ValidateInput()
        {
            if(TextTitle == null || TextPath == null)
            {
                return;
            }

            if(ValidateTitle() && ValidatePath() && ValidateSource())
            {
                LabelError.Content = string.Empty;
                ButtonOk.IsEnabled = true;
            }
            else
            {
                ButtonOk.IsEnabled = false;
            }
        }

        private bool ValidateTitle()
        {
            if (string.IsNullOrWhiteSpace(TextTitle.Text))
            {
                LabelError.Content = "Title is empty";
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool ValidatePath()
        {
            if (string.IsNullOrWhiteSpace(TextPath.Text))
            {
                LabelError.Content = "Path is empty";
                return false;
            }
            else if (!Directory.Exists(TextPath.Text))
            {
                LabelError.Content = "Directory not found";
                return false;
            }
            else if (File.Exists(TextPath.Text + TextTitle.Text.ToLower() + ".dialog"))
            {
                LabelError.Content = "Project with this title already exist";
                return false;
            }
            else
            {
                string path = TextPath.Text + TextTitle.Text.ToLower() + ".dialog";
                bool createFilePossibility = false;
                try
                {
                    using (File.Create(path)) { }
                    File.Delete(path);
                    createFilePossibility = true;
                }
                catch
                {
                    createFilePossibility = false;
                }

                if (!createFilePossibility)
                {
                    LabelError.Content = "Project file cant create here";
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        private bool ValidateSource()
        {
            if (string.IsNullOrWhiteSpace(TextSourceFolder.Text))
            {
                LabelError.Content = "Source folder cant be empty";
                return false;
            }

            var path = TextPath.Text + TextSourceFolder.Text + "\\";
            if (Directory.Exists(path))
            {
                return true;
            }
            else
            {
                bool createDirPossibility = false;
                try
                {
                    Directory.CreateDirectory(path);
                    Directory.Delete(path);
                    createDirPossibility = true;
                }
                catch
                {
                    createDirPossibility = false;
                }

                if (createDirPossibility)
                {
                    return true;
                }
                else
                {
                    LabelError.Content = "Cant create this source folder";
                    return false;
                }
            }
        }
    }
}