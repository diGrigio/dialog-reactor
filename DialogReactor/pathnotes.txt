﻿b70 [0.2]
- Added format, elements and elements fields adding

b69 [0.2]
- Generalization of supported file types in Project

b68 [0.2]
- Refactoring

b67 [0.2]
- Update element editor

b66 [0.2]
- Added custom element fields editor

b65 [0.1]
- Added import of native dialog .xml format

b64 [0.1]
- Links indication after element selecting

b63 [0.1]
- Added "open file with explorer.exe" by context menu command

b62 [0.1]
- Validation error indication on elements

b61 [0.1]
- On edit elements validation

b60 [0.1]
- "dialogList.csv not found" error not logged if target folder is empty
- Fixed drag-ignore area in state and answer elements
- Unused states shifted from startDialog element after importing

b59 [0.1]
- Added errors log messages from import process

b58 [0.1]
- Added errors log selecting and copy to clipboard

b57 [0.1]
- Added closed tab history
- Added hotkeys Ctrl+W to close current tab, Ctrl+T to restore last closed tab

b56 [0.1]
- Fixed an bug on which the source file was cleaned after the import without reloaded the project
- Now answer element can get links only from one state

b55 [0.1]
- Added empty text warnings
- Refactoring table import code
- Reworked graph drawing algorithm
- Fixed import folder without subfolders bug

b54 [0.1]
- Added errors count indicator
- Close all tabs from tab header context menu
- Little design changes

b53 [0.1]
- Added errors log
- Added some export validation features

b52 [0.1]
- Fixed\hacked encoding detection bug
- Updated UDE lib version

b51 [0.1]
- Added progress bar updating while opening dialog
- Added copy\paste\cut of selected elements and links

b50 [0.1]
- Added resizing of text content elements (Answers and NpcStatements)

b49 [0.1]
- Added progress bar updating of project opening, folders removing, importing tables and sources, project exporting

b48 [0.1]
- Added Undo\Redo icons
- Сhanged behavior of Undo\Redo buttons
- Merged undo\redo of elements group movement action

b47 [0.1]
- Added import .xlsx dialog tables

b46 [0.1]
- Added group elements movement

b45 [0.1]
- Added multiply elements and links selecting

b44 [0.1]
- Refactoring elements selecting code

b43 [0.1]
- Refactroing elements drag code

b42 [0.1]
- Added undo\redo to edit menu
- Added cursor changing while drag dialog element

b41 [0.1]
- Fixed rendering and main thread synchronize
- Fixed autosave after element data editing
- Added udno\redo to dialog editor (elements [add, remove, move] and links [add, remove, replace] actions)

b40 [0.1]
- Autoclose tab on try to add new dialog if it exist in project, but not exist source file
- Added import directory auto creation
- Added tooltips
- Fixed numeric validation in Player Answer edit dialog

b39 [0.1]
- Added project settings edit dialog
- Added project select source path
- Files will not removed from .dialog file if file not found in file system

b38 [0.1]
- Added horizontal scroll bar to project treeview
- Fixed autosave dialog file by every movement tic
- Changed ZIndex to Control of Links (ZIndex=0), DummyLink (ZIndex=2), Elements (ZIndexNormal=1, ZIndexSelected=2)
- Removed hack in ScrollableCanvas.AddLink(). But problem still need to fix
- Fixed IOException on save and load dialog xml files and AuroraCSV export