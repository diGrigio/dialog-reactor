﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace DialogReactor
{
    public partial class ControlAnswer : UserControl, IControlElement, ILocalable, IEdited, IResizeble
    {
        private static SolidColorBrush NormalBorderBrush = new BrushConverter().ConvertFrom("#e67e22") as SolidColorBrush;

        // IControlElement
        private Element Element;

        // ILocalable
        private Localization Locale = Localization.RU;
        Localization ILocalable.Locale
        {
            get { return Locale; }

            set
            {
                Locale = value;
                UpdatePreview();
            }
        }

        public ControlAnswer(ElementAnswer element)
        { 
            InitialControl();

            Element = element;
            Element.Control = this;
        }

        private void InitialControl()
        {
            InitializeComponent();
            BuildContextMenu();
        }

        private void BuildContextMenu()
        {
            var menu = new ContextMenu();

            // 
            var edit = new MenuItem();
            var remove = new MenuItem();
            var addDialogEnd = new MenuItem();

            edit.Header = "Edit";
            remove.Header = "Remove";

            edit.Click += Edit;
            remove.Click += Remove;

            menu.Items.Add(edit);
            menu.Items.Add(remove);

            ContextMenu = menu;
        }

        public Element GetContainedElement()
        {
            return Element;
        }

        public void SetContainedElement(Element element)
        {
            Element = element;
        }

        public Shape GetInAnchor()
        {
            return LinkIn;
        }

        public Shape GetOutAnchor()
        {
            return LinkOut;
        }

        public void UpdatePreview()
        {
            if (Locale == Localization.RU)
            {
                TextPreview.Text = (Element as ElementAnswer).TextRU;
            }
            else if (Locale == Localization.ENG)
            {
                TextPreview.Text = (Element as ElementAnswer).TextEng;
            }
        }

        private void UserControl_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            var canvas = Parent as ScrollableCanvas;
            canvas.SelectControl(this);

            ContextMenu.PlacementTarget = sender as ControlAnswer;
            ContextMenu.IsOpen = true;
        }

        private void Remove(object sender, RoutedEventArgs e)
        {
            (Parent as ScrollableCanvas).RemoveControl(this, true);
        }

        private void Edit(object sender, RoutedEventArgs e)
        {
            Edit();
        }

        public void UpdateLinks(ScrollableCanvas canvas)
        {
            if(Element.InputLinks.Count > 0)
            {
                var point = canvas.GetPosition(LinkIn);

                foreach (Link link in Element.InputLinks)
                {
                    link.Control.Arrow.X2 = point.X;
                    link.Control.Arrow.Y2 = point.Y - 10;
                }
            }

            if (Element.OutputLinks.Count > 0)
            {
                var point = canvas.GetPosition(LinkOut);

                foreach (Link link in Element.OutputLinks)
                {
                    link.Control.Arrow.X1 = point.X;
                    link.Control.Arrow.Y1 = point.Y + 5;
                }
            }
        }

        private void LinkOut_MouseEnter(object sender, MouseEventArgs e)
        {
            LinkOut.Fill = new SolidColorBrush(Colors.Red);
        }

        private void LinkOut_MouseLeave(object sender, MouseEventArgs e)
        {
            LinkOut.Fill = new SolidColorBrush(Colors.Green);
        }

        public void Edit()
        {
            var dialog = new DialogEditAnswer(Element as ElementAnswer);

            if (dialog.ShowDialog() == true)
            {
                TextPreview.Text = dialog.TextRU;

                var element = Element as ElementAnswer;
                if (element != null)
                {
                    element.Priority = dialog.Priority;
                    element.TextRU = dialog.TextRU;
                    element.TextEng = dialog.TextEng;
                    element.Conditions = dialog.Conditions;
                    element.EndDialogKey = dialog.EndDialogKey;
                    element.AnswerFlags = dialog.Flag;
                    UpdatePreview();
                    (Parent as ScrollableCanvas).SaveDialog();
                }
            }
        }

        public void Select()
        {
            HeaderRectangle.Fill = Brushes.Red;
            borderHeader.BorderBrush = Brushes.Red;
            borderText.BorderBrush = Brushes.Red;
        }

        public void Unselect()
        {
            HeaderRectangle.Fill = NormalBorderBrush;
            borderHeader.BorderBrush = NormalBorderBrush;
            borderText.BorderBrush = NormalBorderBrush;
        }

        public bool Removable()
        {
            return true;
        }

        public double ResizeMinWidth()
        {
            return 247;
        }

        public double ResizeMinHeight()
        {
            return 105;
        }

        public double ResizeMaxWidth()
        {
            return 1000;
        }

        public double ResizeMaxHeight()
        {
            return 1000;
        }

        public UIElement ResizeControl()
        {
            return ResizeAnchor;
        }

        public bool CanCopyPaste()
        {
            return true;
        }

        public void ValidationError(bool value)
        {
            if (value)
            {
                ValidationIcon.Visibility = Visibility.Visible;
            }
            else
            {
                ValidationIcon.Visibility = Visibility.Hidden;
            }
        }
    }
}
