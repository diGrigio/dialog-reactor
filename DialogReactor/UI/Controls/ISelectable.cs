﻿namespace DialogReactor
{
    public interface ISelectable
    {
        void Select();
        void Unselect();

        bool CanCopyPaste();
    }
}
