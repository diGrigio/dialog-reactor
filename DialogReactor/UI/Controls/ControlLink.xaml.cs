﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace DialogReactor
{
    public partial class ControlLink : ISelectable
    {
        private ScrollableCanvas Canvas;

        public ArrowLine Arrow;

        public Link Link { get; private set; }
        public IControlElement From { get; private set; }
        public IControlElement To { get; private set; }

        public ControlLink(IControlElement from, IControlElement to, Point fromPoint, Point toPoint, ScrollableCanvas canvas)
        {
            Canvas = canvas;
            From = from;
            To = to;

            Arrow = new ArrowLine(canvas);
            Arrow.Link = this;
            
            Arrow.X1 = fromPoint.X ;
            Arrow.Y1 = fromPoint.Y + 5;
            Arrow.X2 = toPoint.X ;
            Arrow.Y2 = toPoint.Y - 10;
            Arrow.ArrowEnds = ArrowEnds.End;
            Arrow.Stroke = ArrowLine.GetArrowColor(from);// Brushes.Green;
            Arrow.StrokeThickness = 2;

            Link = new Link(from.GetContainedElement(), to.GetContainedElement());
            Link.Control = this;
        }

        public void Select()
        {
            Arrow.Select();
        }

        public void Unselect()
        {
            Arrow.Unselect();
        }

        public bool CanCopyPaste()
        {
            return true;
        }
    }

    public enum ArrowEnds
    {
        None = 0,
        Start = 1,
        End = 2,
        Both = 3
    }

    public class ArrowLine : Shape, ISelectable
    {
        public ControlLink Link { get; set; }
        public IControlElement From { get; set; }

        private ScrollableCanvas Canvas { get; set; }

        private PathGeometry pathgeo;
        private PathFigure pathfigLine;
        private PolyLineSegment polysegLine;

        private PathFigure pathfigHead1;
        private PolyLineSegment polysegHead1;
        private PathFigure pathfigHead2;
        private PolyLineSegment polysegHead2;

        public static readonly DependencyProperty ArrowAngleProperty =
            DependencyProperty.Register("ArrowAngle",
                typeof(double), typeof(ArrowLine),
                new FrameworkPropertyMetadata(45.0,
                        FrameworkPropertyMetadataOptions.AffectsMeasure));

        public double ArrowAngle
        {
            set { SetValue(ArrowAngleProperty, value); }
            get { return (double)GetValue(ArrowAngleProperty); }
        }

        public static readonly DependencyProperty ArrowLengthProperty =
            DependencyProperty.Register("ArrowLength",
                typeof(double), typeof(ArrowLine),
                new FrameworkPropertyMetadata(12.0,
                        FrameworkPropertyMetadataOptions.AffectsMeasure));


        public static readonly DependencyProperty ArrowEndsProperty =
            DependencyProperty.Register("ArrowEnds",
                typeof(ArrowEnds), typeof(ArrowLine),
                new FrameworkPropertyMetadata(ArrowEnds.End,
                        FrameworkPropertyMetadataOptions.AffectsMeasure));


        public static readonly DependencyProperty IsArrowClosedProperty =
            DependencyProperty.Register("IsArrowClosed",
                typeof(bool), typeof(ArrowLine),
                new FrameworkPropertyMetadata(false,
                        FrameworkPropertyMetadataOptions.AffectsMeasure));

        public static readonly DependencyProperty X1Property =
            DependencyProperty.Register("X1",
                typeof(double), typeof(ArrowLine),
                new FrameworkPropertyMetadata(0.0,
                        FrameworkPropertyMetadataOptions.AffectsMeasure));


        public static readonly DependencyProperty Y1Property =
            DependencyProperty.Register("Y1",
                typeof(double), typeof(ArrowLine),
                new FrameworkPropertyMetadata(0.0,
                        FrameworkPropertyMetadataOptions.AffectsMeasure));

        public static readonly DependencyProperty X2Property =
            DependencyProperty.Register("X2",
                typeof(double), typeof(ArrowLine),
                new FrameworkPropertyMetadata(0.0,
                        FrameworkPropertyMetadataOptions.AffectsMeasure));


        public static readonly DependencyProperty Y2Property =
            DependencyProperty.Register("Y2",
                typeof(double), typeof(ArrowLine),
                new FrameworkPropertyMetadata(0.0,
                        FrameworkPropertyMetadataOptions.AffectsMeasure));

        public double ArrowLength
        {
            set { SetValue(ArrowLengthProperty, value); }
            get { return (double)GetValue(ArrowLengthProperty); }
        }
        public ArrowEnds ArrowEnds
        {
            set { SetValue(ArrowEndsProperty, value); }
            get { return (ArrowEnds)GetValue(ArrowEndsProperty); }
        }

        public double X1
        {
            set { SetValue(X1Property, value); }
            get { return (double)GetValue(X1Property); }
        }
        public double Y1
        {
            set { SetValue(Y1Property, value); }
            get { return (double)GetValue(Y1Property); }
        }

        public double X2
        {
            set { SetValue(X2Property, value); }
            get { return (double)GetValue(X2Property); }
        }

        public double Y2
        {
            set { SetValue(Y2Property, value); }
            get { return (double)GetValue(Y2Property); }
        }

        public bool IsArrowClosed
        {
            set { SetValue(IsArrowClosedProperty, value); }
            get { return (bool)GetValue(IsArrowClosedProperty); }
        }

        protected override Geometry DefiningGeometry
        {
            get
            {
                pathgeo.Figures.Clear();

                pathfigLine.StartPoint = new Point(X1, Y1);
                polysegLine.Points.Clear();
                polysegLine.Points.Add(new Point(X2, Y2));
                pathgeo.Figures.Add(pathfigLine);

                int count = polysegLine.Points.Count;
                if (count > 0)
                {
                    if ((ArrowEnds & ArrowEnds.Start) == ArrowEnds.Start)
                    {
                        Point pt1 = pathfigLine.StartPoint;
                        Point pt2 = polysegLine.Points[0];
                        pathgeo.Figures.Add(CalculateArrow(pathfigHead1, pt2, pt1));
                    }

                    if ((ArrowEnds & ArrowEnds.End) == ArrowEnds.End)
                    {
                        Point pt1 = count == 1 ? pathfigLine.StartPoint : polysegLine.Points[count - 2];
                        Point pt2 = polysegLine.Points[count - 1];
                        pathgeo.Figures.Add(CalculateArrow(pathfigHead2, pt1, pt2));
                    }
                }
                return pathgeo;
            }
        }

        public ArrowLine(ScrollableCanvas canvas)
        {
            Canvas = canvas;
            pathgeo = new PathGeometry();

            pathfigLine = new PathFigure();
            polysegLine = new PolyLineSegment();
            pathfigLine.Segments.Add(polysegLine);

            pathfigHead1 = new PathFigure();
            polysegHead1 = new PolyLineSegment();
            pathfigHead1.Segments.Add(polysegHead1);

            pathfigHead2 = new PathFigure();
            polysegHead2 = new PolyLineSegment();
            pathfigHead2.Segments.Add(polysegHead2);

            RenderOptions.SetEdgeMode(polysegLine, EdgeMode.Aliased);
            RenderOptions.SetEdgeMode(polysegHead1, EdgeMode.Aliased);
            RenderOptions.SetEdgeMode(polysegHead2, EdgeMode.Aliased);

            BuildContextMenu();
        }

        private void BuildContextMenu()
        {
            var menu = new ContextMenu();
            var remove = new MenuItem();
            remove.Header = "Remove";
            remove.Click += Remove;

            menu.Items.Add(remove);
            ContextMenu = menu;
        }

        private PathFigure CalculateArrow(PathFigure pathfig, Point pt1, Point pt2)
        {
            Matrix matx = new Matrix();
            Vector vect = pt1 - pt2;
            vect.Normalize();
            vect *= ArrowLength;

            PolyLineSegment polyseg = pathfig.Segments[0] as PolyLineSegment;
            polyseg.Points.Clear();
            matx.Rotate(ArrowAngle / 2);
            pathfig.StartPoint = pt2 + vect * matx;
            polyseg.Points.Add(pt2);

            matx.Rotate(-ArrowAngle);
            polyseg.Points.Add(pt2 + vect * matx);
            pathfig.IsClosed = IsArrowClosed;

            return pathfig;
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            if (Link != null)
            {
                Canvas.SelectControl(Link);
            }
        }

        protected override void OnMouseRightButtonDown(MouseButtonEventArgs e)
        {
            if(Link != null)
            {
                ContextMenu.IsOpen = true;
            }
        }
        
        private void Remove(object sender, RoutedEventArgs e)
        {
            if(Link != null && Canvas != null)
            {
                Canvas.RemoveLink(Link, true, true);
            }
        }

        public void Select()
        {
            Stroke = Brushes.Red;
        }

        public void Unselect()
        {
            if(Link != null && Link.From != null)
            {
                Stroke = GetArrowColor(Link.From);
            }
        }

        public static Brush GetArrowColor(IControlElement from)
        {
            string hexColor = "#ff000000";

            if (from is ControlDialogStart)
            {
                hexColor = "#E527AE60";
            }
            else if (from is ControlNpcStatement)
            {
                hexColor = "#E52980B9";
            }
            else if (from is ControlAnswer)
            {
                hexColor = "#E5E67E22";
            }

            return new BrushConverter().ConvertFrom(hexColor) as SolidColorBrush;
        }

        public bool CanCopyPaste()
        {
            return true;
        }
    }
}