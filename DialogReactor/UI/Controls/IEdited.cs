﻿namespace DialogReactor
{
    public interface IEdited
    {
        void Edit();
    }
}
