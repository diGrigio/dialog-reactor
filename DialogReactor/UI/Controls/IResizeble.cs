﻿using System.Windows;

namespace DialogReactor
{
    interface IResizeble
    {
        UIElement ResizeControl();

        double ResizeMinWidth();
        double ResizeMinHeight();

        double ResizeMaxWidth();
        double ResizeMaxHeight();
    }
}
