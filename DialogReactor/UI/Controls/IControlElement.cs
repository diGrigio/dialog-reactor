﻿using System.Windows.Shapes;

namespace DialogReactor
{
    public interface IControlElement : ISelectable
    {
        Element GetContainedElement();
        void SetContainedElement(Element element);

        Shape GetInAnchor();
        Shape GetOutAnchor();

        void UpdateLinks(ScrollableCanvas canvas);
        
        bool Removable();
        void ValidationError(bool value);
    }
}
