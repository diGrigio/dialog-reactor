﻿namespace DialogReactor
{
    public enum Localization
    {
        ENG,
        RU
    }

    interface ILocalable
    {
        Localization Locale { get; set; }
    }
}
