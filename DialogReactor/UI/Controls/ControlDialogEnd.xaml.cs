﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace DialogReactor
{
    public partial class ControlDialogEnd : UserControl, IControlElement
    {
        private static SolidColorBrush NormalBorderBrush = new BrushConverter().ConvertFrom("#E74C3C") as SolidColorBrush;

        private Element Element;

        public ControlDialogEnd(ElementDialogEnd element)
        {
            InitialControl();

            Element = element;
            Element.Control = this;
        }

        private void InitialControl()
        {
            InitializeComponent();
            BuildContextMenu();
        }

        private void BuildContextMenu()
        {
            var menu = new ContextMenu();

            var remove = new MenuItem();
            var addDialogEnd = new MenuItem();
            
            remove.Header = "Remove";
            remove.Click += Remove;
            menu.Items.Add(remove);

            ContextMenu = menu;
        }

        public Element GetContainedElement()
        {
            return Element;
        }

        public void SetContainedElement(Element element)
        {
            Element = element;
        }

        public Shape GetInAnchor()
        {
            return LinkIn;
        }

        public Shape GetOutAnchor()
        {
            throw new NotImplementedException();
        }

        private void Remove(object sender, RoutedEventArgs e)
        {
            (Parent as ScrollableCanvas).RemoveControl(this, true);
        }

        private void UserControl_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            var canvas = Parent as ScrollableCanvas;
            canvas.SelectControl(this);

            ContextMenu.PlacementTarget = sender as ControlAnswer;
            ContextMenu.IsOpen = true;
        }
        
        public void UpdateLinks(ScrollableCanvas canvas)
        {
            if (Element.InputLinks.Count > 0)
            {
                var point = canvas.GetPosition(LinkIn);

                foreach (Link link in Element.InputLinks)
                {
                    link.Control.Arrow.X2 = point.X;
                    link.Control.Arrow.Y2 = point.Y - 10;
                }
            }
        }

        public void Select()
        {
            Header.Fill = Brushes.Red;
        }

        public void Unselect()
        {
            Header.Fill = NormalBorderBrush;
        }

        public bool Removable()
        {
            return true;
        }

        public bool CanCopyPaste()
        {
            return true;
        }

        public void ValidationError(bool value)
        {
            if (value)
            {
                ValidationIcon.Visibility = Visibility.Visible;
            }
            else
            {
                ValidationIcon.Visibility = Visibility.Hidden;
            }
        }
    }
}
