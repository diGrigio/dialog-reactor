﻿using DialogReactor.Util.Validation;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace DialogReactor
{
    public partial class ErrorLog : UserControl
    {
        private MainWindow Window;

        public ObservableCollection<LogMessage> Messages { get; private set; }

        public ErrorLog()
        {
            InitializeComponent();

            Messages = new ObservableCollection<LogMessage>();
            List.ItemsSource = Messages;
        }

        public void Init(MainWindow window)
        {
            Window = window;
        }

        private void ItemSelect(object sender, MouseButtonEventArgs e)
        {
            if(e.ClickCount == 2)
            {
                var item = sender as ListViewItem;
                if (item != null && item.IsSelected)
                {
                    if(List.SelectedIndex < Messages.Count)
                    {
                        var line = Messages[List.SelectedIndex];

                        if(line != null && line.Dialog != null)
                        { // find and select invalid element
                            Window.OpenDialog(line.Dialog);
                            Window.Refresh();
                            Window.ScrollToElement(line.ElementId);
                            Window.SelectElement(line.ElementId);
                        }
                    }
                }
            }
        }

        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateContextMenu();
        }

        private void MenuItemSelectAll(object sender, RoutedEventArgs e)
        {
            SelectAll();
        }

        private void MenuItemCopy(object sender, RoutedEventArgs e)
        {
            CopySelected();
        }

        public void UpdateContextMenu()
        {
            if (ContextMenu == null)
            {
                ContextMenu = Resources["SelectedItemsContextMenu"] as ContextMenu;
            }

            var selectAll = ContextMenu.Items.GetItemAt(0) as MenuItem;
            var copy = ContextMenu.Items.GetItemAt(1) as MenuItem;

            if (List.SelectedItems.Count == 0)
            {
                selectAll.IsEnabled = false;
                copy.IsEnabled = false;
            }
            else
            {
                selectAll.IsEnabled = true;
                copy.IsEnabled = true;
            }
        }

        public void SelectAll()
        {
            if (List.Items.Count > 0)
            {
                List.SelectAll();
                UpdateContextMenu();
            }
        }

        public void CopySelected()
        {
            if (List.SelectedItems.Count > 0)
            {
                var builder = new StringBuilder();

                foreach (var element in List.SelectedItems)
                {
                    if (element != null)
                    {
                        var line = element as LogMessage;
                        builder.Append(line.MessageType).Append("; ")
                            .Append(line.Id).Append("; ")
                            .Append(line.Description).Append("; ")
                            .Append(line.File).Append("; ")
                            .Append("element id=").Append(line.ElementId).Append(';')
                            .Append('\n');
                    }
                }

                // copy
                Clipboard.Clear();
                Clipboard.SetText(builder.ToString());
            }
        }

        public void ValidateDialog(ProjectTree tree, Dialog dialog)
        {
            DialogValidation.ValidateDialog(Window, Messages, tree, dialog);
        }
    }
}
