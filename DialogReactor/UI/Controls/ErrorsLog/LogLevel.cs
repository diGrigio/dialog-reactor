﻿namespace DialogReactor
{
    public enum LogLevel
    {
        Null,
        Error,
        Warning,
        Info
    }
}
