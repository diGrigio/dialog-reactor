﻿namespace DialogReactor
{
    public class LogMessage
    {
        const string TypeErrorIconPath = "/Dialog Reactor;component/Images/icon_error_16px.png";
        const string TypeWarningIconPath = "/Dialog Reactor;component/Images/icon_warning_16px.png";
        const string TypeInfoIconPath = "/Dialog Reactor;component/Images/icon_info_16px.png";

        public LogLevel MessageType { get; private set; }
        public string Icon { get; private set; }

        public int Id { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string File { get; set; }
        public int ElementId { get; set; }

        public Dialog Dialog { get; set; }
        public Element Element { get; set; }

        public LogMessage(LogLevel logLevel, string type, string description)
        {
            Id = 0;
            Type = type;
            File = string.Empty;
            ElementId = 0;

            MessageType = logLevel;
            SetIcon(logLevel);
            Description = description;
        }

        public void SetIcon(LogLevel type)
        {
            switch (type)
            {
                case LogLevel.Null:
                    Icon = string.Empty;
                    break;

                case LogLevel.Error:
                    Icon = TypeErrorIconPath;
                    break;

                case LogLevel.Warning:
                    Icon = TypeWarningIconPath;
                    break;

                case LogLevel.Info:
                    Icon = TypeInfoIconPath;
                    break;      
            }
        }
    }
}
