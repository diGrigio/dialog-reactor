﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace DialogReactor
{
    public class ClosableTab : TabItem
    {
        const double ScrollSpeedRate = 0.6;

        public Dialog Dialog { get; private set; }

        public string Title { set { (Header as CloseableTabItemHeader).label_TabTitle.Content = value; } }

        private CloseableTabItemHeader TabHeader;
        private ScrollViewer Viewer;
        private ScrollableCanvas Canvas;
        public MainWindow DialogReactorWindow { get; private set; }

        public ClosableTab(MainWindow window, Dialog dialog)
        {
            DialogReactorWindow = window;
            Dialog = dialog;

            // header
            TabHeader = new CloseableTabItemHeader();
            Header = TabHeader;

            TabHeader.button_close.MouseEnter += new MouseEventHandler(button_close_MouseEnter);
            TabHeader.button_close.MouseLeave += new MouseEventHandler(button_close_MouseLeave);
            TabHeader.button_close.Click += new RoutedEventHandler(button_close_Click);
            TabHeader.label_TabTitle.SizeChanged += new SizeChangedEventHandler(label_TabTitle_SizeChanged);
            
            // scroll viwer
            Viewer = new ScrollViewer();
            Viewer.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            Viewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
            Viewer.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FAFAFA"));
            AddChild(Viewer);

            // canvas
            Canvas = new ScrollableCanvas(DialogReactorWindow, this, Dialog);
            Viewer.Content = Canvas;

            BuildContextMenu();
        }

        private void BuildContextMenu()
        {
            var menu = new ContextMenu();

            // 
            var close = new MenuItem();
            var closeAll = new MenuItem();

            close.Header = "Close tab";
            closeAll.Header = "Close all tabs";

            close.Click += CloseTab;
            closeAll.Click += CloseAllTabs;

            menu.Items.Add(close);
            menu.Items.Add(closeAll);

            TabHeader.ContextMenu = menu;
        }

        private void CloseAllTabs(object sender, RoutedEventArgs e)
        {
            DialogReactorWindow.CloseAllTabs();
        }

        private void CloseTab(object sender, RoutedEventArgs e)
        {
            DialogReactorWindow.CloseDialogTab(Dialog);
        }

        protected override void OnSelected(RoutedEventArgs e)
        {
            base.OnSelected(e);
            (Header as CloseableTabItemHeader).button_close.Visibility = Visibility.Visible;
        }

        protected override void OnUnselected(RoutedEventArgs e)
        {
            base.OnUnselected(e);
            (Header as CloseableTabItemHeader).button_close.Visibility = Visibility.Hidden;
        }

        protected override void OnMouseEnter(MouseEventArgs e)
        {
            base.OnMouseEnter(e);
            (Header as CloseableTabItemHeader).button_close.Visibility = Visibility.Visible;
        }

        protected override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);
            if (!IsSelected)
            {
                (Header as CloseableTabItemHeader).button_close.Visibility = Visibility.Hidden;
            }
        }

        void button_close_MouseEnter(object sender, MouseEventArgs e)
        {
            (Header as CloseableTabItemHeader).button_close.Foreground = Brushes.Red;
        }

        void button_close_MouseLeave(object sender, MouseEventArgs e)
        {
            (Header as CloseableTabItemHeader).button_close.Foreground = Brushes.Black;
        }

        void button_close_Click(object sender, RoutedEventArgs e)
        {
            DialogReactorWindow.CloseDialogTab(Dialog);
        }

        void label_TabTitle_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            (Header as CloseableTabItemHeader).button_close.Margin = new Thickness((Header as CloseableTabItemHeader).label_TabTitle.ActualWidth + 5, 3, 4, 0);
        }

        protected override void OnPreviewMouseWheel(MouseWheelEventArgs e)
        {
            if (DialogReactorWindow.IsKeyCtrlPressed)
            {
                e.Handled = true;
                Canvas.Zoom(e);
            }
            else
            {
                base.OnPreviewMouseWheel(e);
            }
        }

        internal void HotKeyDelete()
        {
            Canvas.HotKeyDelete();
        }

        internal void HotKeyUndo()
        {
            Canvas.Undo();
        }
        
        internal void HotKeyRedo()
        {
            Canvas.Redo();
        }

        internal void HotKeyCopy()
        {
            Canvas.Copy();
        }

        internal void HotKeyPaste()
        {
            Canvas.Paste();
        }

        internal void HotKeyCut()
        {
            Canvas.Cut();
        }
        
        internal void AddElementToTab(IControlElement control, int posX, int posY, bool saveDialog, bool cancelable)
        {
            if(control != null && control is UserControl)
            {
                Canvas.AddElementToCanvas(control as UserControl, posX, posY, saveDialog, cancelable);
            }
        }

        internal void UpdateSize()
        {
            Canvas.UpdateSize();
        }

        internal void AddLinkToTab(IControlElement from, IControlElement to, bool saveDialog, bool cancelable)
        {
            Canvas.AddLink(from, to, saveDialog, cancelable);
        }

        public void Scroll(double dx, double dy)
        {
            if(dx > 0)
            {
                Viewer.ScrollToHorizontalOffset(Viewer.HorizontalOffset + ScrollSpeedRate);
            }
            else if(dx < 0)
            {
                Viewer.ScrollToHorizontalOffset(Viewer.HorizontalOffset - ScrollSpeedRate);
            }

            if(dy > 0)
            {
                Viewer.ScrollToVerticalOffset(Viewer.VerticalOffset + ScrollSpeedRate);
            }
            else if(dy < 0)
            {
                Viewer.ScrollToVerticalOffset(Viewer.VerticalOffset - ScrollSpeedRate);
            }
        }

        public void ScrollToElementById(int elementId)
        {
            foreach(Element element in Dialog.Elements)
            {
                if(element.Id == elementId)
                {
                    var control = element.Control;
                    if(control != null)
                    {
                        var userControl = control as UserControl;
                        double x = System.Windows.Controls.Canvas.GetLeft(userControl);
                        double y = System.Windows.Controls.Canvas.GetTop(userControl);

                        Viewer.ScrollToHorizontalOffset(Math.Max(0, x));
                        Viewer.ScrollToVerticalOffset(Math.Max(0, y));
                    }

                    break;
                }
            }
        }

        public void SelectAll()
        {
            Canvas.SelectAll();
        }

        internal void SelectElementById(int elementId)
        {
            foreach (Element element in Dialog.Elements)
            {
                if (element.Id == elementId)
                {
                    var control = element.Control;
                    if (control != null)
                    {
                        Canvas.SelectControl(control);
                    }

                    break;
                }
            }
        }

        public bool JournalUndoIsEmpty()
        {
            return Canvas.JournalUndoIsEmpty();
        }

        public bool JournalRedoIsEmpty()
        {
            return Canvas.JournalRedoIsEmpty();
        }
    }
}