﻿using DialogReactor.Journal;
using DialogReactor.Util.ClipboardClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace DialogReactor
{
    public class ScrollableCanvas : Canvas, IUndoRedo
    {
        // Program data
        private MainWindow Window;
        private ClosableTab Tab;

        // Tab dialog data
        private Dialog Dialog;
        public bool IsRequestSaveDialog { get; set; }

        // Mouse events data
        private Point CanvasMousePosition;

        // Zoom data
        const double ScaleMin = 0.5;
        const double ScaleMax = 3.0;
        const double ScaleRate = 0.2;
        private ScaleTransform CanvasScaleTransform;

        // ISelectable events data
        private bool IsMultiplySelecting;
        private Collection<ISelectable> SelectedControls;
        private Point SelectedMinimalBound;
        private Point MultiplySelectingStartPos;
        private Rectangle SelectRectangle;

        // ZIndex sort data
        const int ZIndexLink = 0;
        const int ZIndexControlNormal = 1;
        const int ZIndexControlSelected = 2;
        const int ZIndexSelectRectangle = 3;
        private IControlElement ForvardControl;

        // Context menu events data
        private bool IsContextMenuClosed;

        // Link build data
        private bool IsLinkBuild;
        private ArrowLine DummyLink;
        private Collection<DependencyObject> HitTestResult;

        // Cavas scroll by middle mouse button
        private bool IsMouseMiddleCaptured;
        private Point MouseCapturedPosition;

        // drag event register
        private bool IsDragInProgress;
        private Point DragedControlElementLastPosition;
        private Point DragedControlElementStartPosition;

        // resize event register
        private bool IsResizeInProgress;
        private Point ResizeControlElementLastPosition;
        private IResizeble ResizedControlElement;

        // ILocalable
        private Localization Locale = Localization.RU;

        // IUndoRedo
        private ActionJournal Journal;

        static ScrollableCanvas()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ScrollableCanvas), new FrameworkPropertyMetadata(typeof(ScrollableCanvas)));
        }

        public ScrollableCanvas(MainWindow window, ClosableTab tab, Dialog dialog)
        {
            // Visual
            Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FAFAFA"));

            // Scroll
            VerticalAlignment = VerticalAlignment.Top;
            HorizontalAlignment = HorizontalAlignment.Left;

            // Data
            Window = window;
            Tab = tab;
            Dialog = dialog;

            // Event data
            IsContextMenuClosed = true;
            CanvasMousePosition = new Point();
            MouseCapturedPosition = new Point();
            DragedControlElementStartPosition = new Point();
            ResizeControlElementLastPosition = new Point();
            HitTestResult = new Collection<DependencyObject>();

            // DummyLink
            DummyLink = new ArrowLine(this);
            DummyLink.ArrowEnds = ArrowEnds.End;
            DummyLink.Stroke = Brushes.Green;
            DummyLink.StrokeThickness = 3;
            Children.Add(DummyLink);
            DummyLink.Visibility = Visibility.Hidden;
            SetZIndex(DummyLink, ZIndexControlSelected);

            // Zoom
            CanvasScaleTransform = new ScaleTransform();
            LayoutTransform = CanvasScaleTransform;
            ClipToBounds = true;

            // Selecting
            SelectedControls = new Collection<ISelectable>();
            IsMultiplySelecting = false;
            SelectedMinimalBound = new Point();
            MultiplySelectingStartPos = new Point();
            SelectRectangle = new Rectangle();
            SelectRectangle.Fill = Brushes.Transparent;
            SelectRectangle.Stroke = Brushes.Black;
            SelectRectangle.Visibility = Visibility.Hidden;
            SetZIndex(SelectRectangle, ZIndexSelectRectangle);
            Children.Add(SelectRectangle);

            // IUndoRedo
            Journal = new ActionJournal();

            // Init
            BuildContextMenu();
        }

        private void BuildContextMenu()
        {
            var menu = new ContextMenu();
            menu.Closed += ContextMenuClosed;

            // 
            var addNpcState = new MenuItem();
            var addPlayerAnswer = new MenuItem();
            var addDialogEnd = new MenuItem();
            var switchLocale = new MenuItem();

            addNpcState.Header = "Npc state";
            addPlayerAnswer.Header = "Player answer";
            addDialogEnd.Header = "Dialog end";
            switchLocale.Header = "Switch locale: RU";

            addNpcState.Click += AddNpcState;
            addPlayerAnswer.Click += AddPlayerAnswer;
            addDialogEnd.Click += AddDialogEnd;
            switchLocale.Click += SwitchLocale;

            menu.Items.Add(addNpcState);
            menu.Items.Add(addPlayerAnswer);
            menu.Items.Add(addDialogEnd);
            menu.Items.Add(new Separator());
            menu.Items.Add(switchLocale);

            ContextMenu = menu;
        }

        public void UpdateSize()
        {
            var size = MeasureOverride(new Size(double.PositiveInfinity, double.PositiveInfinity));
            Width = size.Width;
            Height = size.Height;
        }

        protected override Size MeasureOverride(Size constraint)
        {
            var parentControl = Tab.Parent as Control; // tabControl
            var result = new Size(parentControl.ActualWidth, parentControl.ActualHeight);

            foreach (UIElement element in InternalChildren)
            {
                var left = GetLeft(element);
                var top = GetTop(element);

                left = double.IsNaN(left) ? 0 : left;
                top = double.IsNaN(top) ? 0 : top;

                element.Measure(constraint);

                Size desiredSize = element.DesiredSize;

                if (!double.IsNaN(desiredSize.Width) && !double.IsNaN(desiredSize.Height))
                {
                    result.Width = result.Width > left + desiredSize.Width ? result.Width : left + desiredSize.Width;
                    result.Height = result.Height > top + desiredSize.Height ? result.Height : top + desiredSize.Height;
                }
            }

            return result;
        }

        internal void CheckRequestSave()
        {
            if (IsRequestSaveDialog)
            {
                SaveDialog();
            }
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            if (!IsMouseCaptured && e.ChangedButton == MouseButton.Middle && e.ButtonState == MouseButtonState.Pressed)
            {
                if (!IsDragInProgress && IsContextMenuClosed)
                {
                    CaptureMouse();
                    IsMouseMiddleCaptured = true;
                    MouseCapturedPosition = e.GetPosition(this);
                    Mouse.OverrideCursor = Cursors.ScrollAll;
                }
            }
            else
            {
                base.OnMouseDown(e);
            }
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Middle && e.ButtonState == MouseButtonState.Released)
            {
                ReleaseMouseCapture();
                IsMouseMiddleCaptured = false;
                Mouse.OverrideCursor = null;
            }
            else
            {
                base.OnMouseDown(e);
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (IsContextMenuClosed)
            {
                CanvasMousePosition = e.GetPosition(this);

                if (IsLinkBuild)
                {
                    DummyLink.X2 = CanvasMousePosition.X;
                    DummyLink.Y2 = CanvasMousePosition.Y;
                }
            }
            else
            {
                base.OnMouseMove(e);
            }
        }

        protected override void OnPreviewMouseMove(MouseEventArgs e)
        {
            e.Handled = true;
            Point mousePos = e.GetPosition(this);

            if (IsMouseMiddleCaptured)
            {
                MouseScroll(e);
            }
            else if (IsMultiplySelecting)
            {
                var dx = (int)(Math.Max(0, mousePos.X) - MultiplySelectingStartPos.X);
                var dy = (int)(Math.Max(0, mousePos.Y) - MultiplySelectingStartPos.Y);

                // set rectangle pos
                SetLeft(SelectRectangle, Math.Max(0, Math.Min(MultiplySelectingStartPos.X, MultiplySelectingStartPos.X + dx)));
                SetTop(SelectRectangle, Math.Max(0, Math.Min(MultiplySelectingStartPos.Y, MultiplySelectingStartPos.Y + dy)));
                SelectRectangle.Width = Math.Abs(dx);
                SelectRectangle.Height = Math.Abs(dy);

                Window.Refresh();

                UpdateMultiplySelecting();
            }
            else if (IsDragInProgress)
            {
                CalculateMinimalBound();
                
                double dx = mousePos.X - DragedControlElementLastPosition.X;
                double dy = mousePos.Y - DragedControlElementLastPosition.Y;
                DragedControlElementLastPosition = mousePos;

                // limit negative direction movement
                if (dx < 0)
                {
                    dx = Math.Max(dx, -SelectedMinimalBound.X);
                }
                if(dy < 0)
                {
                    dy = Math.Max(dy, -SelectedMinimalBound.Y);
                }

                Window.Refresh();

                // move selected group
                foreach (ISelectable selected in SelectedControls)
                {
                    if(selected is IControlElement)
                    {
                        var control = selected as UserControl;
                        double newX = GetLeft(control);
                        double newY = GetTop(control);

                        newX += dx;
                        newY += dy;

                        SetLeft(control, Math.Max(0, newX));
                        SetTop(control, Math.Max(0, newY));
                    }
                }
                
                Window.Refresh();
                UpdateSize();

                // update links
                foreach (ISelectable selected in SelectedControls)
                {
                    if(selected is IControlElement)
                    {
                        (selected as IControlElement).UpdateLinks(this);
                    }
                }
                
                IsRequestSaveDialog = true;
            }
            else if(IsResizeInProgress)
            {
                if(ResizedControlElement != null)
                {
                    var control = ResizedControlElement as UserControl;

                    var point = Mouse.GetPosition(this);
                    double dx = point.X - ResizeControlElementLastPosition.X;
                    double dy = point.Y - ResizeControlElementLastPosition.Y;

                    double newX = GetLeft(control);
                    double newY = GetTop(control);

                    double newWidth = control.Width;
                    double newHeight = control.Height;

                    newWidth += dx;
                    newHeight += dy;

                    if ((newWidth >= 0) && (newHeight >= 0))
                    {
                        SetLeft(control, newX);
                        SetTop(control, newY);

                        control.Width = Math.Max(ResizedControlElement.ResizeMinWidth(), Math.Min(ResizedControlElement.ResizeMaxWidth(), newWidth));
                        control.Height = Math.Max(ResizedControlElement.ResizeMinHeight(), Math.Min(ResizedControlElement.ResizeMaxHeight(), newHeight));

                        ResizeControlElementLastPosition = point;

                        // refresh
                        UpdateSize();
                        Window.Refresh();
                        (control as IControlElement).UpdateLinks(this);
                    }
                }
            }
            else
            {
                e.Handled = false;
                base.OnPreviewMouseMove(e);
            }
        }

        public void MouseScroll(MouseEventArgs e)
        {
            Point mousePos = e.GetPosition(this);

            var dx = (int)(mousePos.X - MouseCapturedPosition.X);
            var dy = (int)(mousePos.Y - MouseCapturedPosition.Y);

            if (dx != 0 && dy != 0)
            {
                MouseCapturedPosition = mousePos;
            }

            Tab.Scroll(dx, dy);
        }

        private void UpdateMultiplySelecting()
        {
            UnselectControls();

            var xMin = GetLeft(SelectRectangle);
            var yMin = GetTop(SelectRectangle);
            var xMax = xMin + SelectRectangle.Width;
            var yMax = yMin + SelectRectangle.Height;

            foreach (UIElement element in Children)
            {
                if(element is IControlElement)
                {
                    var control = element as UserControl;

                    var x = GetLeft(element);
                    var y = GetTop(element);
                    var w = control.ActualWidth;
                    var h = control.ActualHeight;

                    if ((x > xMin && x < xMax && y > yMin && y < yMax) ||
                        ((x + w) > xMin && (x + w) < xMax && (y + h) > yMin && (y + h) < yMax))
                    {
                        SelectedControls.Add(element as ISelectable);
                        (element as ISelectable).Select();
                    }
                }
                else if(element is ArrowLine)
                {
                    var arrow = element as ArrowLine;

                    if((arrow.X1 > xMin && arrow.X1 < xMax && arrow.Y1 > yMin && arrow.Y1 < yMax) ||
                       (arrow.X2 > xMin && arrow.X2 < xMax && arrow.Y2 > yMin && arrow.Y2 < yMax))
                    {
                        SelectedControls.Add(element as ISelectable);
                        (element as ISelectable).Select();
                    }
                }
            }
        }

        private void CalculateMinimalBound()
        {
            SelectedMinimalBound.X = double.MaxValue;
            SelectedMinimalBound.Y = double.MaxValue;

            if (SelectedControls.Count > 0)
            {
                foreach (ISelectable selected in SelectedControls)
                {
                    if (selected is IControlElement)
                    {
                        var userControl = selected as UserControl;

                        var x = GetLeft(userControl);
                        var y = GetTop(userControl);

                        if(x < SelectedMinimalBound.X)
                        {
                            SelectedMinimalBound.X = Math.Max(0, x);
                        }

                        if(y < SelectedMinimalBound.Y)
                        {
                            SelectedMinimalBound.Y = Math.Max(0, y);
                        }
                    }
                }
            }
        }

        internal void SelectAll()
        {
            UnselectControls();

            foreach (UIElement element in Children)
            {
                if(element is ISelectable)
                {
                    SelectedControls.Add(element as ISelectable);
                    (element as ISelectable).Select();
                }
            }
        }

        protected override void OnPreviewMouseWheel(MouseWheelEventArgs e)
        {
            if (Window.IsKeyCtrlPressed)
            {
                e.Handled = true; // to disable scroll
                Zoom(e);
            }
            else
            {
                base.OnPreviewMouseWheel(e);
            }
        }

        public void Zoom(MouseWheelEventArgs e)
        {
            if (e.Delta > 0)
            {
                if (CanvasScaleTransform.ScaleX < ScaleMax)
                {
                    CanvasScaleTransform.ScaleX += ScaleRate;
                    CanvasScaleTransform.ScaleY += ScaleRate;

                    Window.Log("Zoom: " + Math.Ceiling(CanvasScaleTransform.ScaleX * 100) + "%");
                }
            }
            else if (e.Delta < 0)
            {
                if (CanvasScaleTransform.ScaleX > ScaleMin)
                {
                    CanvasScaleTransform.ScaleX -= ScaleRate;
                    CanvasScaleTransform.ScaleY -= ScaleRate;

                    Window.Log("Zoom: " + Math.Ceiling(CanvasScaleTransform.ScaleX * 100) + "%");
                }
            }
        }

        public void SaveDialog()
        {
            Tab.Dialog.Save();
        }

        private void ContextMenuClosed(object sender, RoutedEventArgs e)
        {
            IsContextMenuClosed = true;
        }

        public Point GetPosition(Shape element)
        {
            if (element == null)
            {
                Window.Log("Error on get shape position: arg is null");
                return new Point(0, 0);
            }

            try
            {
                var areaPosition = element.TransformToAncestor(this).Transform(new Point(0, 0));
                areaPosition.X += element.ActualWidth / 2;
                areaPosition.Y += element.ActualHeight / 2;
                return areaPosition;
            }
            catch (Exception e)
            {
                Window.Log("Error on get shape position (" + e.ToString() + ")");
                return new Point(0, 0);
            }
        }
        
        private void BuildLink(IControlElement control, Shape anchor)
        {
            if (!IsLinkBuild)
            {
                IsLinkBuild = true;
                DummyLink.Visibility = Visibility.Visible;

                var startPos = GetPosition(anchor);

                DummyLink.X1 = startPos.X;
                DummyLink.Y1 = startPos.Y;
                DummyLink.X2 = startPos.X;
                DummyLink.Y2 = startPos.Y;
                DummyLink.From = control;
                DummyLink.Stroke = ArrowLine.GetArrowColor(control);
            }
        }

        private HitTestResultBehavior MyHitTestResult(HitTestResult result)
        {
            HitTestResult.Add(result.VisualHit);
            return HitTestResultBehavior.Continue;
        }

        internal void SelectControl(ISelectable control)
        {
            if (control == null)
            {
                throw new ArgumentNullException("Try to select null control");
            }
            else
            {
                UnselectControls();

                SelectedControls.Add(control);
                control.Select();

                if (control is IControlElement)
                {
                    SetControlForvard(control as IControlElement);
                }

                if(control is IControlElement)
                {   // select all links
                    var element = (control as IControlElement).GetContainedElement();
                    
                    if(element != null)
                    {
                        if(element.InputLinks.Count > 0)
                        {
                            foreach(Link link in element.InputLinks)
                            {
                                SelectedControls.Add(link.Control);
                                link.Control.Select();
                            }
                        }

                        if(element.OutputLinks.Count > 0)
                        {
                            foreach(Link link in element.OutputLinks)
                            {
                                SelectedControls.Add(link.Control);
                                link.Control.Select();
                            }
                        }
                    }
                }
            }
        }

        public void UnselectControls()
        {
            if(SelectedControls.Count > 0)
            {
                foreach (ISelectable control in SelectedControls)
                {
                    control.Unselect();
                }

                SelectedControls.Clear();
            }
        }

        private void SetControlForvard(IControlElement control)
        {
            if (ForvardControl == null)
            {
                ForvardControl = control;
                SetZIndex(ForvardControl as UserControl, ZIndexControlSelected);
            }
            else
            {
                if (ForvardControl != control)
                {
                    SetZIndex(ForvardControl as UserControl, ZIndexControlNormal);
                    ForvardControl = control;
                    SetZIndex(ForvardControl as UserControl, ZIndexControlSelected);
                }
            }
        }
        
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            if (e.OriginalSource is ScrollableCanvas)
            {   // Canvas only process
                UnselectControls();
                MultiplySelectBegin();
            }
            else
            {
                ElementControlClick(e);
            }
        }

        private void MultiplySelectBegin()
        {
            IsMultiplySelecting = true;
            MultiplySelectingStartPos = Mouse.GetPosition(this);

            // Set start pos
            SetLeft(SelectRectangle, MultiplySelectingStartPos.X);
            SetTop(SelectRectangle, MultiplySelectingStartPos.Y);
            SelectRectangle.Width = 0;
            SelectRectangle.Height = 0;

            SelectRectangle.Visibility = Visibility.Visible;
            CaptureMouse();
        }

        private void ElementControlClick(MouseButtonEventArgs e)
        {
            var control = CheckForIControlElemenet(e.OriginalSource);
            if (control != null)
            {
                if (e.ClickCount == 1)
                {
                    ElementControlSingleClick(control);
                }
                else if (e.ClickCount == 2)
                {
                    ElementControlDoubleClick(control);
                }
            }
        }

        private void ElementControlSingleClick(IControlElement control)
        {
            if (!IsMouseMiddleCaptured)
            {
                if (!SelectedControls.Contains(control))
                {
                    SelectControl(control);
                }

                if (Mouse.DirectlyOver is Ellipse && Mouse.DirectlyOver == control.GetOutAnchor())
                {
                    BuildLink(control, control.GetOutAnchor());
                }
                else if(control is IResizeble && Mouse.DirectlyOver == (control as IResizeble).ResizeControl())
                {
                    if(!IsDragInProgress && !IsResizeInProgress)
                    {
                        ResizeElementBegin(control);
                    }
                }
                else
                {
                    if (!IsDragInProgress && !IsResizeInProgress)
                    {
                        MoveElementBegin(control);
                    }
                }
            }
        }

        private void ElementControlDoubleClick(IControlElement control)
        {
            if (control is IEdited)
            {
                (control as IEdited).Edit();
                ValidateDialog();
            }
        }

        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            if (IsLinkBuild)
            {
                IsLinkBuild = false;
                DummyLink.Visibility = Visibility.Hidden;

                var point = e.GetPosition(e.OriginalSource as UIElement);
                HitTestResult.Clear();
                VisualTreeHelper.HitTest(this, null, new HitTestResultCallback(MyHitTestResult), new PointHitTestParameters(point));

                if (HitTestResult.Count > 0)
                {
                    foreach (DependencyObject obj in HitTestResult)
                    {
                        var target = CheckForIControlElemenet(obj);
                        if (target != null && obj != DummyLink.From)
                        {
                            AddLink(DummyLink.From, target, true, true);
                            break;
                        }

                    }
                }
            }
            else if (IsMultiplySelecting)
            {
                IsMultiplySelecting = false;
                SelectRectangle.Visibility = Visibility.Hidden;
                ReleaseMouseCapture();
            }
            else if (IsDragInProgress)
            {
                MoveElementEnd();
            }
            else if (IsResizeInProgress)
            {
                ResizeElementEnd();
            }
        }

        protected override void OnMouseRightButtonDown(MouseButtonEventArgs e)
        {
            if (IsContextMenuClosed)
            {
                if (e.OriginalSource is ScrollableCanvas)
                {
                    IsContextMenuClosed = false;
                    ContextMenu.PlacementTarget = e.OriginalSource as Canvas;
                    ContextMenu.IsOpen = true;
                }
            }
        }

        private IControlElement CheckForIControlElemenet(object source)
        {
            if(source != null && source is DependencyObject)
            {
                var target = VisualTreeHelper.GetParent(source as DependencyObject);
                while (target != null && !(target is IControlElement))
                {
                    target = VisualTreeHelper.GetParent(target);
                }

                if (target != null && target is IControlElement)
                {
                    return target as IControlElement;
                }
            }
            
            return null;
        }

        private void AddNpcState(object sender, RoutedEventArgs e)
        {
            AddControl(Element.TypeNpcStatement, CanvasMousePosition.X, CanvasMousePosition.Y, true, true);
        }

        private void AddPlayerAnswer(object sender, RoutedEventArgs e)
        {
            AddControl(Element.TypePlayerAnswer, CanvasMousePosition.X, CanvasMousePosition.Y, true, true);
        }

        private void AddDialogEnd(object sender, RoutedEventArgs e)
        {
            AddControl(Element.TypeDialogEnd, CanvasMousePosition.X, CanvasMousePosition.Y, true, true);
        }

        private IControlElement AddControl(string type, double x, double y, bool saveDialog, bool cancelable)
        {
            var control = Element.BuildControlElement(Element.GetElement(Dialog, type));
            AddElementToCanvas(control as UserControl, x, y, saveDialog, cancelable);
            ValidateDialog();
            return control;
        }

        private void SwitchLocale(object sender, RoutedEventArgs e)
        {
            var item = sender as MenuItem;
            if (Locale == Localization.RU)
            {
                item.Header = "Switch locale: Eng";
                Locale = Localization.ENG;
            }
            else if (Locale == Localization.ENG)
            {
                item.Header = "Switch locale: RU";
                Locale = Localization.RU;
            }

            // update controls
            foreach (Element element in Dialog.Elements)
            {
                if (element.Control is ILocalable)
                {
                    (element.Control as ILocalable).Locale = Locale;
                }
            }
        }

        public void AddElementToCanvas(UserControl control, double x, double y, bool saveDialog, bool cancelable)
        {
            Children.Add(control);

            SetLeft(control, x);
            SetTop(control, y);
            SetZIndex(control, ZIndexControlNormal);

            Dialog.Elements.Add((control as IControlElement).GetContainedElement());

            if (control is ILocalable)
            {
                (control as ILocalable).Locale = Locale;
            }

            if (saveDialog)
            {
                SaveDialog();
            }

            if (cancelable)
            {
                var element = (control as IControlElement).GetContainedElement();
                RegisterAction(new ActionDialogElementAdd(element, x, y), true);
            }
        }

        public void AddLink(IControlElement from, IControlElement to, bool saveDialog, bool cancelable)
        {
            if (from == null || to == null)
            {
                return;
            }

            if (Link.BuildLinkPossibility(from, to))
            {
                if (from is ControlDialogStart || from is ControlAnswer)
                {   // 1 out max
                    if (from.GetContainedElement().OutputLinks.Count > 0)
                    {
                        Link link = from.GetContainedElement().OutputLinks[0];
                        RemoveLink(link.Control, false, true);
                    }
                }

                if(to is ControlAnswer)
                {   // links from only one state
                    var input = to.GetContainedElement().InputLinks;

                    if (input.Count > 0)
                    {
                        var toRemoveList = new List<Link>();
                        foreach (Link link in input)
                        {
                            toRemoveList.Add(link);
                        }

                        if(toRemoveList.Count > 0)
                        {
                            foreach(Link link in toRemoveList)
                            {
                                if (link.From != from)
                                {
                                    RemoveLink(link.Control, false, true);
                                }
                            }
                        }
                    }
                }

                var control = new ControlLink(from, to, GetPosition(from.GetOutAnchor()), GetPosition(to.GetInAnchor()), this);
                from.GetContainedElement().OutputLinks.Add(control.Link);
                to.GetContainedElement().InputLinks.Add(control.Link);

                SetZIndex(control.Arrow, ZIndexLink);
                Children.Add(control.Arrow);
                Dialog.Links.Add(control.Link);

                if (saveDialog)
                {    
                    SaveDialog();
                }

                if (cancelable)
                {
                    RegisterAction(new ActionDialogLinkAdd(from.GetContainedElement(), to.GetContainedElement()), true);
                }
            }
        }

        internal void HotKeyDelete()
        {
            try{
                if(SelectedControls.Count > 0)
                {
                    foreach (ISelectable control in SelectedControls)
                    {
                        if (control is IControlElement)
                        {
                            if ((control as IControlElement).Removable())
                            {
                                RemoveControl(control as IControlElement, true);
                            }
                        }
                        else if (control is ControlLink)
                        {
                            RemoveLink(control as ControlLink, true, true);
                        }
                    }

                    UnselectControls();
                    ValidateDialog();
                }
            }
            catch (Exception e)
            {
                Window.Log(e.StackTrace);
            }
        }

        internal void RemoveControl(IControlElement control, bool cancelable)
        {
            Window.Refresh();

            var userControl = control as UserControl;
            var element = control.GetContainedElement();

            // IUndoRedo register
            if (cancelable)
            {   
                var x = GetLeft(userControl);
                var y = GetTop(userControl);
                Collection<ActionDialogLinkRemove> removedLinks = new Collection<ActionDialogLinkRemove>();

                if(element.InputLinks.Count > 0)
                {
                    foreach (Link link in element.InputLinks)
                    {
                        removedLinks.Add(new ActionDialogLinkRemove(link.From, link.To));
                    }
                }
                
                if(element.OutputLinks.Count > 0)
                {
                    foreach (Link link in element.OutputLinks)
                    {
                        removedLinks.Add(new ActionDialogLinkRemove(link.From, link.To));
                    }
                }
                
                RegisterAction(new ActionDialogElementRemoved(control.GetContainedElement(), x, y, removedLinks), true);
            }

            // Remove control
            Children.Remove(userControl);
            Dialog.Elements.Remove(control.GetContainedElement());

            if (element.InputLinks.Count > 0)
            {
                foreach (Link link in element.InputLinks)
                {
                    Dialog.Links.Remove(link);
                    Children.Remove(link.Control.Arrow);
                    link.From.OutputLinks.Remove(link);
                }
                element.InputLinks.Clear();
            }

            if (element.OutputLinks.Count > 0)
            {
                foreach (Link link in element.OutputLinks)
                {
                    Dialog.Links.Remove(link);
                    Children.Remove(link.Control.Arrow);
                    link.To.InputLinks.Remove(link);
                }
                element.OutputLinks.Clear();
            }

            SaveDialog();
        }

        public void RemoveLink(ControlLink control, bool saveDialog, bool cancelable)
        {
            IControlElement from = control.From;
            from.GetContainedElement().OutputLinks.Remove(control.Link);

            IControlElement to = control.To;
            to.GetContainedElement().InputLinks.Remove(control.Link);

            Children.Remove(control.Arrow);

            Dialog.Links.Remove(control.Link);

            if (saveDialog)
            {
                SaveDialog();
            }

            // IUndoRedo register
            if (cancelable)
            {
                RegisterAction(new ActionDialogLinkRemove(from.GetContainedElement(), to.GetContainedElement()), true);
            }
        }

        private void MoveElementBegin(IControlElement control)
        {
            if(control != null)
            {
                Mouse.OverrideCursor = Cursors.Hand;
                Window.Refresh();

                IsDragInProgress = true;
                DragedControlElementStartPosition = Mouse.GetPosition(this);
                DragedControlElementLastPosition = Mouse.GetPosition(this);

                CaptureMouse();
            }
            else
            {
                throw new ArgumentNullException("Try to drag null element [ScrollableCanvas.MoveElementBegin()]");
            }
        }

        private void MoveElementEnd()
        {
            if(SelectedControls.Count > 0)
            {
                Mouse.OverrideCursor = null;
                Window.Refresh();

                IsDragInProgress = false;
                var mousePos = Mouse.GetPosition(this);

                var dx = DragedControlElementStartPosition.X - mousePos.X;
                var dy = DragedControlElementStartPosition.Y - mousePos.Y;

                ReleaseMouseCapture();
                CheckRequestSave();

                // Register action in Undo\Redo journal
                var elements = new Collection<Element>();
                foreach (ISelectable selected in SelectedControls)
                {
                    if(selected is IControlElement)
                    {
                        var control = selected as IControlElement;
                        control.UpdateLinks(this);
                        elements.Add(control.GetContainedElement());
                    }
                }
                RegisterAction(new ActionDialogElementMoved(elements, dx, dy), false);
            }
        }

        private void MoveElementsGroup(Collection<Element> elements, double dx, double dy)
        {
            if (elements != null && elements.Count > 0)
            {
                Window.Refresh();

                // replace elements
                foreach (Element element in elements)
                {
                    if (element.Control != null)
                    {
                        var userControl = element.Control as UserControl;
                        SetLeft(userControl, Math.Max(0, GetLeft(userControl) + dx));
                        SetTop(userControl, Math.Max(0, GetTop(userControl) + dy));
                    }
                }

                UpdateSize();
                Window.Refresh();

                // update links
                foreach (Element element in elements)
                {
                    if (element.Control != null)
                    {
                        element.Control.UpdateLinks(this);
                    }
                }

                Window.Refresh();
                SaveDialog();
            }
        }
        
        private void ResizeElementBegin(IControlElement control)
        {
            if (control != null && control is IResizeble)
            {
                Mouse.OverrideCursor = Cursors.SizeNWSE;
                Window.Refresh();

                IsResizeInProgress = true;
                ResizedControlElement = control as IResizeble;
                ResizeControlElementLastPosition = Mouse.GetPosition(this);

                CaptureMouse();
            }
            else
            {
                throw new ArgumentNullException("Try to resize null element [ScrollableCanvas.ResizeElementBegin()]");
            }
        }

        private void ResizeElementEnd()
        {
            Mouse.OverrideCursor = null;
            Window.Refresh();

            ResizedControlElement = null;
            IsResizeInProgress = false;
            ReleaseMouseCapture();
        }

        public void RegisterAction(Journal.Action action, bool needValidation)
        {
            Journal.RegisterAction(action);
            Window.UpdateUndoRedoControl();

            ValidateDialog();
        }

        public bool JournalRedoIsEmpty()
        {
            return Journal.RedoIsEmpty();
        }

        public bool JournalUndoIsEmpty()
        {
            return Journal.UndoIsEmpty();
        }

        public void Undo()
        {
            try
            {
                Journal.Action action = Journal.Undo();

                if (action != null)
                {
                    switch (action.Type)
                    {
                        case ActionType.ElementAdd:
                            UndoElementAdd(action as ActionDialogElementAdd);
                            break;

                        case ActionType.ElementRemoved:
                            UndoElementRemoved(action as ActionDialogElementRemoved);
                            break;

                        case ActionType.ElementMoved:
                            UndoElementMoved(action as ActionDialogElementMoved);
                            break;

                        case ActionType.LinkAdd:
                            UndoLinkAdd(action as ActionDialogLinkAdd);
                            break;

                        case ActionType.LinkRemoved:
                            UndoLinkRemoved(action as ActionDialogLinkRemove);
                            break;
                    }

                    Window.UpdateUndoRedoControl();
                }
            }
            catch (ArgumentNullException e)
            {
                Window.Log("Error on undo operation: " + e.ParamName);
            }
            catch(Exception e)
            {
                Window.Log("Error on undo operation:\r\n" + e.StackTrace);
            }
        }

        private void UndoElementAdd(ActionDialogElementAdd action)
        {
            if(action == null)
            {
                throw new ArgumentNullException("Action is null [ScrollableCanvas.UndoElementAdd()]");
            }
            else if(action.Element == null)
            {
                throw new ArgumentNullException("Element is null [ScrollableCanvas.UndoElementAdd()]");
            }
            else if(action.Element.Control == null)
            {
                throw new ArgumentNullException("Element control is null [ScrollableCanvas.UndoElementAdd()]");
            }
            else
            {
                RemoveControl(action.Element.Control, false);
            }
        }

        private void UndoElementRemoved(ActionDialogElementRemoved action)
        {
            if(action == null)
            {
                throw new ArgumentNullException("Action is null [ScrollableCanvas.UndoElementRemoved()]");
            }
            else if (action.Element == null)
            {
                throw new ArgumentNullException("Element is null [ScrollableCanvas.UndoElementRemoved()]");
            }
            else
            {
                // restore element
                var control = Element.BuildControlElement(action.Element);
                AddElementToCanvas(control as UserControl, action.PosX, action.PosY, true, false);
                Window.Refresh();

                // restore links
                if(action.RemovedLinks != null && action.RemovedLinks.Count > 0)
                {
                    foreach (ActionDialogLinkRemove actionLinkRemove in action.RemovedLinks)
                    {
                        UndoLinkRemoved(actionLinkRemove);
                    }
                }
            }
        }

        private void UndoElementMoved(ActionDialogElementMoved action)
        {
            if(action == null)
            {
                throw new ArgumentNullException("Action is null [ScrollableCanvas.UndoElementMoved()]");
            }
            else if(action.Elements == null)
            {
                throw new ArgumentNullException("Elements group is null [ScrollableCanvas.UndoElementMoved()]");
            }
            else
            {
                MoveElementsGroup(action.Elements, action.dx, action.dy);
            }
        }

        private void UndoLinkRemoved(ActionDialogLinkRemove action)
        {
            if(action == null)
            {
                throw new ArgumentNullException("Action is null [ScrollableCanvas.UndoLinkRemoved()]");
            }
            else if (action.From == null || action.From.Control == null)
            {
                throw new ArgumentNullException("Element is null [ScrollableCanvas.UndoLinkRemoved()]");
            }
            else if(action.To == null || action.To.Control == null)
            {
                throw new ArgumentNullException("Element control is null [ScrollableCanvas.UndoLinkRemoved()]");
            }
            else
            {
                AddLink(action.From.Control, action.To.Control, true, false);
            }
        }

        private void UndoLinkAdd(ActionDialogLinkAdd action)
        {
            if(action == null)
            {
                throw new ArgumentNullException("Action is null [ScrollableCanvas.UndoLinkAdd()]");
            }
            else if (action.From == null)
            {
                throw new ArgumentNullException("Eelement is null [ScrollableCanvas.UndoLinkAdd()]");
            }
            else if (action.To == null)
            {
                throw new ArgumentNullException("Element control is null [ScrollableCanvas.UndoLinkAdd()]");
            }
            else
            {
                Link link = Dialog.SearchLink(action.From, action.To);

                if (link != null && link.Control != null)
                {
                    RemoveLink(link.Control, true, false);
                }
                else
                {
                    Window.Log("Error on search link fromId: " + action.From.Id + " toId: " + action.To.Id);
                }
            }
        }

        public void Redo()
        {
            try
            {
                Journal.Action action = Journal.Redo();

                if (action != null)
                {
                    switch (action.Type)
                    {
                        case ActionType.ElementAdd:
                            RedoElementAdd(action as ActionDialogElementAdd);
                            break;

                        case ActionType.ElementRemoved:
                            RedoElementRemoved(action as ActionDialogElementRemoved);
                            break;

                        case ActionType.ElementMoved:
                            RedoElementMoved(action as ActionDialogElementMoved);
                            break;

                        case ActionType.LinkAdd:
                            RedoLinkAdd(action as ActionDialogLinkAdd);
                            break;

                        case ActionType.LinkRemoved:
                            RedoLinkRemoved(action as ActionDialogLinkRemove);
                            break;
                    }

                    Window.UpdateUndoRedoControl();
                }
            }
            catch (ArgumentNullException e)
            {
                Window.Log("Error on redo operation\r\n" + e.ParamName);
            }
            catch (Exception e)
            {
                Window.Log("Error on redo operation\r\n" + e.StackTrace);
            }
        }

        private void RedoElementAdd(ActionDialogElementAdd action)
        {
            if(action == null)
            {
                throw new ArgumentNullException("Action is null [ScrollableCanvas.RedoElementAdd()]");
            }
            else if(action.Element == null)
            {
                throw new ArgumentNullException("Element is null [ScrollableCanvas.RedoElementAdd()]");
            }
            else
            {
                var control = Element.BuildControlElement(action.Element);
                AddElementToCanvas(control as UserControl, action.PosX, action.PosY, true, false);
                Window.Refresh();
            }
        }

        private void RedoElementRemoved(ActionDialogElementRemoved action)
        {
            if (action == null)
            {
                throw new ArgumentNullException("Action is null [ScrollableCanvas.RedoElementRemoved()]");
            }
            else if (action.Element == null)
            {
                throw new ArgumentNullException("Element is null [ScrollableCanvas.RedoElementRemoved()]");
            }
            else if (action.Element.Control == null)
            {
                throw new ArgumentNullException("Element control is null [ScrollableCanvas.RedoElementRemoved()]");
            }
            else
            {
                RemoveControl(action.Element.Control, false);
            }
        }

        private void RedoElementMoved(ActionDialogElementMoved action)
        {
            if (action == null)
            {
                throw new ArgumentNullException("Action is null [ScrollableCanvas.RedoElementMoved()]");
            }
            else if (action.Elements == null)
            {
                throw new ArgumentNullException("Elements group is null [ScrollableCanvas.RedoElementMoved()]");
            }
            else
            {
                MoveElementsGroup(action.Elements, -action.dx, -action.dy);
            }
        }

        private void RedoLinkRemoved(ActionDialogLinkRemove action)
        {
            if (action == null)
            {
                throw new ArgumentNullException("Action is null [ScrollableCanvas.RedoLinkAdd()]");
            }
            else if (action.From == null)
            {
                throw new ArgumentNullException("Eelement is null [ScrollableCanvas.UndoLinkAdd()]");
            }
            else if (action.To == null)
            {
                throw new ArgumentNullException("Element control is null [ScrollableCanvas.UndoLinkAdd()]");
            }
            else
            {
                Link link = Dialog.SearchLink(action.From, action.To);

                if (link != null && link.Control != null)
                {
                    RemoveLink(link.Control, true, false);
                }
                else
                {
                    Window.Log("Error on search link fromId: " + action.From.Id + " toId: " + action.To.Id);
                }
            }
        }

        private void RedoLinkAdd(ActionDialogLinkAdd action)
        {
            if (action == null)
            {
                throw new ArgumentNullException("Action is null [ScrollableCanvas.RedoLinkAdd()]");
            }
            else if (action.From == null || action.From.Control == null)
            {
                throw new ArgumentNullException("From control element is null [ScrollableCanvas.RedoLinkAdd()]");
            }
            else if (action.To == null || action.To.Control == null)
            {
                throw new ArgumentNullException("To control element is null [ScrollableCanvas.RedoLinkAdd()]");
            }
            else
            {
                AddLink(action.From.Control, action.To.Control, true, false);
            }
        }
        
        internal void Copy()
        {
            try
            {
                if (SelectedControls.Count > 0)
                {
                    var format = DataFormats.GetDataFormat(typeof(List<ClipboardElement>).FullName);
                    var obj = new DataObject();

                    var list = new List<ClipboardElement>();

                    CalculateMinimalBound();

                    foreach (ISelectable selected in SelectedControls)
                    {
                        if (selected.CanCopyPaste())
                        {
                            var data = new ClipboardElement();

                            if (selected is IControlElement)
                            {
                                var control = selected as UserControl;
                                data.Dx = GetLeft(control) - SelectedMinimalBound.X;
                                data.Dy = GetTop(control) - SelectedMinimalBound.Y;
                                data.Width = control.ActualWidth;
                                data.Height = control.ActualHeight;

                                var element = (selected as IControlElement).GetContainedElement();
                                data.Id = element.Id;
                                data.Type = Element.GetElementType(element);
                                data.Data = element.GetArrayData();

                                list.Add(data);
                            }
                            else if(selected is ArrowLine)
                            {
                                var link = (selected as ArrowLine).Link;
                                if(link != null)
                                {
                                    var arr = new object[2];
                                    arr[0] = link.From.GetContainedElement().Id;
                                    arr[1] = link.To.GetContainedElement().Id;

                                    data.Type = Element.TypeLink;
                                    data.Data = arr;

                                    list.Add(data);
                                }
                            }
                        }
                    }

                    obj.SetData(format.Name, list);
                    Clipboard.Clear();
                    Clipboard.SetDataObject(obj, false);
                }
            }
            catch(Exception e)
            {
                Window.Log("Copy exception: " + e.StackTrace);
            }
        }

        internal void Paste()
        {
            try
            {
                var obj = Clipboard.GetDataObject();
                var format = typeof(List<ClipboardElement>).FullName;

                if (obj != null && obj.GetDataPresent(format))
                {
                    var list = obj.GetData(format) as List<ClipboardElement>;
                    if (list != null && list.Count > 0)
                    {
                        PasteElements(list);
                    }
                }
            }
            catch(Exception e)
            {
                Window.Log("Paste exception: " + e.StackTrace);
            }
        }

        private void PasteElements(List<ClipboardElement> list)
        {
            UnselectControls();
            var mouse = Mouse.GetPosition(this);
            var indexMap = new Dictionary<int, IControlElement>(); // <OldId, NewControlElement>
            var links = new List<ClipboardElement>();

            // build elements, remap indexes and filter links from list
            foreach (ClipboardElement element in list)
            {
                if (element.Type.Equals(Element.TypeLink))
                {
                    links.Add(element);
                }
                else
                {
                    var controlElement = AddControl(element.Type, mouse.X + element.Dx, mouse.Y + element.Dy, true, true);
                    var control = controlElement as UserControl;
                    control.Width = element.Width;
                    control.Height = element.Height;

                    if (element.Type.Equals(Element.TypePlayerAnswer))
                    {
                        controlElement.GetContainedElement().SetArrayData(element.Data);
                        (controlElement as ControlAnswer).UpdatePreview();
                    }
                    else if (element.Type.Equals(Element.TypeNpcStatement))
                    {
                        controlElement.GetContainedElement().SetArrayData(element.Data);
                        (controlElement as ControlNpcStatement).UpdatePreview();
                    }


                    indexMap.Add(element.Id, controlElement);

                    SelectedControls.Add(control as ISelectable);
                    (control as ISelectable).Select();
                }
            }

            // build links
            Window.Refresh();
            if (links.Count > 0)
            {
                foreach (ClipboardElement link in links)
                {
                    int FromId = (int)link.Data[0];
                    int ToId = (int)link.Data[1];

                    if(indexMap.ContainsKey(FromId) && indexMap.ContainsKey(ToId))
                    {
                        IControlElement From = null;
                        IControlElement To = null;

                        if(indexMap.TryGetValue(FromId, out From) && indexMap.TryGetValue(ToId, out To))
                        {
                            AddLink(From, To, true, true);
                        }
                    }
                }
            }
        }

        internal void Cut()
        {
            Copy();
            HotKeyDelete();
        }

        private void ValidateDialog()
        {
            Window.ValidateDialog(Dialog);
        }
    }
}