﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace DialogReactor
{
    public partial class ControlDialogStart : UserControl, IControlElement
    {
        private static SolidColorBrush NormalBorderBrush = new BrushConverter().ConvertFrom("#27AE60") as SolidColorBrush;

        private Element Element;

        public ControlDialogStart(ElementDialogStart element)
        {
            InitialControl();

            Element = element;
            Element.Control = this;
        }

        private void InitialControl()
        {
            InitializeComponent();
        }

        public Element GetContainedElement()
        {
            return Element;
        }

        public void SetContainedElement(Element element)
        {
            Element = element;
        }

        public Shape GetInAnchor()
        {
            throw new NotImplementedException();
        }

        public Shape GetOutAnchor()
        {
            return LinkOut;
        }

        public void UpdateLinks(ScrollableCanvas canvas)
        {
            if (Element.OutputLinks.Count > 0)
            {
                var point = canvas.GetPosition(LinkOut);

                foreach (Link link in Element.OutputLinks)
                {
                    link.Control.Arrow.X1 = point.X;
                    link.Control.Arrow.Y1 = point.Y + 5;
                }
            }
        }

        private void LinkOut_MouseEnter(object sender, MouseEventArgs e)
        {
            LinkOut.Fill = new SolidColorBrush(Colors.Red);
        }

        private void LinkOut_MouseLeave(object sender, MouseEventArgs e)
        {
            LinkOut.Fill = new SolidColorBrush(Colors.Green);
        }

        public void Select()
        {
            Header.Fill = Brushes.Red;
        }

        public void Unselect()
        {
            Header.Fill = NormalBorderBrush;
        }

        public bool Removable()
        {
            return false;
        }

        public bool CanCopyPaste()
        {
            return false;
        }

        public void ValidationError(bool value)
        {
            if (value)
            {
                ValidationIcon.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                ValidationIcon.Visibility = System.Windows.Visibility.Hidden;
            }
        }
    }
}
