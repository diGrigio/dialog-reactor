﻿using DialogReactor.Util;
using System;
using System.IO;
using System.Windows;
using System.Windows.Forms;

namespace DialogReactor
{
    public partial class DialogEditProject : Window
    {
        private ProjectHandler ProjectHandler;

        public string ProjectTitle { get { return TextTitle.Text; } }
        public string DialogsPath { get { return TextSourcePath.Text; } }
        public bool IsSourcePathRelative { get; private set; }

        public DialogEditProject(ProjectHandler project)
        {
            InitializeComponent();

            ProjectHandler = project;
            TextTitle.Text = project.Title;
            TextSourcePath.Text = project.GetContentTypeDirectory(FileContentType.Dialog);

            ValidateInput();
        }

        private void ButtonOk_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void ButtonSelectPath_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                TextSourcePath.Text = dialog.SelectedPath + "\\";
                ValidateInput();
            }
        }

        private void TextTitle_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            ValidateInput();
        }

        private void TextSourcePath_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            ValidateInput();
        }

        private void ValidateInput()
        {
            if(TextTitle == null || TextSourcePath == null)
            {
                return;
            }
            
            if (ValidateTitle() && ValidateSourcePath())
            {
                LabelError.Content = string.Empty;
                ButtonOk.IsEnabled = true;
            }
            else
            {
                ButtonOk.IsEnabled = false;
            }
        }

        private bool ValidateTitle()
        {
            if (string.IsNullOrWhiteSpace(TextTitle.Text))
            {
                LabelError.Content = "Project title cant be empty";
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool ValidateSourcePath()
        {
            try
            {
                var projectRootDir = new FileInfo(ProjectHandler.ProjectXmlPath).Directory.FullName;
                IsSourcePathRelative = false;

                if (string.IsNullOrWhiteSpace(projectRootDir) || !Directory.Exists(projectRootDir))
                {
                    LabelError.Content = "Error on get project root path";
                    return false;
                }
                else if (string.IsNullOrWhiteSpace(TextSourcePath.Text))
                {
                    LabelError.Content = "Source directory path is empty";
                    return false;
                }
                else if (!Directory.Exists(TextSourcePath.Text) && !Directory.Exists(projectRootDir + TextSourcePath.Text))
                {
                    LabelError.Content = "Directory not found";
                    return false;
                }
                else
                {
                    if (FileSystemUtil.IsSubfolder(projectRootDir, TextSourcePath.Text))
                    {
                        return true;
                    }
                    else if (FileSystemUtil.IsSubfolder(projectRootDir, projectRootDir + TextSourcePath.Text))
                    {
                        IsSourcePathRelative = true;
                        return true;
                    }
                    else
                    {
                        LabelError.Content = "Source directory must be project subdirectory";
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                LabelError.Content = "Unexpected error on validate source path";
                return false;
            }
        }
    }
}
