﻿using System.Windows;

namespace DialogReactor
{
    public partial class DialogEditNpcState : Window
    {
        public string AvatarKey { get { return textBoxAvatarKey.Text; } }
        public string TextRU { get { return textBoxTextRu.Text; } }
        public string TextEng { get { return textBoxTextEng.Text; } }

        public DialogEditNpcState()
        {
            InitializeComponent();
        }

        public DialogEditNpcState(IControlElement control)
        {
            InitializeComponent();

            if(control != null && control is ControlNpcStatement)
            {
                SetData(control as IControlElement);
            }
        }

        private void SetData(IControlElement control)
        {
            var data = control.GetContainedElement() as ElementNpcStatement;

            if(data != null)
            {
                textBoxAvatarKey.Text = data.AvatarKey;
                textBoxTextRu.Text = data.TextRU;
                textBoxTextEng.Text = data.TextEng;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
