﻿using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;

namespace DialogReactor
{
    public partial class DialogImportData : Window
    {
        public string ImportPath { get { return TextPath.Text; } }

        public DialogImportData()
        {
            InitializeComponent();
        }

        private void ButtonOk_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void ButtonSelectPath_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                TextPath.Text = dialog.SelectedPath + "\\";
                ValidateInput();
            }
        }

        private void TextPath_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidateInput();
        }

        private void ValidateInput()
        {
            if (TextPath == null)
            {
                return;
            }

            var validatePath = true;

            if (string.IsNullOrEmpty(TextPath.Text))
            {
                LabelError.Content = "Path is empty";
                validatePath = false;
            }
            else if (!Directory.Exists(TextPath.Text))
            {
                LabelError.Content = "Directory not found";
                validatePath = false;
            }

            if (validatePath)
            {
                LabelError.Content = string.Empty;
                ButtonOk.IsEnabled = true;
            }
            else
            {
                ButtonOk.IsEnabled = false;
            }
        }
    }
}
