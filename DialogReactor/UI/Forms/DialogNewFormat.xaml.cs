﻿using System.Windows;
using System.Windows.Controls;

namespace DialogReactor
{
    public partial class DialogNewFormat : Window
    {
        private ProjectHandler Project;
        private ProjectFolder TargetFolder;

        public string FormatName { get { return TextName.Text; } }

        public DialogNewFormat()
        {
            InitializeComponent();
        }

        public DialogNewFormat(ProjectHandler project, ProjectFolder folder)
        {
            Project = project;
            TargetFolder = folder;

            InitializeComponent();
        }

        private void TextName_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (Project.ContainsFormat(TextName.Text))
            {
                LabelError.Content = "Format with that name already exist in the project";
                ButtonOk.IsEnabled = false;
            }
            else
            {
                if (string.IsNullOrWhiteSpace(TextName.Text))
                {
                    LabelError.Content = "Name cant be empty";
                    ButtonOk.IsEnabled = false;
                }
                else
                {
                    LabelError.Content = string.Empty;
                    ButtonOk.IsEnabled = true;
                }
            }
        }

        private void ButtonOk_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}