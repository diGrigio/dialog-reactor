﻿using System.IO;
using System.Windows;

namespace DialogReactor
{
    public partial class DialogNewDialog : Window
    {
        private ProjectTree Tree;
        private ProjectFolder ProjectFolder;

        public string DialogTitle { get { return TextTitle.Text; } }
        public string DialogId { get { return TextDialogId.Text; } }
        public string DialogAvatarKey { get { return TextAvatarKey.Text; } }

        public DialogNewDialog(ProjectTree tree, ProjectFolder folder)
        {
            InitializeComponent();

            Tree = tree;
            ProjectFolder = folder;
        }

        private void ButtonOk_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void TextAvatarKey_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            ValidateInput();
        }

        private void TextTitle_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            ValidateInput();
        }
        
        private void ValidateInput()
        {
            if (TextTitle == null || TextAvatarKey == null)
            {
                return;
            }

            if (ValidateTitle())
            {
                LabelError.Content = string.Empty;
                ButtonOk.IsEnabled = true;
            }
            else
            {
                ButtonOk.IsEnabled = false;
            }
        }

        private bool ValidateTitle()
        {
            if (string.IsNullOrEmpty(TextTitle.Text))
            {
                LabelError.Content = "Title cant be empty";
                return false;
            }
            else
            {
                if (ProjectFolder.ContainsKey(TextTitle.Text))
                {
                    // check existing file
                    if (File.Exists(ProjectFolder.GetRelativePath(Tree) + TextTitle.Text + ".xml"))
                    {
                        LabelError.Content = "Dialog with this title already exist";
                        return false;
                    }
                }

                return true;
            }
        }
    }
}