﻿using DialogReactor.Elements;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Media;

namespace DialogReactor
{
    public partial class DialogNewElement : Window
    {
        private System.Windows.Forms.ColorDialog ColorPickDialog;

        public string ElementTtitle { get { return TextBoxTitle.Text; } }
        public ObservableCollection<ElementField> Fields { get; private set; }
        public Color ElementColor { get; private set; }

        private Format TargetFormat;

        public DialogNewElement(Format format)
        {
            InitializeComponent();
            TargetFormat = format;

            ColorPickDialog = new System.Windows.Forms.ColorDialog();
            ColorPickDialog.CustomColors = new int[] { System.Drawing.ColorTranslator.ToOle(System.Drawing.ColorTranslator.FromHtml("#2980b9")) };
            ElementColor = (Color)ColorConverter.ConvertFromString("#2980b9");

            Fields = new ObservableCollection<ElementField>();
            GridFields.ItemsSource = Fields;

            SetDefaultValues();
        }

        private void SetDefaultValues()
        {
            var elementTtitle = new ElementField("Text");
            elementTtitle.DataType = ElementFieldDataType.String;
            elementTtitle.FieldPreview = true;

            Fields.Add(elementTtitle);
            TextBoxTitle.Text = "New element";
            Validate();
        }

        private void Validate()
        {
            bool validate = true;

            if (string.IsNullOrWhiteSpace(TextBoxTitle.Text))
            {
                LabelError.Content = "Element title cant be empty";
                validate = false;
            }
            else if (TargetFormat.ContainsElement(TextBoxTitle.Text))
            {
                LabelError.Content = "Element already exist in the format";
                validate = false;
            }
            else
            {
                LabelError.Content = string.Empty;
            }

            ButtonOk.IsEnabled = validate;
        }

        private void ButtonOk_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void AddField_Click(object sender, RoutedEventArgs e)
        {
            Fields.Add(new ElementField("New field " + Fields.Count));
            Validate();
        }

        private void RemoveField_Click(object sender, RoutedEventArgs e)
        {
            if(GridFields.SelectedItem != null && GridFields.SelectedItem is ElementField)
            {
                Fields.Remove(GridFields.SelectedItem as ElementField);
                Validate();
            }
        }

        private void LineUp_Click(object sender, RoutedEventArgs e)
        {
            if(GridFields.SelectedItem != null)
            {
                if(GridFields.SelectedIndex > 0)
                {
                    Fields.Move(GridFields.SelectedIndex, GridFields.SelectedIndex - 1);
                }
            }
        }

        private void LineDown_Click(object sender, RoutedEventArgs e)
        {
            if (GridFields.SelectedItem != null)
            {
                if (GridFields.SelectedIndex < Fields.Count - 1)
                {
                    Fields.Move(GridFields.SelectedIndex, GridFields.SelectedIndex + 1);
                }
            }
        }

        private void ChangeColor(object sender, RoutedEventArgs e)
        {
            var element = ColorPreview.Template.FindName("ColorPreview", ColorPreview);

            if(element != null && element is System.Windows.Shapes.Rectangle)
            {
                // Warning! Here used Windows Forms data over WPF.
                var preview = element as System.Windows.Shapes.Rectangle;
                var currentColor = (preview.Fill as SolidColorBrush).Color;

                if (ColorPickDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    ElementColor = Color.FromRgb(ColorPickDialog.Color.R, ColorPickDialog.Color.G, ColorPickDialog.Color.B);
                    preview.Fill = new SolidColorBrush(ElementColor);
                }
            }
        }

        private void TextBoxTitle_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            Validate();
        }
    }
}
