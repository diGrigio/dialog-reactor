﻿using System.Windows;

namespace DialogReactor
{
    public partial class DialogAbout : Window
    {
        public DialogAbout()
        {
            InitializeComponent();
            label.Content = "Version: " + Version.Title();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
