﻿using System.Windows;

namespace DialogReactor
{
    public partial class DialogEditDialog : Window
    {
        public string DialogId { get { return TextDialogId.Text; } }
        public string AvatarKey { get { return TextAvatarKey.Text; } }

        public DialogEditDialog()
        {
            InitializeComponent();
        }

        public DialogEditDialog(Dialog dialog)
        {
            InitializeComponent();

            TextTitle.Text = dialog.Title;
            TextDialogId.Text = dialog.DialogId;
            TextAvatarKey.Text = dialog.AvatarKey;
        }

        private void ButtonOk_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
