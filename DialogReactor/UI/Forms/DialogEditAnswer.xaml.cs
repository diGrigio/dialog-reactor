﻿using System.Windows;

namespace DialogReactor
{
    public partial class DialogEditAnswer : Window
    {
        public int Priority {
            get
            {
                int priority = 0;
                if(int.TryParse(textBoxPriority.Text, out priority))
                {
                    return priority;
                }
                else
                {
                    return 0;
                }
            }
        }

        public string TextRU { get { return textBoxTextRu.Text; } }
        public string TextEng { get { return textBoxTextEng.Text; } }
        public string Conditions { get { return textBoxTextConditions.Text; } }

        private string TmpEndKey = "0";

        public int EndDialogKey {
            get
            {
                int key = 0;
                if(int.TryParse(textBoxEndDialogKey.Text, out key))
                {
                    return key;
                }
                else
                {
                    return 0;
                }
            }
        }

        public string Flag { get { return textBoxEndFlag.Text; } }


        public DialogEditAnswer()
        {
            InitializeComponent();
        }

        public DialogEditAnswer(ElementAnswer data)
        {
            InitializeComponent();
            SetData(data);
        }

        private void SetData(ElementAnswer data)
        {
            if(data != null)
            {
                textBoxPriority.Text = "" + data.Priority;
                textBoxTextRu.Text = data.TextRU;
                textBoxTextEng.Text = data.TextEng;
                textBoxTextConditions.Text = data.Conditions;
                textBoxEndDialogKey.Text = "" + data.EndDialogKey;
                textBoxEndFlag.Text = data.AnswerFlags;
            }
        }

        private void textBoxEndDialogKey_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            int value;

            if (int.TryParse(textBoxEndDialogKey.Text, out value))
            {
                TmpEndKey = textBoxEndDialogKey.Text;
            }
            else
            {
                textBoxEndDialogKey.Text = TmpEndKey;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}