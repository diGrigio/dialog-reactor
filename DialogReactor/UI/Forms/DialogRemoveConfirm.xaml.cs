﻿using System.Windows;

namespace DialogReactor
{
    public partial class DialogRemoveConfirm : Window
    {
        public DialogRemoveConfirm()
        {
            InitializeComponent();
        }

        public void SetFileTitle(string title)
        {
            label.Content = "Remove \"" + title + "\"?";
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
