﻿using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace DialogReactor
{
    public partial class DialogNewFolder : Window
    {
        private ProjectTree Tree;
        private ProjectFolder ProjectFolder;

        public string FolderTitle { get { return TextTitle.Text; } }

        public DialogNewFolder(ProjectTree tree, ProjectFolder projectFolder)
        {
            InitializeComponent();

            Tree = tree;
            ProjectFolder = projectFolder;
        }

        private void ButtonOk_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void TextTitle_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidateInput();
        }

        private void ValidateInput()
        {
            if (TextTitle == null)
            {
                return;
            }

            if (ValidateTitle())
            {
                LabelError.Content = string.Empty;
                ButtonOk.IsEnabled = true;
            }
            else
            {
                ButtonOk.IsEnabled = false;
            }
        }

        private bool ValidateTitle()
        {
            if (string.IsNullOrEmpty(TextTitle.Text))
            {
                LabelError.Content = "Title cant be empty";
                return false;
            }
            else
            {
                if (ProjectFolder.FoldersContainsKey(TextTitle.Text))
                {
                    if (Directory.Exists(ProjectFolder.GetRelativePath(Tree) + TextTitle.Text))
                    {
                        LabelError.Content = "Folder with this title already exist";
                        return false;
                    }
                }

                return true;
            }
        }
    }
}
