﻿using System;
using System.IO;
using System.Xml;

namespace DialogReactor
{
    public static class XmlProjectFileManager
    {
        public const string NodeInfo = "info";
        public const string NodeTitle = "title";
        public const string NodeContent = "content";
        public const string NodeType = "type";
        public const string NodePath = "path";

        public const string ProjectFiles = "/project/files";
        public const string ProjectFileDescriptor = "/project/files/file";

        public const string ProjectInfoTitle = "/project/info/title";
        public const string ProjectInfoSource = "/project/info/source";

        public static void WriteXML(XmlDocument doc, string filePath)
        {
            Directory.CreateDirectory(new FileInfo(filePath).DirectoryName);
            
            var settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.NewLineOnAttributes = true;

            var writer = XmlWriter.Create(filePath, settings);
            doc.Save(writer);
            writer.Close();
        }

        public static string FindXMLNode(XmlDocument doc, string path)
        {
            XmlNodeList xnList = doc.SelectNodes(path);

            foreach (XmlNode xn in xnList)
            {
                return xn.InnerText;
            }

            return null;
        }

        public static void CreateDialogFile(string dialogTitle, string dialogId, string dialogAvatarKey, string xmlPath)
        {
            XmlDocument xml = new XmlDocument();

            var dialog = xml.AppendChild(xml.CreateElement("dialog")) as XmlElement;
            {
                // info
                var info = dialog.AppendChild(xml.CreateElement(NodeInfo)) as XmlElement;
                {
                    var title = info.AppendChild(xml.CreateElement(NodeTitle)) as XmlElement;
                    title.InnerText = dialogTitle;

                    var id = info.AppendChild(xml.CreateElement("dialogId")) as XmlElement;
                    id.InnerText = dialogId;

                    var avatarKey = info.AppendChild(xml.CreateElement("avatarKey")) as XmlElement;
                    avatarKey.InnerText = dialogAvatarKey;
                }

                // data
                dialog.AppendChild(xml.CreateElement("elements"));
                dialog.AppendChild(xml.CreateElement("links"));
            }

            WriteXML(xml, xmlPath);
        }

        public static void CreateFormatFile(string formatName, string xmlPath)
        {
            XmlDocument xml = new XmlDocument();

            var format = xml.AppendChild(xml.CreateElement("format")) as XmlElement;
            {
                var info = format.AppendChild(xml.CreateElement(NodeInfo)) as XmlElement;
                {
                    var title = info.AppendChild(xml.CreateElement(NodeTitle)) as XmlElement;
                    title.InnerText = formatName;
                }

                format.AppendChild(xml.CreateElement("elements"));
                format.AppendChild(xml.CreateElement("linkRules"));
                format.AppendChild(xml.CreateElement("importRules"));
                format.AppendChild(xml.CreateElement("exportRules"));
            }

            WriteXML(xml, xmlPath);
        }

        public static void AddFolder(ProjectHandler project, ProjectFolder folder)
        {
            var xml = project.ProjectXml;

            if (!ProjectContainsEqualFile(project, folder))
            {
                var files = xml.SelectSingleNode(ProjectFiles) as XmlElement;
                var file = files.AppendChild(xml.CreateElement("file")) as XmlElement;

                {
                    var title = file.AppendChild(xml.CreateElement(NodeTitle)) as XmlElement;
                    title.InnerText = folder.Title;

                    var contentType = file.AppendChild(xml.CreateElement(NodeContent)) as XmlElement;
                    contentType.InnerText = ProjectFile.GetTypeName(folder.ContentType);

                    var fileType = file.AppendChild(xml.CreateElement(NodeType)) as XmlElement;
                    fileType.InnerText = ProjectFile.FileTypeFolder;

                    var path = file.AppendChild(xml.CreateElement(NodePath)) as XmlElement;
                    path.InnerText = folder.Parent.GetRelativePath(project.ProjectTree);
                }

                xml.Save(project.ProjectXmlPath);
            }
        }

        public static void AddContentFile(ProjectHandler project, ContentFile contentFile)
        {
            if(!ProjectContainsEqualFile(project, contentFile))
            {
                var xml = project.ProjectXml;

                var files = xml.SelectSingleNode(ProjectFiles) as XmlElement;
                var file = files.AppendChild(xml.CreateElement("file")) as XmlElement;
                {
                    var title = file.AppendChild(xml.CreateElement(NodeTitle)) as XmlElement;
                    title.InnerText = contentFile.Title;

                    var folder = file.AppendChild(xml.CreateElement(NodeContent)) as XmlElement;
                    folder.InnerText = ProjectFile.GetTypeName(contentFile.ContentType);

                    var type = file.AppendChild(xml.CreateElement(NodeType)) as XmlElement;
                    type.InnerText = ProjectFile.FileTypeContentFile;

                    var path = file.AppendChild(xml.CreateElement(NodePath)) as XmlElement;
                    path.InnerText = contentFile.Parent.GetRelativePath(project.ProjectTree);
                }

                project.ProjectXml.Save(project.ProjectXmlPath);
            }
        }

        private static bool IsFileEqualNode(ProjectHandler project, XmlNode node, ProjectFile file)
        {
            if(file == null || file.Parent == null)
            {
                return false;
            }

            if (node.SelectSingleNode(NodeTitle).InnerText.Equals(file.Title)
             && node.SelectSingleNode(NodePath).InnerText.Equals(file.Parent.GetRelativePath(project.ProjectTree))
             && node.SelectSingleNode(NodeType).InnerText.Equals(ProjectFile.GetFileType(file))
             && node.SelectSingleNode(NodeContent).InnerText.Equals(ProjectFile.GetTypeName(file.ContentType)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static bool ProjectContainsEqualFile(ProjectHandler project, ProjectFile file)
        {
            var files = project.ProjectXml.SelectNodes(ProjectFileDescriptor);

            if(files.Count > 0)
            {
                foreach (XmlNode node in files)
                {
                    if(IsFileEqualNode(project, node, file))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public static void RemoveFile(MainWindow window, ProjectHandler project, ProjectFile file)
        {
            XmlNodeList filesNodes = project.ProjectXml.SelectNodes(ProjectFileDescriptor);

            foreach(XmlElement node in filesNodes)
            {
                if (IsFileEqualNode(project, node, file))
                {
                    node.ParentNode.RemoveChild(node);
                    break;
                }
            }

            project.ProjectXml.Save(project.ProjectXmlPath);
        }

        public static void UpdateProjectSettings(ProjectHandler project)
        {
            XmlDocument xml = project.ProjectXml;

            XmlElement title = (XmlElement)xml.SelectSingleNode(ProjectInfoTitle);
            title.InnerText = project.Title;

            XmlElement source = (XmlElement)xml.SelectSingleNode(ProjectInfoSource);
            source.InnerText = project.SourceRelativeFolder;

            xml.Save(project.ProjectXmlPath);
        }
    }
}
