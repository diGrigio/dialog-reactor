﻿using DialogReactor.Util;
using System.Collections.Generic;
using System;

namespace DialogReactor
{
    public sealed class ProjectFolder : ProjectFile
    {
        public class FilesSorter : IComparer<ProjectFile>
        {
            public int Compare(ProjectFile x, ProjectFile y)
            {
                if(x == null || y == null)
                {
                    return 0;
                }

                if (x.GetType().Equals(y.GetType()))
                {
                    return string.Compare(x.Title, y.Title);
                }
                else
                {
                    if(x is ProjectFolder && !(y is ProjectFolder))
                    {
                        return -1;
                    }
                    else
                    {
                        return 1;
                    }
                }
            }
        }

        public SortableObservableCollection<ProjectFile> VisibleItems { get; private set; }

        private Dictionary<string, ProjectFolder> Folders;
        private Dictionary<string, ContentFile> ContentFiles;

        public Dictionary<string, ProjectFolder>.ValueCollection FoldersValues { get { return Folders.Values; } }
        public Dictionary<string, ContentFile>.ValueCollection ItemsValues { get { return ContentFiles.Values; } }

        public int FoldersCount { get { return Folders.Count; } }
        public int ItemsCount { get { return ContentFiles.Count; } }

        public bool IsRoot { get; set; }

        public ProjectFolder(ProjectFile parent, FileContentType type, string title) : base(parent, type, title)
        {
            VisibleItems = new SortableObservableCollection<ProjectFile>();
            Folders = new Dictionary<string, ProjectFolder>();
            ContentFiles = new Dictionary<string, ContentFile>();
        }

        public void Add(ContentFile file)
        {
            if(file != null)
            {
                if (!ContentFiles.ContainsKey(file.Title))
                {
                    ContentFiles.Add(file.Title, file);
                    VisibleItems.Add(file);
                    VisibleItems.Sort(value => value, new FilesSorter());
                }
            }
        }

        public void Remove(string title)
        {
            Remove(Get(title));
        }

        public void Remove(ContentFile file)
        {
            if(file != null)
            {
                ContentFiles.Remove(file.Title);
                VisibleItems.Remove(file);
            }
        }

        public ContentFile Get(string title)
        {
            ContentFile file = null;

            if(ContentFiles.TryGetValue(title, out file))
            {
                return file;
            }
            else
            {
                return null;
            }
        }

        public bool ContainsKey(string title)
        {
            return ContentFiles.ContainsKey(title);
        }

        public void Clear()
        {
            ContentFiles.Clear();
        }

        public void AddFolder(ProjectFolder folder)
        {
            if(folder != null)
            {
                if (!Folders.ContainsKey(folder.Title))
                {
                    Folders.Add(folder.Title, folder);
                    VisibleItems.Add(folder);
                    VisibleItems.Sort(value => value, new FilesSorter());
                }
            }
        }

        public void RemoveFolder(ProjectFolder folder)
        {
            if(folder != null)
            {
                Folders.Remove(folder.Title);
                VisibleItems.Remove(folder);
            }
        }

        public void RemoveFolder(string title)
        {
            ProjectFolder folder = GetFolder(title);

            if(folder != null)
            {
                Folders.Remove(title);
                VisibleItems.Remove(folder);
            }
        }

        public ProjectFolder GetFolder(string title)
        {
            ProjectFolder folder = null;
            if(Folders.TryGetValue(title, out folder))
            {
                return folder;
            }
            else
            {
                return null;
            }
        }

        public bool FoldersContainsKey(string title)
        {
            return Folders.ContainsKey(title);
        }

        internal void ClearFolders()
        {
            Folders.Clear();
        }
    }
}