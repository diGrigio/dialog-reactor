﻿namespace DialogReactor
{
    public enum FileContentType
    {
        Common,
        Dialog,
        Format,
    }

    public abstract class ContentFile : ProjectFile
    {
        public ContentFile(ProjectFile parent, FileContentType contentType, string title) : base(parent, contentType, title)
        {

        }
    }
}
