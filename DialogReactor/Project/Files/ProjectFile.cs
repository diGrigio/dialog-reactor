﻿namespace DialogReactor
{
    public abstract class ProjectFile
    {
        public const string FileTypeFolder = "folder";
        public const string FileTypeContentFile = "contentfile";

        private const string ContentTypeCommon = "common";
        private const string ContentTypeDialog = "dialog";
        private const string ContentTypeFormat = "format";

        public ProjectFile Parent { get; private set; }
        public FileContentType ContentType { get; private set; }

        public string Title { get; set; }

        protected ProjectFile(ProjectFile parent, FileContentType contentType, string title)
        {
            Title = title;
            ContentType = contentType;
            Parent = parent;
        }

        public string GetRelativePath(ProjectTree tree)
        {
            if(tree == null || this == tree || (this is ProjectFolder && (this as ProjectFolder).IsRoot))
            {
                return string.Empty;
            }
            
            string path = Title;
            ProjectFile parent = Parent;

            while (parent != null && parent is ProjectFolder && !(parent as ProjectFolder).IsRoot)
            {
                path = parent.Title + "\\" + path;
                parent = parent.Parent;
            }

            if(this is ProjectFolder)
            {
                path += "\\";
            }
            
            return path;
        }

        public static string GetFileType(ProjectFile file)
        {
            if (file is ContentFile)
            {
                return FileTypeContentFile;
            }
            else if (file is ProjectFolder)
            {
                return FileTypeFolder;
            }
            else
            {
                return string.Empty;
            }
        }

        public static string GetTypeName(FileContentType type)
        {
            switch (type)
            {
                case FileContentType.Common: return ContentTypeCommon;
                case FileContentType.Dialog: return ContentTypeDialog;
                case FileContentType.Format: return ContentTypeFormat;
            }

            return string.Empty;
        }

        public static FileContentType GetTypeByName(string type)
        {
            switch (type)
            {
                case ContentTypeDialog: return FileContentType.Dialog;
                case ContentTypeFormat: return FileContentType.Format;
                case ContentTypeCommon:
                default:
                    return FileContentType.Common;
            }
        }
    }
}
