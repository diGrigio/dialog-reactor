﻿using DialogReactor.Elements;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DialogReactor
{
    public class Format : ContentFile
    {
        public ObservableCollection<CustomElement> Items { get; private set; }

        private Dictionary<string, CustomElement> Elements;

        public Format(ProjectFile parent, string title) : base(parent, FileContentType.Format, title)
        {
            Items = new ObservableCollection<CustomElement>();
            Elements = new Dictionary<string, CustomElement>();
        }

        public void AddElement(CustomElement element)
        {
            if (!Elements.ContainsKey(element.Title))
            {
                Items.Add(element);
                Elements.Add(element.Title, element);
            }
        }

        public bool ContainsElement(string title)
        {
            return Elements.ContainsKey(title);
        }
    }
}