﻿using System.IO;
using System.Xml;

namespace DialogReactor
{
    public static class ProjectCreator
    {
        public static void CreateNewProject(string title, string path, string sourcePath)
        {
            // build project file
            var doc = new XmlDocument();

            var project = doc.AppendChild(doc.CreateElement("project")) as XmlElement;
            {
                // version
                {
                    var version = project.AppendChild(doc.CreateElement("version")) as XmlElement;

                    var major = version.AppendChild(doc.CreateElement("major")) as XmlElement;
                    major.InnerText = Version.Major;

                    var minor = version.AppendChild(doc.CreateElement("minor")) as XmlElement;
                    minor.InnerText = Version.Minor;
                }

                // project info
                {
                    var info = project.AppendChild(doc.CreateElement("info")) as XmlElement;

                    var projectTitle = info.AppendChild(doc.CreateElement("title")) as XmlElement;
                    projectTitle.InnerText = title;

                    var sourceFolder = info.AppendChild(doc.CreateElement("source")) as XmlElement;
                    sourceFolder.InnerText = sourcePath;
                }

                // files
                {
                    project.AppendChild(doc.CreateElement("files"));
                }
            }

            XmlProjectFileManager.WriteXML(doc, path + title.ToLower() + ".dialog");

            // source folder
            var projectFilesDir = path + sourcePath;
            var projectDialogsDir = projectFilesDir + "\\dialogs\\";
            var projectFormatsDir = projectFilesDir + "\\formats\\";

            if (!Directory.Exists(projectFilesDir))
            {
                Directory.CreateDirectory(projectFilesDir);
            }

            if(!Directory.Exists(projectDialogsDir))
            {
                Directory.CreateDirectory(projectDialogsDir);
            }

            if (!Directory.Exists(projectFormatsDir))
            {
                Directory.CreateDirectory(projectFormatsDir);
            }
        }
    }
}