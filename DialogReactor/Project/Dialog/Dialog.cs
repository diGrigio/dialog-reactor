﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml;

namespace DialogReactor
{
    public class Dialog : ContentFile
    {
        public string XmlFilePath { get; set; }
        public XmlDocument XmlFile { get; set; }

        public Collection<Element> Elements { get; private set; }
        public Collection<Link> Links { get; private set; }

        private int elementId = 0;
        public int LastElementId { get { return elementId++; } }

        public string DialogId { get; set; }
        public string AvatarKey { get; set; }

        public ClosableTab TabItem { get; set; }

        public Dialog(ProjectFolder parent, string title, string dialogId, string avatarKey, string xmlFilePath) : base(parent, FileContentType.Dialog, title)
        {
            Elements = new Collection<Element>();
            Links = new Collection<Link>();

            DialogId = dialogId;
            AvatarKey = avatarKey;

            if (!string.IsNullOrWhiteSpace(xmlFilePath))
            {
                XmlFilePath = xmlFilePath;
                XmlFile = new XmlDocument();

                if (File.Exists(XmlFilePath))
                {
                    try
                    {
                        XmlFile.Load(XmlFilePath);
                    }
                    catch (IOException e)
                    {
                        throw e;
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                }
                else
                {
                    throw new FileNotFoundException();
                }
            }
            else
            {
                throw new ArgumentException();
            }
        }

        internal void AddElement(Element element, bool rewriteXml)
        {
            if (element != null)
            {
                Elements.Add(element);

                if (rewriteXml)
                {
                    Save();
                }
            }
        }

        internal void UpdateElementId(int id)
        {   // normalize elementId generation after dialog elements loading from XML
            elementId = Math.Max(id + 1, elementId + 1);
        }

        internal void RemoveElement(Element element, bool rewriteXml)
        {
            if(element != null)
            {
                Elements.Remove(element);

                if (rewriteXml)
                {
                    Save();
                }
            }
        }

        internal void AddLink(Link link, bool rewriteXml)
        {
            if (link != null)
            {
                Links.Add(link);

                if (rewriteXml)
                {
                    Save();
                }
            }
        }

        internal void RemoveLink(Link link, bool rewriteXml)
        {
            if (link != null)
            {
                Links.Remove(link);

                if (rewriteXml)
                {
                    Save();
                }
            }
        }

        internal void Save()
        {
            XmlDialogFileSaver.Save(TabItem.DialogReactorWindow, XmlFile, XmlFilePath, this);
        }

        internal ElementNpcStatement GetStartStatement()
        {
            foreach(Link link in Links)
            {
                if(link.From is ElementDialogStart)
                {
                    return link.To as ElementNpcStatement;
                }
            }

            return null;
        }

        internal Link SearchLink(Element from, Element to)
        {
            foreach (Link link in Links)
            {
                if (link.From == from && link.To == to)
                {
                    return link;
                }
            }

            return null;
        }
    }
}