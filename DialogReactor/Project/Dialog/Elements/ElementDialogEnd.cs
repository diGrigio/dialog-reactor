﻿using System;

namespace DialogReactor
{
    public class ElementDialogEnd : Element
    {
        public ElementDialogEnd(Dialog dialog) : base(dialog)
        {

        }

        public ElementDialogEnd(Dialog dialog, int id) : base(dialog, id)
        {

        }

        public override object[] GetArrayData()
        {
            return null;
        }

        public override void SetArrayData(object[] arr)
        {
            
        }
    }
}
