﻿using System.Xml;

namespace DialogReactor
{
    public class ElementNpcStatement : Element
    {
        const int ParamsCount = 3;

        public string AvatarKey { get; set; }
        public string TextRU { get; set; }
        public string TextEng { get; set; }

        public ElementNpcStatement(Dialog dialog) : base(dialog)
        {
            initDefaultValues();
        }

        public ElementNpcStatement(Dialog dialog, int id, XmlNode node) : base(dialog, id)
        {
            if (node == null)
            {
                initDefaultValues();
            }
            else
            {
                AvatarKey = node.SelectSingleNode("avatarKey").InnerText;
                TextRU = node.SelectSingleNode("textRU").InnerText;
                TextEng = node.SelectSingleNode("textENG").InnerText;
            }
        }

        private void initDefaultValues()
        {
            AvatarKey = "";
            TextRU = "Текст состояния NPC";
            TextEng = "NPC Statement text";
        }

        internal void Rewrite(ElementNpcStatement element)
        {
            AvatarKey = element.AvatarKey;
            TextRU = element.TextRU;
            TextEng = element.TextEng;
        }

        public override object[] GetArrayData()
        {
            object[] arr = new object[ParamsCount];

            arr[0] = AvatarKey;
            arr[1] = TextRU;
            arr[2] = TextEng;

            return arr;
        }

        public override void SetArrayData(object[] arr)
        {
            if (arr.Length == ParamsCount)
            {
                AvatarKey = (string)arr[0];
                TextRU = (string)arr[1];
                TextEng = (string)arr[2];
            }
        }
    }
}
