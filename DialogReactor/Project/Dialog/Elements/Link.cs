﻿namespace DialogReactor
{
    public class Link
    {
        public Element From { get; private set; }
        public Element To { get; private set; }
        public ControlLink Control { get; set; }
        
        public Link(Element from, Element to)
        {
            From = from;
            To = to;
        }

        public static bool BuildLinkPossibility(IControlElement from, IControlElement to)
        {
            if (from != null && to != null)
            {
                if (from is ControlDialogStart && to is ControlNpcStatement)
                {
                    return true;
                }
                else if (from is ControlNpcStatement && to is ControlAnswer)
                {
                    return true;
                }
                else if (from is ControlAnswer && to is ControlDialogEnd)
                {
                    return true;
                }
                else if (from is ControlAnswer && to is ControlNpcStatement)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
