﻿using System.Collections.ObjectModel;

namespace DialogReactor
{
    public abstract class Element
    {
        public const string TypeNull = "null";
        public const string TypeDialogStart = "dialogStart";
        public const string TypeDialogEnd = "dialogEnd";
        public const string TypeNpcStatement = "statement";
        public const string TypePlayerAnswer = "answer";
        public const string TypeLink = "link";

        public int Id { get; private set; }
        public Collection<Link> InputLinks { get; private set; }
        public Collection<Link> OutputLinks { get; private set; }
        public IControlElement Control { get; set; }

        public bool ValidationError {
            set
            {
                if (Control != null)
                {
                    Control.ValidationError(value);
                }
            }
        }

        protected Element(Dialog dialog)
        {
            if (dialog == null)
            {
                Id = -1; // null ID
            }
            else
            {
                Id = dialog.LastElementId;
            }

            InputLinks = new Collection<Link>();
            OutputLinks = new Collection<Link>();
        }

        protected Element(Dialog dialog, int id)
        {
            dialog.UpdateElementId(id);
            Id = id;
            InputLinks = new Collection<Link>();
            OutputLinks = new Collection<Link>();
        }

        public abstract object[] GetArrayData();
        public abstract void SetArrayData(object[] arr);

        public static string GetElementType(Element element)
        {
            if (element is ElementDialogStart)
            {
                return TypeDialogStart;
            }
            else if (element is ElementDialogEnd)
            {
                return TypeDialogEnd;
            }
            else if (element is ElementNpcStatement)
            {
                return TypeNpcStatement;
            }
            else if (element is ElementAnswer)
            {
                return TypePlayerAnswer;
            }
            else
            {
                return TypeNull;
            }
        }

        public static Element GetElement(Dialog dialog, string type)
        {
            if (type.Equals(TypeDialogStart))
            {
                return new ElementDialogStart(dialog);
            }
            else if (type.Equals(TypeDialogEnd))
            {
                return new ElementDialogEnd(dialog);
            }
            else if (type.Equals(TypeNpcStatement))
            {
                return new ElementNpcStatement(dialog);
            }
            else if (type.Equals(TypePlayerAnswer))
            {
                return new ElementAnswer(dialog);
            }
            else
            {
                return null;
            }
        }

        public static IControlElement BuildControlElement(Element element)
        {
            if (element is ElementDialogStart)
            {
                return new ControlDialogStart(element as ElementDialogStart);
            }
            else if (element is ElementDialogEnd)
            {
                return new ControlDialogEnd(element as ElementDialogEnd);
            }
            else if (element is ElementNpcStatement)
            {
                return new ControlNpcStatement(element as ElementNpcStatement);
            }
            else if (element is ElementAnswer)
            {
                return new ControlAnswer(element as ElementAnswer);
            }
            else
            {
                return null;
            }
        }
    }
}