﻿using System.Xml;

namespace DialogReactor
{
    public class ElementAnswer : Element
    {
        const int ParamsCount = 6;

        public int Priority { get; set; }
        public int EndDialogKey { get; set; }
        public string TextRU { get; set; }
        public string TextEng { get; set; }
        public string Conditions { get; set; }
        public string AnswerFlags { get; set; }

        public ElementAnswer(Dialog dialog) : base(dialog)
        {
            initDefaultValues();
        }

        public ElementAnswer(Dialog dialog, int id, XmlNode node) : base(dialog, id)
        {
            if (node == null)
            {
                initDefaultValues();
            }
            else
            {
                Priority = int.Parse(node.SelectSingleNode("priority").InnerText);
                EndDialogKey = int.Parse(node.SelectSingleNode("endDialogKey").InnerText);
                TextRU = node.SelectSingleNode("textRU").InnerText;
                TextEng = node.SelectSingleNode("textENG").InnerText;
                Conditions = node.SelectSingleNode("conditions").InnerText;
                AnswerFlags = node.SelectSingleNode("flags").InnerText;
            }
        }

        private void initDefaultValues()
        {
            Priority = 0;
            EndDialogKey = 0;
            TextRU = "Текст ответа";
            TextEng = "Answer text";
            Conditions = "";
            AnswerFlags = "";
        }

        internal void Rewrite(ElementAnswer element)
        {
            Priority = element.Priority;
            EndDialogKey = element.EndDialogKey;
            TextRU = element.TextRU;
            TextEng = element.TextEng;
            Conditions = element.Conditions;
            AnswerFlags = element.AnswerFlags;
        }

        public override object[] GetArrayData()
        {
            object[] arr = new object[ParamsCount];

            arr[0] = Priority;
            arr[1] = EndDialogKey;
            arr[2] = TextRU;
            arr[3] = TextEng;
            arr[4] = Conditions;
            arr[5] = AnswerFlags;

            return arr;
        }

        public override void SetArrayData(object[] arr)
        {
            if(arr.Length == ParamsCount)
            {
                Priority = (int)arr[0];
                EndDialogKey = (int)arr[1];
                TextRU = (string)arr[2];
                TextEng = (string)arr[3];
                Conditions = (string)arr[4];
                AnswerFlags = (string)arr[5];
            }
        }
    }
}