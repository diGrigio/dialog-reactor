﻿using System;

namespace DialogReactor
{
    public class ElementDialogStart : Element
    {
        public ElementDialogStart(Dialog dialog) : base(dialog)
        {
        }

        public ElementDialogStart(Dialog dialog, int id) : base(dialog, id)
        {
        }

        public override object[] GetArrayData()
        {
            return null;
        }

        public override void SetArrayData(object[] arr)
        {
            
        }
    }
}
