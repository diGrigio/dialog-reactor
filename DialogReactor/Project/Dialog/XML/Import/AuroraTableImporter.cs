﻿using System.Collections.Generic;

namespace DialogReactor
{
    public static class AuroraTableImporter
    {
        public static List<GridElement> BuildTable(MainWindow window, ImportFormat format, string importPathRu, string importPathEng)
        {
            if (importPathRu == null && importPathEng == null)
            {
                window.Log("Import dialog failed - no csv files pathes");
                return null;
            }

            var linesRu = TableReader.ReadTable(window, format, importPathRu);
            var linesEng = TableReader.ReadTable(window, format, importPathEng);

            if (linesRu == null && linesEng == null)
            {
                window.Log("Import dialog \"" + importPathRu + "\" failed - error 1");
                return null;
            }
            if((linesRu != null && linesRu.Length == 0) && (linesEng != null && linesEng.Length == 0))
            {
                window.Log("Import dialog \"" + importPathRu + "\" failed - error 2");
                return null;
            }
            if(linesRu != null && linesRu.Length == 0 && importPathRu == null)
            {
                window.Log("Import dialog failed - error 3");
                return null;
            }
            if(linesEng != null && linesEng.Length == 0 && importPathEng == null)
            {
                window.Log("Import dialog failed - error 4");
                return null;
            }

            // Parse elements
            var result = new List<GridElement>();
            var newIndexMap = new Dictionary<int, GridNpcState>(); // <oldId, state.newId>
            int idCounter = 1;

            ParseTable(window, result, Localization.RU, importPathRu, linesRu, newIndexMap, ref idCounter);
            ParseTable(window, result, Localization.ENG, importPathEng, linesEng, newIndexMap, ref idCounter);

            // Remap indexes
            foreach (GridElement element in result)
            {
                if (element is GridPlayerAnswer)
                {
                    var answer = element as GridPlayerAnswer;

                    GridNpcState state = null;
                    if (newIndexMap.TryGetValue(answer.TargetStateId, out state)) // oldId -> newId
                    {
                        answer.TargetStateId = state.Id;
                        answer.TargetState = state;
                    }
                    else
                    {
                        answer.TargetStateId = -1; // target not found
                    }
                }
            }

            if (result.Count > 0)
            {
                ImportGridBuilder.BuildGrid(window, result);
                return result;
            }
            else
            {
                return null;
            }
        }

        private static void ParseTable(MainWindow window, List<GridElement> result, Localization locale, string tableFilePath, string[] table, Dictionary<int, GridNpcState> newIndexMap, ref int idCounter)
        {
            if(table == null || tableFilePath == null)
            {
                return;
            }

            GridNpcState currentNpcState = null;
            bool blockAnswerParsing = false;

            for (int i = 0, answerPriority = 0; i < table.Length; ++i, ++answerPriority)
            {
                if (string.IsNullOrWhiteSpace(table[i]))
                {
                    window.LogMessage(LogLevel.Warning, "Import", "Line " + i + " is empty.", tableFilePath);
                    continue;
                }

                var args = table[i].TrimEnd(';', ' ').Split(';');
                var element = GetElement(args, idCounter++, locale);

                if (element == null)
                {
                    window.LogMessage(LogLevel.Error, "Import", "Cant build element in line " + i, tableFilePath);
                    continue;
                }

                // set parsed data
                if (element is GridNpcState)
                {
                    var state = element as GridNpcState;
                    answerPriority = 0;
                    blockAnswerParsing = false;

                    if (newIndexMap.ContainsKey(state.OriginalId))
                    {
                        GridNpcState tmpState = null;
                        if(newIndexMap.TryGetValue(state.OriginalId, out tmpState))
                        {
                            if (tmpState.ParsedFirst == locale)
                            {
                                window.LogMessage(LogLevel.Error, "Import", "NPC State index conflict id=" + tmpState.OriginalId, tableFilePath);
                                blockAnswerParsing = true;
                            }
                            else
                            {
                                state = tmpState;
                            }
                        }
                    }
                    else
                    {
                        newIndexMap.Add(state.OriginalId, state);
                        result.Add(state);
                    }
                    
                    state.SetData(args, locale);
                    currentNpcState = state;
                }
                else if (element is GridPlayerAnswer)
                {
                    if (!blockAnswerParsing)
                    {
                        GridPlayerAnswer answer = null;
                        if(currentNpcState.Answers.Count > 0)
                        {
                            foreach (GridPlayerAnswer tmpAnswer in currentNpcState.Answers)
                            {
                                if (tmpAnswer.Priority == answerPriority)
                                {
                                    answer = tmpAnswer;
                                    break;
                                }
                            }
                        }

                        if(answer == null)
                        {
                            answer = element as GridPlayerAnswer;
                            answer.SetAnswerData(currentNpcState, answerPriority);
                            
                            result.Add(answer);
                            currentNpcState.Answers.Add(answer);
                        }

                        answer.SetData(args, locale);
                    }
                    else
                    {
                        window.LogMessage(LogLevel.Warning, "Import", "Answer did not imported from line " + i + " due to a conflict of indixes.", tableFilePath);
                    }
                }
            }
        }

        private static GridElement GetElement(string[] args, int id, Localization locale)
        {
            if (args != null && args.Length > 0)
            {
                // elements CSV signature
                // npc state     [3]: id; text; avatar_key;
                // player answer [6]: __; text; target_state_id; condition; return_value; flag;

                int originalId = -1;
                if (!string.IsNullOrEmpty(args[0]) && int.TryParse(args[0], out originalId))
                {
                    return new GridNpcState(id, originalId, locale);
                }
                else
                {
                    return new GridPlayerAnswer(id);
                }
            }

            return null;
        }
    }
}
