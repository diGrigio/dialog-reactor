﻿using DialogReactor.Util;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;

namespace DialogReactor
{
    public static class TableReader
    {
        public static string[] ReadTable(MainWindow window, ImportFormat format, string importPath)
        {
            if(importPath == null)
            {
                return null;
            }

            if (format == ImportFormat.AuroraCSV)
            {
                return ReadCsvTable(window, importPath);
            }
            else if(format == ImportFormat.AuroraXLSX)
            {
                return ReadLinesMicrosoftExcel(window, importPath);
            }
            else
            {
                return null;
            }
        }

        private static string[] ReadCsvTable(MainWindow window, string importPath)
        {
            try
            {
                return FileEncodingDetector.RealAllLines(window, importPath);
            }
            catch (Exception e)
            {
                window.Log("Error on read csv table \"" + importPath + "\"\r\n" + e.StackTrace);
                return null;
            }
        }

        private static string[] ReadLinesMicrosoftExcel(MainWindow window, string importPath)
        {
            try
            {
                using (var package = new ExcelPackage(new FileInfo(importPath)))
                {
                    var table = new List<string>();
                    var workSheet = package.Workbook.Worksheets[1];
                    var start = workSheet.Dimension.Start;
                    var end = workSheet.Dimension.End;

                    for (int row = start.Row; row <= end.Row; row++)
                    {
                        var args = new List<string>();
                        for (int col = start.Column; col <= end.Column; col++)
                        {
                            if (workSheet.Cells[row, col] != null && workSheet.Cells[row, col].Text != null)
                            {
                                args.Add(workSheet.Cells[row, col].Text);
                            }
                            else
                            {
                                args.Add(string.Empty);
                            }
                        }

                        table.Add(string.Join(";", args.ToArray()));
                    }

                    return table.ToArray();
                }
            }
            catch (Exception e)
            {
                window.Log("Error on read xlsx table \"" + importPath + "\"\r\n" + e.StackTrace);
                return null;
            }
        }
    }
}
