﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace DialogReactor
{
    public static class XmlDialogTableImport
    {
        public static void Import(MainWindow window, ImportFormat format, Dialog dialog, string importPathRu, string importPathEng)
        {
            var xml = dialog.XmlFile;

            if(xml != null)
            {
                try
                {
                    var table = AuroraTableImporter.BuildTable(window, format, importPathRu, importPathEng);

                    if(table != null && table.Count > 0)
                    {
                        BuildElements(window, xml, table);
                        BuildLinksXml(xml, table);
                        xml.Save(dialog.XmlFilePath);

                        XmlDialogFileLoaderWithoutUi.Load(window, dialog);
                    }
                    else
                    {
                        window.LogMessage(LogLevel.Error, "Import", "Table is empty.", importPathRu);
                    }
                }
                catch (Exception e)
                {
                    window.LogMessage(LogLevel.Error, "Import", "Unexpected error. Check log.", importPathRu);
                    window.Log("Unexpected error on import dialog table " + e.StackTrace);
                }
            }
        }

        private static void BuildElements(MainWindow window, XmlDocument xml, List<GridElement> table)
        {
            var elements = xml.SelectSingleNode("/dialog/elements") as XmlElement;

            XmlElementsBuilder.AddStartDialog(xml, elements);

            foreach(GridElement element in table)
            {
                if(element is GridNpcState)
                {
                    XmlElementsBuilder.BuildStateXml(xml, elements, element as GridNpcState);
                }
                else if(element is GridPlayerAnswer)
                {
                    XmlElementsBuilder.BuildAnswerXml(xml, elements, element as GridPlayerAnswer);
                }
            }
        }
        
        private static void BuildLinksXml(XmlDocument xml, List<GridElement> table)
        {
            var links = xml.SelectSingleNode("/dialog/links") as XmlElement;

            BuildLink(xml, links, 0, 1); // start dialog

            foreach (GridElement element in table)
            {
                if(element is GridNpcState)
                {
                    BuildStateLinks(xml, links, element as GridNpcState);
                }
                else if(element is GridPlayerAnswer)
                {
                    BuildAnswerLinks(xml, links, element as GridPlayerAnswer);
                }
            }
        }

        private static void BuildLink(XmlDocument xml, XmlElement links, int from, int to)
        {
            var linkXml = links.AppendChild(xml.CreateElement("link")) as XmlElement;
            {
                var fromId = linkXml.AppendChild(xml.CreateElement("fromId")) as XmlElement;
                fromId.InnerText = "" + from;

                var toId = linkXml.AppendChild(xml.CreateElement("toId")) as XmlElement;
                toId.InnerText = "" + to;
            }
        }

        private static void BuildStateLinks(XmlDocument xml, XmlElement links, GridNpcState state)
        {
            foreach(GridPlayerAnswer answer in state.Answers)
            {
                BuildLink(xml, links, state.Id, answer.Id);
            }
        }

        private static void BuildAnswerLinks(XmlDocument xml, XmlElement links, GridPlayerAnswer answer)
        {
            if (answer.TargetStateId != -1)
            {
                BuildLink(xml, links, answer.Id, answer.TargetStateId);
            }
        }
    }
}