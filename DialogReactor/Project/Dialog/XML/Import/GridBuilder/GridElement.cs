﻿using System.Collections.Generic;

namespace DialogReactor
{
    public abstract class GridElement
    {
        public int Id { get; protected set; }

        public int Line { get; set; }
        public int Column { get; set; }

        public bool IsUsed { get; set; }

        public abstract void SetData(string[] args, Localization locale);

        protected GridElement()
        {
            Line = -1;
            Column = -1;
        }
    }

    public class GridNpcState : GridElement
    {
        public int OriginalId { get; private set; }

        public string TextRU { get; private set; }
        public string TextEng { get; private set; }
        public string AvatarKey { get; private set; }

        public List<GridPlayerAnswer> Answers { get; private set; }

        public Localization ParsedFirst { get; private set; }

        public GridNpcState(int id, int originalId, Localization locale) : base()
        {   // signature: id;text;avatar_key;
            Id = id;

            OriginalId = originalId;
            TextRU = string.Empty;
            TextEng = string.Empty;
            AvatarKey = string.Empty;

            Answers = new List<GridPlayerAnswer>();

            ParsedFirst = locale;
        }

        public override void SetData(string[] args, Localization locale)
        {
            if (args.Length > 1)
            {
                if(locale == Localization.RU)
                {
                    TextRU = args[1];
                }
                else if(locale == Localization.ENG)
                {
                    TextEng = args[1];
                }
            }

            if (args.Length > 2 && string.IsNullOrWhiteSpace(AvatarKey))
            {
                AvatarKey = args[2];
            }
        }
    }

    public class GridPlayerAnswer : GridElement
    {
        public int Priority { get; private set; }
        public string TextRU { get; private set; }
        public string TextEng { get; private set; }
        public int TargetStateId { get; set; }
        public string Conditions { get; private set; }
        public int EndDialogKey { get; private set; }
        public string AnswerFlags { get; private set; }

        public GridNpcState NpcState { get; private set; }
        public GridNpcState TargetState { get; set; }

        public GridPlayerAnswer(int id) : base()
        {
            Id = id;
            TextRU = string.Empty;
            TextEng = string.Empty;
            TargetStateId = -1;
            Conditions = string.Empty;
            EndDialogKey = 0;
            AnswerFlags = string.Empty;
        }

        public void SetAnswerData(GridNpcState state, int priority)
        {
            NpcState = state;
            Priority = priority;
        }

        public override void SetData(string[] args, Localization locale)
        {
            // TextRU \ TextEng
            if (args.Length > 1)
            {
                if(locale == Localization.RU)
                {
                    TextRU = args[1];
                }
                else if(locale == Localization.ENG)
                {
                    TextEng = args[1];
                }
            }

            if (args.Length > 2 && TargetStateId == -1)
            {
                int targetId;
                if (int.TryParse(args[2], out targetId))
                {
                    TargetStateId = targetId;
                }
            }

            if (args.Length > 3 && string.IsNullOrWhiteSpace(Conditions))
            {
                Conditions = args[3];
            }

            if (args.Length > 4 && EndDialogKey == 0)
            {
                int endDialogKey;
                if (int.TryParse(args[4], out endDialogKey))
                {
                    EndDialogKey = endDialogKey;
                }
            }

            if (args.Length > 5 && string.IsNullOrWhiteSpace(AnswerFlags))
            {
                AnswerFlags = args[5];
            }
        }
    }
}