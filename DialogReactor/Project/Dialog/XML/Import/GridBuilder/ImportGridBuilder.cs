﻿using System;
using System.Collections.Generic;

namespace DialogReactor
{
    static class ImportGridBuilder
    {
        public static void BuildGrid(MainWindow window, List<GridElement> table)
        {
            try
            {
                GridNpcState root = GetRoot(table);
                if (root == null)
                {
                    return;
                }
                if (root.Answers.Count == 0)
                {
                    return;
                }

                var currentLayer = new List<GridNpcState>();
                var nextLayer = new List<GridNpcState>();
                var columnsInLines = new List<int>();
                int line = 0;
                int column = 0;

                currentLayer.Add(root);
                while (currentLayer.Count > 0)
                {
                    var state = currentLayer[0];
                    if (!state.IsUsed)
                    {
                        state.IsUsed = true;
                        state.Line = line;

                        var nextLayerElements = GetNextLayerElements(state, line, ref column);
                        if (nextLayerElements != null)
                        {
                            nextLayer.AddRange(nextLayerElements);
                        }
                    }

                    currentLayer.RemoveAt(0);
                    if (currentLayer.Count == 0 && nextLayer.Count > 0)
                    {
                        currentLayer.AddRange(nextLayer);
                        nextLayer.Clear();
                        line++;
                        columnsInLines.Add(column);
                        column = 0;
                    }
                    else if(currentLayer.Count == 0 && nextLayer.Count == 0)
                    {
                        // shift unused states to end
                        line = 0;
                        if (columnsInLines.Count > 0)
                        {
                            column = columnsInLines[0] + 1;
                        }

                        foreach (GridElement element in table)
                        {
                            if(element is GridNpcState && !element.IsUsed)
                            {
                                element.Column = column;
                                currentLayer.Add(element as GridNpcState);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                window.Log(e.GetType() + " " + e.StackTrace);
            }
        }

        private static List<GridNpcState> GetNextLayerElements(GridNpcState state, int line, ref int column)
        {
            if (state.Answers.Count > 0)
            {
                var result = new List<GridNpcState>();
                int maxColumn = 0;

                foreach (GridPlayerAnswer answer in state.Answers)
                {
                    answer.Line = line;
                    answer.Column = column + answer.Priority;
                    maxColumn = answer.Column;

                    if(answer.TargetState != null)
                    {
                        if(answer.TargetState.Line == -1)
                        {
                            answer.TargetState.Column = answer.Column;
                        }
                        
                        result.Add(answer.TargetState);
                    }
                }

                column = ++maxColumn;
                return result;
            }

            return null;
        }

        private static GridNpcState GetRoot(List<GridElement> table)
        {
            if (table != null && table.Count > 0)
            {
                foreach (GridElement element in table)
                {
                    if (element is GridNpcState)
                    {
                        return element as GridNpcState;
                    }
                }
            }

            return null;
        }
    }
}
