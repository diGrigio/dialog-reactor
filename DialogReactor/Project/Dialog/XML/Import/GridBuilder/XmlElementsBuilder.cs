﻿using System;
using System.Xml;

namespace DialogReactor
{
    public static class XmlElementsBuilder
    {
        public static void AddStartDialog(XmlDocument xml, XmlElement elements)
        {
            var elementNode = GetElementNode(xml, elements);
            {
                var type = elementNode.AppendChild(xml.CreateElement("type")) as XmlElement;
                type.InnerText = Element.TypeDialogStart;

                var id = elementNode.AppendChild(xml.CreateElement("id")) as XmlElement;
                id.InnerText = "0";

                var posX = elementNode.AppendChild(xml.CreateElement("posX")) as XmlElement;
                var posY = elementNode.AppendChild(xml.CreateElement("posY")) as XmlElement;
                posX.InnerText = "183";
                posY.InnerText = "50";
            }
        }

        public static void BuildStateXml(XmlDocument xml, XmlElement elements, GridNpcState state)
        {
            var elementNode = GetElementNode(xml, elements);
            {
                var type = elementNode.AppendChild(xml.CreateElement("type")) as XmlElement;
                type.InnerText = Element.TypeNpcStatement;

                var id = elementNode.AppendChild(xml.CreateElement("id")) as XmlElement;
                id.InnerText = "" + state.Id;

                var posX = elementNode.AppendChild(xml.CreateElement("posX")) as XmlElement;
                var posY = elementNode.AppendChild(xml.CreateElement("posY")) as XmlElement;
                posX.InnerText = "" + (Math.Max(0, state.Column - 1) * 250 + 100);
                posY.InnerText = "" + (Math.Max(0, state.Line) * 300 + 150);

                var contentNode = GetContentNode(xml, elementNode);
                {
                    var avatarKey = contentNode.AppendChild(xml.CreateElement("avatarKey")) as XmlElement;
                    avatarKey.InnerText = state.AvatarKey;

                    var textRU = contentNode.AppendChild(xml.CreateElement("textRU")) as XmlElement;
                    textRU.InnerText = state.TextRU;

                    var textENG = contentNode.AppendChild(xml.CreateElement("textENG")) as XmlElement;
                    textENG.InnerText = state.TextEng;
                }
            }
        }

        public static void BuildAnswerXml(XmlDocument xml, XmlElement elements, GridPlayerAnswer answer)
        {
            var elementNode = GetElementNode(xml, elements);

            var type = elementNode.AppendChild(xml.CreateElement("type")) as XmlElement;
            type.InnerText = Element.TypePlayerAnswer;

            var id = elementNode.AppendChild(xml.CreateElement("id")) as XmlElement;
            id.InnerText = "" + answer.Id;

            var posX = elementNode.AppendChild(xml.CreateElement("posX")) as XmlElement;
            var posY = elementNode.AppendChild(xml.CreateElement("posY")) as XmlElement;
            posX.InnerText = "" + (Math.Max(0, answer.Column - 1) * 250 + 100);
            posY.InnerText = "" + (Math.Max(0, answer.Line) * 300 + 300);

            var contentNode = GetContentNode(xml, elementNode);
            {
                var priority = contentNode.AppendChild(xml.CreateElement("priority")) as XmlElement;
                priority.InnerText = "" + answer.Priority;

                var endDialogKey = contentNode.AppendChild(xml.CreateElement("endDialogKey")) as XmlElement;
                endDialogKey.InnerText = "" + answer.EndDialogKey;

                var textRU = contentNode.AppendChild(xml.CreateElement("textRU")) as XmlElement;
                textRU.InnerText = answer.TextRU;

                var textENG = contentNode.AppendChild(xml.CreateElement("textENG")) as XmlElement;
                textENG.InnerText = answer.TextEng;

                var conditions = contentNode.AppendChild(xml.CreateElement("conditions")) as XmlElement;
                conditions.InnerText = answer.Conditions;

                var flags = contentNode.AppendChild(xml.CreateElement("flags")) as XmlElement;
                flags.InnerText = answer.AnswerFlags;
            }
        }

        private static XmlElement GetElementNode(XmlDocument xml, XmlElement elements)
        {
            return elements.AppendChild(xml.CreateElement("element")) as XmlElement;
        }

        private static XmlElement GetContentNode(XmlDocument xml, XmlElement elementHeader)
        {
            return elementHeader.AppendChild(xml.CreateElement("content")) as XmlElement;
        }
    }
}
