﻿namespace DialogReactor
{
    public enum ImportFormat
    {
        DialogReactorXML,
        AuroraCSV,
        AuroraXLSX
    }
}
