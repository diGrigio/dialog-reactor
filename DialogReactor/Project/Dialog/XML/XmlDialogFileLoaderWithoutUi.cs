﻿using System.Xml;

namespace DialogReactor
{
    public static class XmlDialogFileLoaderWithoutUi
    {
        public static void Load(MainWindow window, Dialog dialog)
        {
            if (dialog == null)
            {
                window.Log("Dialog is null. Loading failed");
                return;
            }

            ClearDialog(dialog);
            LoadInfo(dialog);
            LoadElements(window, dialog);
            LoadLinks(window, dialog);
        }

        private static void ClearDialog(Dialog dialog)
        {
            dialog.Elements.Clear();
            dialog.Links.Clear();
        }

        private static void LoadInfo(Dialog dialog)
        {
            var dialogId = dialog.XmlFile.SelectSingleNode("/dialog/info/dialogId");
            dialog.DialogId = dialogId.InnerText;

            var avatarKey = dialog.XmlFile.SelectSingleNode("/dialog/info/avatarKey");
            dialog.AvatarKey = avatarKey.InnerText;
        }

        private static void LoadElements(MainWindow window, Dialog dialog)
        {
            var elementsNodes = dialog.XmlFile.SelectNodes("/dialog/elements/element");
            if (elementsNodes == null)
            {
                return;
            }

            if (elementsNodes.Count == 0)
            {
                // add dialog start
                var element = new ElementDialogStart(dialog);
            }
            else
            {
                // load elements
                foreach (XmlNode node in elementsNodes)
                {
                    var type = node.SelectSingleNode("type").InnerText;
                    var element = LoadElement(dialog, node, type);

                    if (element != null)
                    {
                        dialog.Elements.Add(element);
                    }
                }
            }
        }

        private static Element LoadElement(Dialog dialog, XmlNode node, string type)
        {
            if (string.IsNullOrEmpty(type) || type.Equals("null"))
            {
                return null;
            }

            int id = 0;
            if (int.TryParse(node.SelectSingleNode("id").InnerText, out id))
            {
                var content = node.SelectSingleNode("content");

                switch (type)
                {
                    case Element.TypeDialogStart:
                        return new ElementDialogStart(dialog, id);

                    case Element.TypeDialogEnd:
                        return new ElementDialogEnd(dialog, id);

                    case Element.TypeNpcStatement:
                        return new ElementNpcStatement(dialog, id, content);

                    case Element.TypePlayerAnswer:
                        return new ElementAnswer(dialog, id, content);
                }
            }

            return null;
        }

        private static void LoadLinks(MainWindow window, Dialog dialog)
        {
            XmlNodeList linksNodes = dialog.XmlFile.SelectNodes("/dialog/links/link");

            if (linksNodes == null || linksNodes.Count == 0)
            {
                return;
            }

            foreach (XmlNode node in linksNodes)
            {
                LoadLink(window, dialog, node);
            }
        }

        private static void LoadLink(MainWindow window, Dialog dialog, XmlNode node)
        {
            int from = -1;
            int to = -1;

            if (int.TryParse(node.SelectSingleNode("fromId").InnerText, out from)
             && int.TryParse(node.SelectSingleNode("toId").InnerText, out to))
            {
                if (from != -1 && to != -1)
                {
                    Element elementFrom = null;
                    Element elementTo = null;

                    foreach (Element element in dialog.Elements)
                    {
                        if (element.Id == from)
                        {
                            elementFrom = element;
                            continue;
                        }

                        if (element.Id == to)
                        {
                            elementTo = element;
                            continue;
                        }
                    }

                    if (elementFrom != null && elementTo != null)
                    {
                        var link = new Link(elementFrom, elementTo);
                        dialog.Links.Add(link);
                        elementFrom.OutputLinks.Add(link);
                        elementTo.InputLinks.Add(link);
                    }
                }
            }
        }
    }
}
