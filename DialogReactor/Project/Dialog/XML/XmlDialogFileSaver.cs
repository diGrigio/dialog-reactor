﻿using System;
using System.IO;
using System.Windows.Controls;
using System.Xml;

namespace DialogReactor
{
    internal static class XmlDialogFileSaver
    {
        internal static void Save(MainWindow window, XmlDocument xml, string filePath, Dialog dialog)
        {
             // info
            {
                XmlElement dialogId = (XmlElement)xml.SelectSingleNode("/dialog/info/dialogId");
                dialogId.InnerText = dialog.DialogId;

                XmlElement avatarKey = (XmlElement)xml.SelectSingleNode("/dialog/info/avatarKey");
                avatarKey.InnerText = dialog.AvatarKey;
            }

            // elements
            {
                XmlElement elements = (XmlElement)xml.SelectSingleNode("/dialog/elements");
                elements.RemoveAll();

                foreach (Element element in dialog.Elements)
                {
                    if(element.Id != -1)
                    {
                        BuildElementXml(xml, element);
                    }
                }
            }

            // links
            {
                XmlElement links = (XmlElement)xml.SelectSingleNode("/dialog/links");
                links.RemoveAll();

                foreach(Link link in dialog.Links)
                {
                    BuildLinkXml(xml, links, link);
                }
            }

            try
            {
                xml.Save(filePath);
            }
            catch (IOException)
            {
                window.Log("Error on save file \"" + filePath + "\": file used from another process");
            }
            catch (Exception e)
            {
                window.Log("Error on save file: " + e.ToString());
            }
        }

        private static void BuildElementXml(XmlDocument xml, Element element)
        {
            XmlElement xmlElements = (XmlElement)xml.SelectSingleNode("/dialog/elements");
            XmlElement xmlAddElement = (XmlElement)xmlElements.AppendChild(xml.CreateElement("element"));
            {
                XmlElement type = (XmlElement)xmlAddElement.AppendChild(xml.CreateElement("type"));
                type.InnerText = Element.GetElementType(element);

                XmlElement id = (XmlElement)xmlAddElement.AppendChild(xml.CreateElement("id"));
                id.InnerText = "" + element.Id;

                XmlElement posX = (XmlElement)xmlAddElement.AppendChild(xml.CreateElement("posX"));
                XmlElement posY = (XmlElement)xmlAddElement.AppendChild(xml.CreateElement("posY"));

                if(element.Control == null)
                {
                    posX.InnerText = "0";
                    posY.InnerText = "0";
                }
                else
                {
                    posX.InnerText = "" + (int)Canvas.GetLeft(element.Control as UserControl);
                    posY.InnerText = "" + (int)Canvas.GetTop(element.Control as UserControl);
                }

                XmlElement content = (XmlElement)xmlAddElement.AppendChild(xml.CreateElement("content"));
                {
                    BuildElementTypeContentToXml(xml, content, element);
                }
            }
        }

        private static void BuildElementTypeContentToXml(XmlDocument xml, XmlElement content, Element element)
        {
            if (element is ElementAnswer)
            {
                ElementAnswer answer = (ElementAnswer)element;

                XmlElement priority = (XmlElement)content.AppendChild(xml.CreateElement("priority"));
                priority.InnerText = "" + answer.Priority;

                XmlElement endDialogKey = (XmlElement)content.AppendChild(xml.CreateElement("endDialogKey"));
                endDialogKey.InnerText = "" + answer.EndDialogKey;

                XmlElement textRU = (XmlElement)content.AppendChild(xml.CreateElement("textRU"));
                textRU.InnerText = answer.TextRU;

                XmlElement textENG = (XmlElement)content.AppendChild(xml.CreateElement("textENG"));
                textENG.InnerText = answer.TextEng;

                XmlElement conditions = (XmlElement)content.AppendChild(xml.CreateElement("conditions"));
                conditions.InnerText = answer.Conditions;

                XmlElement flags = (XmlElement)content.AppendChild(xml.CreateElement("flags"));
                flags.InnerText = answer.AnswerFlags;
            }
            else if (element is ElementNpcStatement)
            {
                ElementNpcStatement statement = (ElementNpcStatement)element;

                XmlElement avatarKey = (XmlElement)content.AppendChild(xml.CreateElement("avatarKey"));
                avatarKey.InnerText = statement.AvatarKey;

                XmlElement textRU = (XmlElement)content.AppendChild(xml.CreateElement("textRU"));
                textRU.InnerText = statement.TextRU;

                XmlElement textENG = (XmlElement)content.AppendChild(xml.CreateElement("textENG"));
                textENG.InnerText = statement.TextEng;
            }
        }

        private static void BuildLinkXml(XmlDocument xml, XmlElement links, Link link)
        {
            XmlElement linkXml = (XmlElement)links.AppendChild(xml.CreateElement("link"));
            {
                XmlElement fromId = (XmlElement)linkXml.AppendChild(xml.CreateElement("fromId"));
                fromId.InnerText = "" + link.From.Id;

                XmlElement toId = (XmlElement)linkXml.AppendChild(xml.CreateElement("toId"));
                toId.InnerText = "" + link.To.Id;
            }
        }
    }
}