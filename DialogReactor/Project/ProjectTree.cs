﻿using System.Collections.ObjectModel;

namespace DialogReactor
{
    public class ProjectTree : ProjectFile
    {
        public ObservableCollection<ProjectFolder> Items { get; private set; }

        public ProjectFolder Dialogs { get; private set; }
        public ProjectFolder Formats { get; private set;}

        public ProjectTree(string projectTitle) : base(null, FileContentType.Common, projectTitle)
        {
            Items = new ObservableCollection<ProjectFolder>();

            //
            Dialogs = new ProjectFolder(this, FileContentType.Dialog, "Dialogs");
            Dialogs.IsRoot = true;
            Items.Add(Dialogs);

            //
            Formats = new ProjectFolder(this, FileContentType.Format, "Formats");
            Formats.IsRoot = true;
            Items.Add(Formats);
        }
    }
}
