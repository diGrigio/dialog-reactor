﻿namespace DialogReactor
{
    public class DialogReactorData
    {
        private MainWindow MainWindow;
        public ProjectHandler Project { get; set; }

        public string ProjectFile { get; set; }

        public DialogReactorData(MainWindow mainWindow)
        {
            MainWindow = mainWindow;
        }

        internal void ExportProject()
        {
            if(Project != null)
            {
                Builder.ExportProject(Project, MainWindow);
            }
        }
    }
}