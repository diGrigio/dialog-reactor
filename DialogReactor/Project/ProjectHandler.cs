﻿using DialogReactor.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using DialogReactor.Elements;

namespace DialogReactor
{
    public class ProjectHandler
    {
        private MainWindow Window;

        // xml
        public string ProjectXmlPath { get; private set; }
        public XmlDocument ProjectXml { get; private set; }

        // data
        public ProjectTree ProjectTree { get; private set; }
        public string Title { get; private set; }
        private Dictionary<string, Format> ProjectFormats;

        // paths
        public string ProjectPath { get; private set; }
        public string SourceRelativeFolder { get; private set; }

        private string SourceFolder { get; set; }
        private string DialogsFolder { get; set; }
        private string FormatsFolder { get; set; }

        public string ResultFolder { get; private set; }

        internal ProjectHandler(MainWindow window, string projectXmlPath, XmlDocument projectXml)
        {
            ProjectFormats = new Dictionary<string, Format>();

            var fileInfo = new FileInfo(projectXmlPath);
            var projectPath = fileInfo.DirectoryName;

            Window = window;
            ProjectXmlPath = projectXmlPath;
            ProjectXml = projectXml;
            
            Title = XmlProjectFileManager.FindXMLNode(projectXml, XmlProjectFileManager.ProjectInfoTitle);

            ProjectPath = projectPath;
            SourceRelativeFolder = XmlProjectFileManager.FindXMLNode(projectXml, XmlProjectFileManager.ProjectInfoSource);
            SourceFolder = projectPath + SourceRelativeFolder;

            DialogsFolder = SourceFolder + "dialogs\\";
            FormatsFolder = SourceFolder + "formats\\";

            ResultFolder = projectPath + "\\dialogs\\";
        }

        public string GetContentTypeDirectory(FileContentType type)
        {
            switch (type)
            {
                case FileContentType.Common: return SourceFolder;
                case FileContentType.Dialog: return DialogsFolder;
                case FileContentType.Format: return FormatsFolder;
            }

            return string.Empty;
        }

        public string GetFullFilePath(ProjectFile file)
        {
            return GetContentTypeDirectory(file.ContentType) + file.GetRelativePath(ProjectTree);
        }

        private ProjectFolder FindProjectFilder(FileContentType type, string path)
        {
            ProjectFolder targetFolder = null;
            switch (type)
            {
                case FileContentType.Dialog: targetFolder = ProjectTree.Dialogs; break;
                case FileContentType.Format: targetFolder = ProjectTree.Formats; break;
            }
            if (targetFolder == null)
            {
                return null;
            }

            var hierarchy = path.Split('\\');
            if (hierarchy.Length > 1)
            {
                for (int i = 0; i < hierarchy.Length - 1; ++i)
                {
                    if (targetFolder.FoldersContainsKey(hierarchy[i]))
                    {
                        var file = targetFolder.GetFolder(hierarchy[i]);
                        if (file != null)
                        {
                            targetFolder = file;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }

            return targetFolder;
        }

        public void LoadProject()
        {
            var filesNodesList = ProjectXml.SelectNodes(XmlProjectFileManager.ProjectFileDescriptor);

            if (filesNodesList != null)
            {
                // project tree
                ProjectTree = new ProjectTree(Title);
                Window.ProjectFileTree.Items.Add(ProjectTree);

                // visual
                Window.ProgressBarStart(filesNodesList.Count);

                //load folders
                {
                    // hierarchy sorting
                    var contentGroups = new Dictionary<string, SortedDictionary<int, List<XmlNode>>>();

                    foreach (XmlNode node in filesNodesList)
                    {
                        if (node.SelectSingleNode(XmlProjectFileManager.NodeType).InnerText.Equals(ProjectFile.FileTypeFolder))
                        {
                            var group = node.SelectSingleNode(XmlProjectFileManager.NodeContent).InnerText;

                            SortedDictionary<int, List<XmlNode>> contentFiles = null;
                            if(!contentGroups.TryGetValue(group, out contentFiles))
                            {
                                contentFiles = new SortedDictionary<int, List<XmlNode>>();
                                contentGroups.Add(group, contentFiles);
                            }

                            int level = node.SelectSingleNode(XmlProjectFileManager.NodePath).InnerText.Split('\\').Length;
                            List<XmlNode> list = null;
                            if (!contentFiles.TryGetValue(level, out list))
                            {
                                list = new List<XmlNode>();
                                contentFiles.Add(level, list);
                            }

                            list.Add(node);
                        }
                    }

                    foreach (string contentKey in contentGroups.Keys)
                    {
                        SortedDictionary<int, List<XmlNode>> contentFiles = null;
                        if (contentGroups.TryGetValue(contentKey, out contentFiles))
                        {
                            foreach (List<XmlNode> levelList in contentFiles.Values)
                            {
                                foreach (XmlNode node in levelList)
                                {
                                    LoadFolder(ProjectFile.GetTypeByName(contentKey), 
                                               node.SelectSingleNode(XmlProjectFileManager.NodeTitle).InnerText,
                                               node.SelectSingleNode(XmlProjectFileManager.NodePath).InnerText);

                                    Window.ProgressBarUpdateInc();
                                }
                            }
                        }
                    }
                }

                // load files
                {
                    foreach (XmlNode node in filesNodesList)
                    {
                        if (node.SelectSingleNode(XmlProjectFileManager.NodeType).InnerText.Equals(ProjectFile.FileTypeContentFile))
                        {
                            LoadFile(ProjectFile.GetTypeByName(node.SelectSingleNode(XmlProjectFileManager.NodeContent).InnerText),
                                     node.SelectSingleNode(XmlProjectFileManager.NodeTitle).InnerText,
                                     node.SelectSingleNode(XmlProjectFileManager.NodePath).InnerText);
                        }

                        Window.ProgressBarUpdateInc();
                    }
                }

                Window.ProgressBarUpdateEnd();
            }
        }

        private void LoadFolder(FileContentType type, string title, string path)
        {
            var destinationFolder = FindProjectFilder(type, path);
            if (destinationFolder == null)
            {
                Window.Log("Error on folder loading: folder \"" + title + "\" not found");
                return;
            }

            var fullDirectoryPath = GetFullFilePath(destinationFolder) + title + "\\";
            if (Directory.Exists(fullDirectoryPath))
            {
                // add folder
                var newFolder = new ProjectFolder(destinationFolder, type, title);
                destinationFolder.AddFolder(newFolder);
                Window.Log("Folder loaded: " + fullDirectoryPath);
            }
            else
            {
                Window.Log("Error on folder loading \"" + fullDirectoryPath + "\" not found");
            }
        }

        private void LoadFile(FileContentType type, string title, string path)
        {
            var fullDialogFilePath = GetContentTypeDirectory(type) + path + title + ".xml";
            var destinationFolder = FindProjectFilder(type, path);

            if(destinationFolder == null)
            {
                Window.Log("Error on dialog loading \"" + fullDialogFilePath + "\" - file not found");
                return;
            }
            
            if (!string.IsNullOrWhiteSpace(fullDialogFilePath) && File.Exists(fullDialogFilePath))
            {
                try
                {
                    if(type == FileContentType.Dialog)
                    {
                        var dialog = new Dialog(destinationFolder, title, string.Empty, string.Empty, fullDialogFilePath);
                        XmlDialogFileLoaderWithoutUi.Load(Window, dialog);

                        destinationFolder.Add(dialog);
                        Window.ValidateDialog(dialog);
                    }
                    else if(type == FileContentType.Format)
                    {
                        var format = new Format(destinationFolder, title);

                        destinationFolder.Add(format);
                        ProjectFormats.Add(title, format);
                    }
                }
                catch (ArgumentException)
                {
                    Window.Log("Error on content file loading \"" + fullDialogFilePath + "\"");
                }
                catch (XmlException)
                {
                    Window.Log("Error on content file loading \"" + fullDialogFilePath + "\" - XML file corrupted");
                }
                catch (FileNotFoundException)
                {
                    Window.Log("Error on content file loading \"" + fullDialogFilePath + "\" - file not found");
                }
                catch (IOException)
                {
                    Window.Log("Error on content file loading \"" + fullDialogFilePath + "\" - file used in another process. Close that process and reopen dialog project.");
                }
                catch (Exception e)
                {
                    Window.Log("Error on content file loading \"" + fullDialogFilePath + "\" - " + e.ToString());
                    Window.Log(e.StackTrace);
                }
            }
            else
            {
                Window.Log("Error on content file loading \"" + fullDialogFilePath + "\" - file not found");
            }
        }

        public void FolderAdd(string title, ProjectFolder targetFolder)
        {
            if (string.IsNullOrWhiteSpace(title) || targetFolder == null)
            {
                return;
            }
            
            if (targetFolder.FoldersContainsKey(title))
            {
                Window.Log("Folder \"" + title + "\" already exist in \"" + targetFolder.Title + "\"");
                return;
            }

            var fullPath = GetFullFilePath(targetFolder) + title;

            // add folder
            try
            {
                if (!Directory.Exists(fullPath))
                {
                    Directory.CreateDirectory(fullPath);
                }

                if (!targetFolder.FoldersContainsKey(title))
                {
                    // add tree node
                    var newFolder = new ProjectFolder(targetFolder, targetFolder.ContentType, title);
                    targetFolder.AddFolder(newFolder);

                    XmlProjectFileManager.AddFolder(this, newFolder);
                    Window.Log("Folder \"" + title + "\" added");
                }
                else
                {
                    Window.Log("Error on folder add \"" + title + "\" - folder aleady created");
                }
            }
            catch(Exception e)
            {
                Window.Log("Error on folder create \"" + fullPath + "\" - " + e.ToString());
                Window.Log(e.StackTrace);
            }
        }

        public Dialog DialogAdd(string title, string dialogId, string avatarKey, ProjectFolder targetFolder)
        {
            if(targetFolder == null || string.IsNullOrWhiteSpace(title))
            {
                return null;
            }

            var xmlPath = GetFullFilePath(targetFolder) + title + ".xml";

            try
            {
                if (!targetFolder.ContainsKey(title))
                {
                    XmlProjectFileManager.CreateDialogFile(title, dialogId, avatarKey, xmlPath);
                    var dialog = new Dialog(targetFolder, title, dialogId, avatarKey, xmlPath);

                    if(dialog != null)
                    {
                        targetFolder.Add(dialog);
                        XmlProjectFileManager.AddContentFile(this, dialog);

                        Window.Log("Dialog \"" + title + "\" added");
                        return dialog;
                    }
                    else
                    {
                        Window.Log("Error on create dialog \"" + title + "\"");
                        return null;
                    }

                }
                else
                {
                    // close dialog if it exist
                    var dialog = targetFolder.Get(title) as Dialog;
                    if(dialog != null && !File.Exists(xmlPath))
                    {
                        Window.CloseDialogTab(dialog);
                    }
                    
                    Window.Log("Error on dialog add \"" + title + "\" - dialog alerady created");
                    return null;
                }
            }
            catch (ArgumentException e)
            {
                Window.Log("Error on dialog add \"" + title + "\" - ArgumentException\r\n" + e.StackTrace);
            }
            catch (Exception e)
            {
                Window.Log("Error on dialog add \"" + title + "\" - " + e.ToString() + "\r\n" + e.StackTrace);
            }

            return null;
        }

        public void FormatAdd(ProjectFolder targetFolder, string formatName)
        {
            if (targetFolder == null || targetFolder.ContentType != FileContentType.Format || string.IsNullOrWhiteSpace(formatName)) 
            {
                return;
            }

            var xmlPath = GetFullFilePath(targetFolder) + formatName + ".xml";

            try
            {
                XmlProjectFileManager.CreateFormatFile(formatName, xmlPath);

                var format = new Format(targetFolder, formatName);
                XmlProjectFileManager.AddContentFile(this, format);
                ProjectFormats.Add(formatName, format);

                targetFolder.Add(format);
            }
            catch(Exception e)
            {
                Window.Log("Error on format create \"" + formatName + "\" - " + e.ToString() + "\r\n" + e.StackTrace);
            }
        }

        public bool ContainsFormat(string name)
        {
            return ProjectFormats.ContainsKey(name);
        }

        public Format GetFormat(string name)
        {
            Format format = null;

            if (ProjectFormats.TryGetValue(name, out format))
            {
                return format;
            }
            else
            {
                return null;
            }
        }

        public void ElementAdd(Format format, CustomElement element)
        {
            if(format == null || element == null)
            {
                return;
            }

            try
            {
                format.AddElement(element);
                XmlFormatManager.AddElement(format, element);
            }
            catch(Exception e)
            {
                Window.Log("Error on add element to the format \"" + format.Title + "\" - " + e.ToString() + "\r\n" + e.StackTrace);
            }
        }

        private void RemoveProjectFile(object sender, EventArgs e)
        {
            RemoveProjectItem();
        }

        public void RemoveProjectItem()
        {
            try
            {
                object item = Window.SelectedTreeItem;

                if (item != null && item is ProjectFile && item != ProjectTree && item != ProjectTree.Dialogs)
                {
                    var file = item as ProjectFile;

                    var confirm = Window.ConfirmFolderDelete(file.Title);

                    if (confirm)
                    {
                        var parent = file.Parent as ProjectFolder;

                        // remove
                        if (file is ProjectFolder)
                        {
                            RemoveProjectFolder(file as ProjectFolder);
                            parent.RemoveFolder(file.Title);
                        }
                        else if (file is ContentFile)
                        {
                            RemoveContentFile(file as ContentFile);
                            parent.Remove(file as ContentFile);
                        }
                    }
                }
            }
            catch(Exception e)
            {
                Window.Log(e.ToString());
                Window.Log(e.StackTrace);
            }
        }

        private void RemoveContentFile(ContentFile file)
        {
            var filePath = GetFullFilePath(file) + ".xml";

            if (File.Exists(filePath))
            {
                if (file is Dialog)
                {
                    Window.OnDialogRemoved(file as Dialog);
                }

                try
                {
                    File.Delete(filePath);
                    XmlProjectFileManager.RemoveFile(Window, this, file);
                    Window.Log("Content file \"" + filePath + "\" removed");
                }
                catch (IOException e)
                {
                    Window.Log("Error on remove content file \"" + filePath + "\" - IOExeption");
                    Window.Log(e.StackTrace);
                }
                catch (Exception e)
                {
                    Window.Log("Error on remove content file \"" + filePath + "\" - " + e.ToString());
                    Window.Log(e.StackTrace);
                }
            }
            else
            {
                Window.Log("Error on remove content file \"" + filePath + "\" - file not exist");
            }
        }

        private void RemoveProjectFolder(ProjectFolder folder)
        {
            var folderPath = GetFullFilePath(folder);

            if (Directory.Exists(folderPath))
            {
                Window.ProgressBarStart(folder.ItemsCount + folder.FoldersCount);

                // clear folder before removing
                if (folder.ItemsCount > 0)
                {
                    foreach (ContentFile containsFile in folder.ItemsValues)
                    {
                        RemoveContentFile(containsFile);
                        Window.ProgressBarUpdateInc();
                    }
                    folder.Clear();
                }

                if (folder.FoldersCount > 0)
                {
                    foreach (ProjectFolder containsFolder in folder.FoldersValues)
                    {
                        RemoveProjectFolder(containsFolder);
                        Window.ProgressBarUpdateInc();
                    }
                    folder.ClearFolders();
                }
                Window.ProgressBarUpdateEnd();

                // remove folder
                try
                {
                    Directory.Delete(folderPath);
                    XmlProjectFileManager.RemoveFile(Window, this, folder);
                    Window.Log("Folder \"" + folderPath + "\" removed");
                }
                catch (IOException e)
                {
                    Window.Log("Error on remove folder \"" + folderPath + "\" - IOExeption");
                    Window.Log(e.StackTrace);
                }
                catch (Exception e)
                {
                    Window.Log("Error on remove folder \"" + folderPath + "\" " + e.ToString());
                    Window.Log(e.StackTrace);
                }
            }
            else
            {
                Window.Log("Error on remove folder \"" + folderPath + "\" - folder not exist");
            }
        }
        
        public void DialogOpen()
        {
            if (Window.SelectedTreeItem != null && Window.SelectedTreeItem is Dialog)
            {
                DialogOpen(Window.SelectedTreeItem as Dialog);
            }
        }

        public void DialogOpen(Dialog dialog)
        {
            if (dialog == null)
            {
                return;
            }

            if(dialog.TabItem != null)
            {
                Window.SelectTab(dialog.TabItem);
                return;
            }

            var nodePath = dialog.GetRelativePath(ProjectTree);
            var filePath = GetContentTypeDirectory(FileContentType.Dialog) + nodePath + ".xml";

            if (!File.Exists(filePath))
            {
                Window.Log("Error: dialog file \"" + filePath + "\" not found");
                return;
            }
            else
            {
                ClosableTab tab = Window.AddDialogTab(dialog);
                dialog.TabItem = tab;

                XmlDialogFileLoader.Load(Window, dialog);

                Window.Refresh();
                Window.ValidateDialog(dialog);
            }
        }

        public void ImportDialogs(string importPath)
        {
            Window.LogMessagesClear();

            if (!string.IsNullOrEmpty(importPath) && Directory.Exists(importPath))
            {
                // convert dir to project data
                var importDir = new DirectoryInfo(importPath);
                var directories = importDir.GetDirectories("*", SearchOption.AllDirectories);

                // begin import
                var importPathReplace = new Regex(Regex.Escape(importPath));
                ImportDialogList(importPath, importPathReplace, importDir); // root

                if (directories != null && directories.Length > 0)
                {
                    foreach (DirectoryInfo directory in directories)
                    {
                        ImportDialogList(importPath, importPathReplace, directory); // subfolders
                    }
                }
            }
            else
            {
                Window.LogMessage(LogLevel.Error, "Import", "Invalid path.", importPath);
            }
        }

        private void ImportDialogList(string importPath, Regex importPathRegex, DirectoryInfo folder)
        {
            var files = folder.GetFiles("dialogList.csv", SearchOption.TopDirectoryOnly);

            if(files != null && files.Length > 0)
            {
                var dialogListFileInfo = files[0];
                var lines = FileEncodingDetector.RealAllLines(Window, dialogListFileInfo.FullName);
                
                if(lines != null && lines.Length > 0)
                {
                    var relativePath = importPathRegex.Replace(dialogListFileInfo.DirectoryName, "", 1) + "\\";
                    if (importPath.Equals(relativePath))
                    {
                        relativePath = string.Empty;
                    }

                    // show progress
                    Window.ProgressBarStart(lines.Length);
                    
                    foreach(string line in lines)
                    {   // import dialogs
                        var args = line.Split(';'); // title;dialog_id;avatarKey;path

                        var fileTitle = args[0];
                        var dialogId = args[1];
                        var avatarKey = args[2];
                        var targetPath = args[3];

                        if (fileTitle.EndsWith(".csv"))
                        {
                            fileTitle = fileTitle.Remove(fileTitle.Length - ".csv".Length);
                        }

                        var targetFolder = ImportFolders(targetPath); // create missing directories before dialogs imoprt

                        if (!targetFolder.ContainsKey(fileTitle))
                        {
                            var dialog = DialogAdd(fileTitle, dialogId, avatarKey, targetFolder);
                            ImportDialogData(dialogListFileInfo.Directory.FullName, fileTitle, targetFolder, dialog);
                        }
                        else
                        {
                            Window.LogMessage(LogLevel.Error, "Import", "Dialog already exist.", targetFolder.Title + fileTitle);
                        }

                        Window.ProgressBarUpdateInc();
                    }

                    Window.ProgressBarUpdateEnd();
                }
                else
                {
                    Window.LogMessage(LogLevel.Warning, "Import", "dialogList.csv is empty.", dialogListFileInfo.FullName);
                }
            }
            else
            {
                if(folder.GetFiles().Length != 0)
                {
                    Window.LogMessage(LogLevel.Warning, "Import", "dialogList.csv not found.", folder.FullName);
                }
            }
        }

        private ProjectFolder ImportFolders(string relativePath)
        {
            var folder = ProjectTree.Dialogs;

            if (!relativePath.Equals("."))
            {
                char separator = '/';
                if (relativePath.Contains("\\")) // hack, to fix - add Uri validation (Uri.IsWellFormedUriString)
                {
                    separator = '\\';
                }

                var dirs = relativePath.Split(separator);
                foreach (string dir in dirs)
                {
                    if (string.IsNullOrWhiteSpace(dir) || dir.Equals("."))
                    {
                        continue;
                    }

                    if (!folder.FoldersContainsKey(dir))
                    {
                        FolderAdd(dir, folder);
                    }

                    // next subfoler
                    var nextSubFolder = folder.GetFolder(dir);

                    if (nextSubFolder != null)
                    {
                        folder = nextSubFolder;
                    }
                    else
                    {
                        Window.LogMessage(LogLevel.Warning, "Import", "Cant import dir.", folder.GetRelativePath(ProjectTree) + "\\" + dir);
                        break;
                    }
                }

            }

            return folder;
        }

        private void ImportDialogData(string importFolder, string fileTitle, ProjectFolder folder, Dialog dialog)
        {
            var importPathRu = importFolder + "\\" + fileTitle + ".csv";
            var importPathEng = importFolder + "\\" + fileTitle + "_eng.csv";
            var xmlPath = GetContentTypeDirectory(FileContentType.Dialog) + folder.GetRelativePath(ProjectTree) + fileTitle + ".xml";

            if(!File.Exists(xmlPath))
            {
                Window.LogMessage(LogLevel.Warning, "Import", "Data file not exist XML.", xmlPath);
                return;
            }

            if(!File.Exists(importPathRu))
            {
                Window.LogMessage(LogLevel.Warning, "Import", "RU localization data file not exist.", importPathRu);
                return;
            }

            if(!File.Exists(importPathEng))
            {
                importPathEng = importFolder + "\\" + fileTitle + "_en.csv";
                if (!File.Exists(importPathEng))
                {
                    Window.LogMessage(LogLevel.Warning, "Import", "ENG localization data file not exist.", importPathEng);
                    return;
                }
            }

            XmlDialogTableImport.Import(Window, ImportFormat.AuroraCSV, dialog, importPathRu, importPathEng);
        }

        public void ImportDialogTable(string importPath, ImportFormat format)
        {
            Window.LogMessagesClear();

            if (File.Exists(importPath))
            {
                object item = Window.SelectedTreeItem;

                if (item != null && item is ProjectFolder)
                {
                    var targetFolder = item as ProjectFolder;

                    var formatPostfix = string.Empty;
                    switch (format)
                    {
                        case ImportFormat.AuroraCSV: formatPostfix = ".csv"; break;
                        case ImportFormat.AuroraXLSX: formatPostfix = ".xlsx"; break;
                    }
                    
                    var fileTitle = ReplaceLastOccurrence(new FileInfo(importPath).Name, formatPostfix, "");
                    if (!targetFolder.ContainsKey(fileTitle))
                    {
                        var dialogId = fileTitle.Trim().Replace(" ","_").ToLower(); // " Dialog Title " -> "dialog_title"
                        var dialog = DialogAdd(fileTitle, dialogId, string.Empty, targetFolder);

                        if(dialog != null)
                        {
                            XmlDialogTableImport.Import(Window, format, dialog, importPath, null);
                            Window.Log("Import done - \"" + importPath + "\"");
                        }
                        else
                        {
                            Window.Log("Import error - \"" + importPath + "\"");
                        }
                    }
                }
            }
            else
            {
                Window.LogMessage(LogLevel.Warning, "Import", "File not found.", importPath);
            }
        }

        public void ImportNativeXml(string importPath)
        {
            if (File.Exists(importPath))
            {
                object item = Window.SelectedTreeItem;

                if (item != null && item is ProjectFolder)
                {
                    var targetFolder = item as ProjectFolder;

                    try
                    {
                        var fileTitle = ReplaceLastOccurrence(new FileInfo(importPath).Name, ".xml", "");
                        if (!targetFolder.ContainsKey(fileTitle))
                        {
                            var targetPath = GetFullFilePath(targetFolder) + fileTitle + ".xml";

                            // register data in project
                            var xml = new XmlDocument();
                            xml.Load(importPath);

                            var dialogId = string.Empty;
                            var avatarKey = string.Empty;

                            var dialogIdNode = xml.SelectSingleNode("/dialog/info/dialogId");
                            if(dialogIdNode != null)
                            {
                                dialogId = dialogIdNode.InnerText;
                            }
                            else
                            {
                                throw new NullReferenceException();
                            }

                            var avatarKeyNode = xml.SelectSingleNode("/dialog/info/avatarKey");
                            if (avatarKeyNode != null)
                            {
                                avatarKey = avatarKeyNode.InnerText;
                            }
                            else
                            {
                                throw new NullReferenceException();
                            }

                            var dialog = DialogAdd(fileTitle, dialogId, avatarKey, targetFolder);

                            xml.Save(targetPath);
                            dialog.XmlFile.Load(dialog.XmlFilePath);
                            XmlDialogFileLoaderWithoutUi.Load(Window, dialog);
                        }
                    }
                    catch (NullReferenceException)
                    {
                        Window.Log("Error on import dialog file \"" + importPath + "\" - invalid format");
                    }
                    catch (XmlException)
                    {
                        Window.Log("Error on import dialog file \"" + importPath + "\" - invalid format");
                    }
                    catch (IOException)
                    {
                        Window.Log("Error on import dialog file \"" + importPath + "\"");
                    }
                    catch (Exception e)
                    {
                        Window.Log("Unexpected error on import dialog file \"" + importPath + "\" - " + e.StackTrace);
                    }
                }
            }
        }

        public static string ReplaceLastOccurrence(string Source, string Find, string Replace)
        {
            int place = Source.LastIndexOf(Find);

            if (place == -1)
            {
                return Source;
            }
            else
            {
                return Source.Remove(place, Find.Length).Insert(place, Replace);
            }
        }

        internal void EditSettings(string title, string sourcePath, bool sourcePathIsRelative)
        {
            // set title
            Title = title;

            // set path
            var fileInfo = new FileInfo(ProjectXmlPath);
            var projectPath = fileInfo.DirectoryName;

            if (sourcePathIsRelative)
            {
                SourceRelativeFolder = sourcePath;
                SourceFolder = projectPath + SourceRelativeFolder;
            }
            else
            {
                SourceRelativeFolder = "\\" + new DirectoryInfo(sourcePath).Name + "\\";
                SourceFolder = projectPath + SourceRelativeFolder;
            }

            XmlProjectFileManager.UpdateProjectSettings(this);
            Window.OpenProject(ProjectXmlPath);
        }
    }
}