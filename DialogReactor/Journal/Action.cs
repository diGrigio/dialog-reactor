﻿using System.Collections.ObjectModel;

namespace DialogReactor.Journal
{
    public enum ActionType
    {
        // dialog editor - elements
        ElementAdd,
        ElementRemoved,
        ElementMoved,

        // dialog editor - links
        LinkAdd,
        LinkRemoved,
    }

    public abstract class Action
    {
        public ActionType Type { get; private set; }

        public Action(ActionType type)
        {
            Type = type;
        }
    }

    // Dialog Elements actions
    public abstract class ActionDialogElement : Action
    {
        public Element Element { get; private set; }

        public ActionDialogElement(ActionType type, Element element) : base(type)
        {
            Element = element;
        }
    }

    public sealed class ActionDialogElementAdd : ActionDialogElement
    {
        public double PosX { get; private set; }
        public double PosY { get; private set; }

        public ActionDialogElementAdd(Element element, double x, double y) : base(ActionType.ElementAdd, element)
        {
            PosX = x;
            PosY = y;
        }
    }
    public sealed class ActionDialogElementRemoved : ActionDialogElement
    {
        public double PosX { get; private set; }
        public double PosY { get; private set; }

        public Collection<ActionDialogLinkRemove> RemovedLinks { get; private set; }

        public ActionDialogElementRemoved(Element element, double x, double y, Collection<ActionDialogLinkRemove> removedLinks) : base(ActionType.ElementRemoved, element)
        {
            PosX = x;
            PosY = y;
            RemovedLinks = removedLinks;
        }
    }

    // Dialog Elements group actions
    public abstract class ActionGroupDialogElement : Action
    {
        public Collection<Element> Elements { get; private set; }

        public ActionGroupDialogElement(ActionType type, Collection<Element> elements) : base(type)
        {
            Elements = elements;
        }
    }

    public sealed class ActionDialogElementMoved : ActionGroupDialogElement
    {
        public double dx { get; private set; }
        public double dy { get; private set; }

        public ActionDialogElementMoved(Collection<Element> elements, double dx, double dy) : base(ActionType.ElementMoved, elements)
        {
            this.dx = dx;
            this.dy = dy;
        }
    }

    // Dialog Links actions
    public abstract class ActionDialogLink : Action
    {
        public Element From { get; private set; }
        public Element To { get; private set; }

        public ActionDialogLink(ActionType type, Element from, Element to) : base(type)
        {
            From = from;
            To = to;
        }
    }

    public sealed class ActionDialogLinkAdd : ActionDialogLink
    {
        public ActionDialogLinkAdd(Element from, Element to) : base(ActionType.LinkAdd, from, to) { }
    }

    public sealed class ActionDialogLinkRemove : ActionDialogLink
    {
        public ActionDialogLinkRemove(Element from, Element to) : base(ActionType.LinkRemoved, from, to) { }
    }
}
