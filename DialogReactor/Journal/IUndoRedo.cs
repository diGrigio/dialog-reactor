﻿namespace DialogReactor.Journal
{
    interface IUndoRedo
    {
        void RegisterAction(Action action, bool needValidation);
        void Undo();
        void Redo();
    }
}
