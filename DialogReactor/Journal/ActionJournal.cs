﻿using System;
using System.Collections.Generic;

namespace DialogReactor.Journal
{
    class ActionJournal
    {
        private Stack<Action> UndoStack;
        private Stack<Action> RedoStack;

        public ActionJournal()
        {
            UndoStack = new Stack<Action>();
            RedoStack = new Stack<Action>();
        }

        public void RegisterAction(Action action)
        {
            if(action != null)
            {
                RedoStack.Clear();
                UndoStack.Push(action);
            }
            else
            {
                throw new ArgumentNullException("Action is null");
            }
        }

        public Action Undo()
        {
            if (UndoStack.Count > 0)
            {
                Action action = UndoStack.Pop();
                RedoStack.Push(action);
                return action;
            }
            else
            {
                return null;
            }
        }

        public Action Redo()
        {
            if(RedoStack.Count > 0)
            {
                Action action = RedoStack.Pop();
                UndoStack.Push(action);
                return action;
            }
            else
            {
                return null;
            }
        }

        public bool RedoIsEmpty()
        {
            return RedoStack.Count == 0;
        }

        public bool UndoIsEmpty()
        {
            return UndoStack.Count == 0;
        }
    }
}