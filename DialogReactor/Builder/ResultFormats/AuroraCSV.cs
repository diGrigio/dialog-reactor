﻿using DialogReactor.Util.Validation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DialogReactor
{
    public class AuroraCSV : IFormat
    {
        public enum Localization
        {
            RU,
            ENG
        }

        public void ExportResult(ProjectHandler project, MainWindow window)
        {
            window.LogMessagesClear();
            
            if (project == null || project.ProjectTree == null)
            {
                window.LogMessage(LogLevel.Error, "Export", "Project not loaded.");
            }
            else
            {
                ProjectTree tree = project.ProjectTree;

                window.Log("Export started");

                if (!Directory.Exists(project.ResultFolder))
                {
                    Directory.CreateDirectory(project.ResultFolder);
                }

                if (Directory.Exists(project.ResultFolder))
                {
                    int totalDialogs = CheckFolders(project, tree, tree.Dialogs, window);

                    window.ProgressBarStart(totalDialogs);
                    ExportFolders(project, tree, tree.Dialogs, window);
                    window.ProgressBarUpdateEnd();
                }
                else
                {
                    window.LogMessage(LogLevel.Error, "Export", "Cant create import directory.", project.ResultFolder);
                }
            }
        }

        private static int CheckFolders(ProjectHandler project, ProjectTree tree, ProjectFolder sourceFolder, MainWindow window)
        {
            if (sourceFolder.FoldersCount == 0)
            {
                return sourceFolder.ItemsCount;
            }

            int dialogs = 0;
            foreach (ProjectFolder folder in sourceFolder.FoldersValues)
            {
                string path = project.ResultFolder + folder.GetRelativePath(tree);

                if (!Directory.Exists(path))
                {
                    try
                    {
                        Directory.CreateDirectory(path);
                    }
                    catch (IOException)
                    {
                        window.LogMessage(LogLevel.Error, "Export", "Cant create directory.", path);
                    }
                    catch (Exception)
                    {
                        window.LogMessage(LogLevel.Error, "Export", "Cant create directory.", path);
                    }
                }

                dialogs += CheckFolders(project, tree, folder, window);
            }

            return sourceFolder.ItemsCount + dialogs;
        }

        private static void ExportFolders(ProjectHandler project, ProjectTree tree, ProjectFolder sourceFolder, MainWindow window)
        {
            if(sourceFolder.FoldersCount > 0)
            {
                foreach (ProjectFolder folder in sourceFolder.FoldersValues)
                {
                    ExportFolders(project, tree, folder, window);
                }
            }

            if (sourceFolder.ItemsCount > 0)
            {
                List<string> dialogsInfo = new List<string>();

                // build dialogs
                foreach (Dialog dialog in sourceFolder.ItemsValues)
                {
                    if(BuildDialog(project, tree, dialog, window))
                    {
                        dialogsInfo.Add(AddDialogListDialogSignature(dialog, tree, sourceFolder));
                        window.ProgressBarUpdateInc();
                    }
                }

                // write dialogList.csv
                string path = project.ResultFolder + sourceFolder.GetRelativePath(tree) + "dialogList.csv";

                try
                {
                    File.WriteAllLines(path, dialogsInfo.ToArray(), new UTF8Encoding(false)); // utf-8 without BOM, this is important
                }
                catch (IOException)
                {
                    window.LogMessage(LogLevel.Error, "Export", "File used from another process. Close that process and try export project again.", path);
                }
                catch(Exception e)
                {
                    window.LogMessage(LogLevel.Error, "Export", "Unexpected file writing error. Check log.", path);
                    window.Log("ERROR: Error on write file \"" + path + "\" - " + e.ToString());
                }
            }
        }
        
        private static string AddDialogListDialogSignature(Dialog dialog, ProjectTree tree, ProjectFolder sourceFolder)
        {
            // title.csv;in_game_id;avatar_key;.
            string result = "";

            result += dialog.Title + ".csv;";
            result += dialog.DialogId + ";";
            result += dialog.AvatarKey + ";";

            string relativePath = sourceFolder.GetRelativePath(tree);

            if (string.IsNullOrEmpty(relativePath))
            {
                result += ".";
            }
            else
            {
                result += relativePath;
            }
            
            return result;
        }

        private static bool BuildDialog(ProjectHandler project, ProjectTree tree, Dialog dialog, MainWindow window)
        {
            window.Log("Export to AuroraCSV dialog \"" + dialog.GetRelativePath(tree) + "\"");
            bool buildConfirm = false;

            //
            DialogValidation.DialogElementsValidation(window, tree, dialog, "Export");

            // Get start element and load dialog data if it not loaded
            ElementNpcStatement startStatement = dialog.GetStartStatement();
            if (startStatement == null)
            {
                XmlDialogFileLoaderWithoutUi.Load(window, dialog);
                startStatement = dialog.GetStartStatement();

                if(startStatement == null)
                {
                    window.LogLine(LogLevel.Error, "Export", tree, dialog, "Start statement not found");
                    return false;
                }
            }
            
            // Build result
            List<string> resultCsvRu = new List<string>();
            List<string> resultCsvEng = new List<string>();
            BuildCSV(dialog, startStatement, resultCsvRu, resultCsvEng, window);

            if (resultCsvRu.Count > 0)
            {
                WriteResultFile(project, tree, dialog, resultCsvRu, resultCsvEng, window);
                buildConfirm = true;
            }

            return buildConfirm;
        }

        private static void BuildCSV(Dialog dialog, ElementNpcStatement startStatement, List<string> resultCsvRu, List<string> resultCsvEng, MainWindow window)
        {
            Dictionary<int, int> indexes = BuildIndexMap(dialog, startStatement);
            HashSet<int> buildedStatements = new HashSet<int>();

            List<ElementNpcStatement> replays = new List<ElementNpcStatement>();
            replays.Add(startStatement);

            while (replays.Count > 0)
            {
                ElementNpcStatement currentStatement = replays[0];
                replays.RemoveAt(0);

                List<ElementNpcStatement> newStatement = BuildStatement(currentStatement, buildedStatements, indexes, resultCsvRu, resultCsvEng, window);
                if(newStatement != null && newStatement.Count > 0)
                {
                    replays.AddRange(newStatement);
                }
            }
        }

        private static Dictionary<int, int> BuildIndexMap(Dialog dialog, ElementNpcStatement startStatement)
        {
            Dictionary<int, int> indexes = new Dictionary<int, int>();
            int id = 0;
            indexes.Add(startStatement.Id, id++); // start statement index is always zero

            foreach (Element element in dialog.Elements)
            {
                if (element is ElementNpcStatement && !indexes.ContainsKey(element.Id))
                {
                    indexes.Add(element.Id, id++);
                }
            }

            return indexes;
        }

        private static List<ElementNpcStatement> BuildStatement(ElementNpcStatement statement, HashSet<int> buildedStatements, Dictionary<int, int> indexes, List<string> resultCsvRu, List<string> resultCsvEng, MainWindow window)
        {
            if (buildedStatements.Contains(statement.Id))
            {
                return null;
            }

            // get id
            int id = 0;
            if(indexes.TryGetValue(statement.Id, out id))
            {
                resultCsvRu.Add(BuildStatementSignature(statement, id, Localization.RU));
                resultCsvEng.Add(BuildStatementSignature(statement, id, Localization.ENG));
                buildedStatements.Add(statement.Id);

                return BuildStatementAnswers(statement, indexes, resultCsvRu, resultCsvEng, window);
            }
            else
            {
                return null;
            }
        }

        private static List<ElementNpcStatement> BuildStatementAnswers(ElementNpcStatement statement, Dictionary<int, int> indexes, List<string> resultCsvRu, List<string> resultCsvEng, MainWindow window)
        {
            List<ElementNpcStatement> newStatement = new List<ElementNpcStatement>();

            if(statement.OutputLinks.Count > 0)
            {            
                // sort answers by priority
                SortedDictionary<int, List<ElementAnswer>> soredAnswers = new SortedDictionary<int, List<ElementAnswer>>();
                foreach (Link link in statement.OutputLinks)
                {
                    if (link.To is ElementAnswer)
                    {
                        ElementAnswer answer = (ElementAnswer)link.To;

                        List<ElementAnswer> level = null;
                        if (!soredAnswers.TryGetValue(answer.Priority, out level))
                        {
                            level = new List<ElementAnswer>();
                            soredAnswers.Add(answer.Priority, level);
                        }

                        level.Add(answer);
                    }
                }

                // build answers
                foreach (List<ElementAnswer> level in soredAnswers.Values)
                {
                    foreach (ElementAnswer answer in level)
                    {
                        BuildAnswer(answer, indexes, resultCsvRu, resultCsvEng);

                        foreach (Link link in answer.OutputLinks)
                        {
                            if (link.To is ElementNpcStatement)
                            {
                                newStatement.Add(link.To as ElementNpcStatement);
                            }
                        }
                    }
                }
            }
            
            return newStatement;
        }

        private static void BuildAnswer(ElementAnswer answer, Dictionary<int, int> indexes, List<string> resultCsvRu, List<string> resultCsvEng)
        {
            int targetId = -1;
            if(answer.OutputLinks.Count > 0)
            {
                Link link = answer.OutputLinks[0];
                if(link.To is ElementNpcStatement)
                {
                    indexes.TryGetValue(link.To.Id, out targetId);
                }
                else if(link.To is ElementDialogEnd)
                {
                    targetId = -1;
                }
            }

            resultCsvRu.Add(BuildAnswerSignature(answer, targetId, Localization.RU));
            resultCsvEng.Add(BuildAnswerSignature(answer, targetId, Localization.ENG));
        }


        private static string BuildStatementSignature(ElementNpcStatement statement, int id, Localization loc)
        {
            // build signature (.csv line)
            // id;text;avatar_key;
            string result = "";
            result += "" + id + ";";

            if (loc == Localization.RU)
            {
                result += statement.TextRU + ";";
            }
            else if (loc == Localization.ENG)
            {
                result += statement.TextEng + ";";
            }

            result += statement.AvatarKey + ";";
            return result;
        }

        private static string BuildAnswerSignature(ElementAnswer answer, int targetId, Localization loc)
        {
            // build signature (.csv line)
            // ;text;target_id;condition;return_value;flag;
            // if target_id == -1 -> end dialog

            string result = ";";

            if(loc == Localization.RU)
            {
                result += answer.TextRU + ";";
            }
            else if(loc == Localization.ENG)
            {
                result += answer.TextEng + ";";
            }
            
            result += "" + targetId + ";";
            result += answer.Conditions + ";";
            result += answer.EndDialogKey + ";";
            result += answer.AnswerFlags + ";";

            return result;
        }

        private static void WriteResultFile(ProjectHandler project, ProjectTree tree, Dialog dialog, List<string> resultCsvRu, List<string> resultCsvEng, MainWindow window)
        {
            string pathRu = project.ResultFolder + dialog.GetRelativePath(tree) + ".csv";
            string pathEng = project.ResultFolder + dialog.GetRelativePath(tree) + "_eng.csv";
            Encoding encode = new UTF8Encoding(false); // utf-8 without BOM, this is important

            FinallyWriteResult(pathRu, resultCsvRu, encode, window);
            FinallyWriteResult(pathEng, resultCsvEng, encode, window);
        }

        private static void FinallyWriteResult(string path, List<string> resultCsv, Encoding encode, MainWindow window)
        {
            try
            {
                File.WriteAllLines(path, resultCsv.ToArray(), encode);
            }
            catch (IOException)
            {
                window.LogMessage(LogLevel.Error, "Export", "File used from another process. Close that process and try export project again.", path);
            }
            catch (Exception e)
            {
                window.LogMessage(LogLevel.Error, "Export", "Unexpected file writing error. Check log.", path);
                window.Log("ERROR: File \"" + path + "\" writing error - " + e.ToString());
            }
        }
    }
}
