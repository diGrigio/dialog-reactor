﻿namespace DialogReactor
{
    interface IFormat
    {
        void ExportResult(ProjectHandler project, MainWindow window);
    }
}