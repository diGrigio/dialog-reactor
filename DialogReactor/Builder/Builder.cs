﻿namespace DialogReactor
{
    internal static class Builder
    {
        internal static void ExportProject(ProjectHandler project, MainWindow window)
        {
            if(project != null)
            {
                IFormat format = new AuroraCSV();
                format.ExportResult(project, window); 
            }
        }
    }
}