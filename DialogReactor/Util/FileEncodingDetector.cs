﻿using System;
using System.IO;
using System.Text;

namespace DialogReactor.Util
{
    public static class FileEncodingDetector
    {
        public static string[] RealAllLines(MainWindow window, string filePath)
        {
            if(filePath == null)
            {
                return null;
            }

            try
            {
                return File.ReadAllLines(filePath, GetEncoding(window, filePath));
            }
            catch(Exception e)
            {
                window.Log("Error on reading file \"" + filePath + "\" - " + e.ToString());
                return null;
            }
        }

        public static Encoding GetEncoding(MainWindow window, string filePath)
        {
            var encode = SimpleHelpers.FileEncoding.DetectFileEncoding(filePath);
            if (encode == null)
            { // hack of encoding bug (https://github.com/dotnet/roslyn/issues/4022)
                encode = Encoding.GetEncoding(1251);

                window.LogMessage(LogLevel.Warning, "Import", "Encoding of text file cant detect correctly. Set encode to UTF-8 to correct work.", filePath);
            }

            return encode;
        }
    }
}
