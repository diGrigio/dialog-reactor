﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DialogReactor.Util.Validation
{
    public static class DialogValidation
    {
        public static void ValidateDialog(MainWindow window, ObservableCollection<LogMessage> messages, ProjectTree tree, Dialog dialog)
        {
            // filter messages in this dialog
            if (messages.Count > 0)
            {
                var toRemove = new List<LogMessage>();

                foreach (LogMessage msg in messages)
                {
                    if (msg.Dialog == dialog)
                    {
                        toRemove.Add(msg);

                        if(msg.Element != null)
                        {
                            msg.Element.ValidationError = false;
                        }
                    }
                }

                if (toRemove.Count > 0)
                {
                    foreach (LogMessage msg in toRemove)
                    {
                        messages.Remove(msg);
                    }
                }
            }

            DialogElementsValidation(window, tree, dialog, "Edit");
            window.LogMessagesCounterUpdate();
        }

        public static void DialogElementsValidation(MainWindow window, ProjectTree tree, Dialog dialog, string type)
        {
            foreach (Element element in dialog.Elements)
            {
                ValidateElements(tree, dialog, window, element, type);
            }
        }

        private static void ValidateElements(ProjectTree tree, Dialog dialog, MainWindow window, Element element, string type)
        {
            if (element is ElementNpcStatement && element.OutputLinks.Count == 0)
            {
                window.LogLine(LogLevel.Error, type, tree, dialog, "NPC statement has no answers", element);
            }
            else if (element is ElementNpcStatement)
            {
                var state = element as ElementNpcStatement;

                if (state.InputLinks.Count == 0)
                {
                    window.LogLine(LogLevel.Warning, type, tree, dialog, "NPC statement not used", element);
                }

                if (string.IsNullOrWhiteSpace(state.TextRU))
                {
                    window.LogLine(LogLevel.Warning, type, tree, dialog, "Text is empty (russian localization)", element);
                }

                if (string.IsNullOrWhiteSpace(state.TextEng))
                {
                    window.LogLine(LogLevel.Warning, type, tree, dialog, "Text is empty (english localization)", element);
                }
            }
            else if (element is ElementAnswer)
            {
                var answer = element as ElementAnswer;

                if (answer.InputLinks.Count == 0)
                {
                    window.LogLine(LogLevel.Warning, type, tree, dialog, "Answer not used", element);
                }

                if (string.IsNullOrWhiteSpace(answer.TextRU))
                {
                    window.LogLine(LogLevel.Warning, type, tree, dialog, "Text is empty (russian localization)", element);
                }

                if (string.IsNullOrWhiteSpace(answer.TextEng))
                {
                    window.LogLine(LogLevel.Warning, type, tree, dialog, "Text is empty (english localization)", element);
                }
            }

            if (element.OutputLinks.Count == 0 && element.InputLinks.Count == 0)
            {
                window.LogLine(LogLevel.Error, type, tree, dialog, "Element not used", element);
            }
        }
    }
}
