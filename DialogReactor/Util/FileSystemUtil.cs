﻿using System;
using System.IO;

namespace DialogReactor.Util
{
    public class FileSystemUtil
    {
        public static bool IsSubfolder(string root, string testedSubDir)
        {
            if (string.IsNullOrWhiteSpace(root) || string.IsNullOrWhiteSpace(testedSubDir))
            {
                return false;
            }
            else if (!Directory.Exists(root) || !Directory.Exists(testedSubDir))
            {
                return false;
            }
            else
            {
                try
                {
                    DirectoryInfo dir1 = new DirectoryInfo(root);
                    DirectoryInfo dir2 = new DirectoryInfo(testedSubDir);

                    bool isParent = false;
                    while (dir2.Parent != null)
                    {
                        if (dir2.Parent.FullName == dir1.FullName)
                        {
                            isParent = true;
                            break;
                        }
                        else
                        {
                            dir2 = dir2.Parent;
                        }
                    }

                    return isParent;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
    }
}
