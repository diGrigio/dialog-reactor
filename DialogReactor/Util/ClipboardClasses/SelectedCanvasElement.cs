﻿using System;

namespace DialogReactor.Util.ClipboardClasses
{
    [Serializable]
    public class ClipboardElement
    {
        public int Id;
        public double Dx;
        public double Dy;

        public double Width;
        public double Height;

        public string Type;
        public object [] Data;
    }
}
