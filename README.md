### Description
Dialog Reactor - visual editor of graphs for [AuroraRL](https://bitbucket.org/e_smirnov/aurora/) game dialogues.


### Downloads
You can download last build [here](https://bitbucket.org/diGrigio/dialog-reactor/downloads)


### System requirements
* Microsoft Windows XP\Vista\7\8\8.1\10 (x32\x64)
* [Microsoft .NET Framework 4](https://www.microsoft.com/en-us/download/details.aspx?id=17718)
* 256 Mb of RAM
* DirectX 9.0 supported graphics card

### Preview
![dialog reactor.png](https://bitbucket.org/repo/7qE7Rg/images/3560504320-dialog%20reactor.png)

### Authors
* [di Grigio](https://vk.com/leodigrigio) - Author and developer, leo.di.grigio@gmail.com
* [Egor Smirnov](https://bitbucket.org/e_smirnov/) - AuroraRL project owner and developer

### Used external resources in program
* [iconmonstr](http://iconmonstr.com/) ( [License](http://iconmonstr.com/license/) ) - UI Icons
* [EPPlus](http://epplus.codeplex.com) ( [LGPL](http://epplus.codeplex.com/license) ) - XLSX import lib
* [Ude](https://github.com/errepi/ude) - Mozilla Universal Charset Detector